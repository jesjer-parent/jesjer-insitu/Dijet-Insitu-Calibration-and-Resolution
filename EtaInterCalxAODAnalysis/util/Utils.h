/*
 * Author: Michaela Queitsch-Maitland <michaela.queitsch-maitland@cern.ch>
 */

// C++ includes
#include <iostream>
#include <string>
#include <map>
#include <unordered_map>

// xAOD ROOT access
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

#include "xAODCore/ShallowCopy.h"

#include "xAODEventInfo/EventInfo.h"

#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTrigger/JetRoIContainer.h"
#include "xAODTrigger/JetRoIAuxContainer.h"

//#include "EventShapeTools/EventShapeCopier.h"

// ROOT includes
#include "TFile.h"
#include "TString.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TEnv.h"
#include "TError.h"

// Tools
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetSelectorTools/JetCleaningTool.h"

static const float GeV = 1000.;
//static float refRegion = 0.8;
//static float centralRegion = 2.4;

static bool descendingPt(xAOD::Jet* a, xAOD::Jet* b) { return a->pt() > b->pt(); }

static float deltaPhi(const xAOD::Jet& j1, const xAOD::Jet& j2) {
  return TVector2::Phi_mpi_pi(j1.phi()-j2.phi());
}

static TH1F* createHist1D(TString hname, TString title, int nbins, int xlow, int xhigh) {
  TH1F* h = new TH1F(hname,title,nbins,xlow,xhigh);
  return h;
}

static TH1F* createHist1D(TString hname, TString title, std::vector<double> bins) {
  TH1F* h = new TH1F(hname,title,bins.size()-1,&bins[0]);
  return h;
}

static TH2F* createHist2D(TString hname, TString title, int xnbins, float xlow, float xhigh, int ynbins, float ylow, float yhigh) {
  TH2F* h = new TH2F(hname,title,xnbins,xlow,xhigh,ynbins,ylow,yhigh);
  return h;
}

static TH2F* createHist2D(TString hname, TString title, std::vector<double> xbins, std::vector<double> ybins) {
  TH2F* h = new TH2F(hname,title,xbins.size()-1,&xbins[0],ybins.size()-1,&ybins[0]);
  return h;
}

static TH3F* createHist3D(TString hname, TString title, int xnbins, float xlow, float xhigh, int ynbins, float ylow, float yhigh, int znbins, float zlow, float zhigh) {
  TH3F* h = new TH3F(hname,title,xnbins,xlow,xhigh,ynbins,ylow,yhigh,znbins,zlow,zhigh);
  return h;
}

static TH3F* createHist3D(TString hname, TString title, std::vector<double> xbins, std::vector<double> ybins, std::vector<double> zbins) {
  TH3F* h = new TH3F(hname,title,xbins.size()-1,&xbins[0],ybins.size()-1,&ybins[0],zbins.size()-1,&zbins[0]);
  return h;
}

static std::vector<TString> vectorise(TString str, TString sep=" ") {
  std::vector<TString> result; TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) { delete strings; return result; }
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr()) result.push_back(os->GetString());
  delete strings; return result;
}

static std::vector<double> vectoriseD(TString str, TString sep=" ") {
  std::vector<double> result; std::vector<TString> vecS = vectorise(str);
  for (uint i=0;i<vecS.size();++i)
    result.push_back(atof(vecS[i]));
  return result;
}

static std::vector<double> makeUniformVec(int N, double min, double max) {
  std::vector<double> vec; double dx=(max-min)/N;
  for (int i=0;i<=N;++i) vec.push_back(min+i*dx);
  return vec;
}
