################################################################################
# Package: EtaInterCalxAODAnalysis
################################################################################
# Declare the package name:
atlas_subdir( EtaInterCalxAODAnalysis )
# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          DataQuality/GoodRunsLists
                          Event/xAOD/xAODEventInfo
                          Event/xAOD/xAODJet
                          Event/xAOD/xAODRootAccess
                          Event/xAOD/xAODTruth
                          PhysicsAnalysis/AnalysisCommon/PileupReweighting
                          PhysicsAnalysis/JetMissingEtID/JetSelectorTools
                          Reconstruction/Jet/JetCalibTools
                          Reconstruction/Jet/JetMomentTools
                          Reconstruction/Jet/JetInterface
                          Trigger/TrigAnalysis/TrigDecisionTool
                          Trigger/TrigConfiguration/TrigConfxAOD
)
# Find the needed external(s):
find_package( ROOT COMPONENTS Core RIO Hist Tree )
# build a dictionary for the library
atlas_add_root_dictionary ( EtaInterCalxAODAnalysis EtaInterCalxAODAnalysisDictSource
                            ROOT_HEADERS EtaInterCalxAODAnalysis/*.h Root/LinkDef.h
                            EXTERNAL_PACKAGES ROOT
)
# build a shared library
atlas_add_library( EtaInterCalxAODAnalysis EtaInterCalxAODAnalysis/*.h Root/*.h Root/*.cxx ${EtaInterCalxAODAnalysisDictSource}
                   PUBLIC_HEADERS EtaInterCalxAODAnalysis
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                   LINK_LIBRARIES ${ROOT_LIBRARIES} GoodRunsListsLib 
                   xAODEventInfo xAODJet xAODRootAccess xAODTruth
                   PileupReweightingLib JetSelectorToolsLib 
                   JetCalibToolsLib JetMomentToolsLib 
                   TrigDecisionToolLib TrigConfxAODLib
                   PRIVATE_LINK_LIBRARIES RootCoreUtils PathResolver
)
# add executable #robert
atlas_add_executable( EtaInterCalAnalysis
	   util/EtaInterCalAnalysis.cxx
	   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
	   LINK_LIBRARIES ${ROOT_LIBRARIES} EtaInterCalxAODAnalysis
                   )
# Install files from the package:
atlas_install_scripts( scripts/*.py )
atlas_install_data( data/* )

