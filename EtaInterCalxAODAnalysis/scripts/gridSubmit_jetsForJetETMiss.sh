#!/bin/bash

USERNAME=jrawling
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=EtaInterCal_3DHistos_data.root

INDS=data16_13TeV:data16_13TeV.00305291.physics_Main.merge.DAOD_JETM1.f727_m1646_p2769
OUTDS=data16_13TeV.0030529_p2769

echo "Submitting to "${INDS}
echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

prun --inDS=${INDS}\
  --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
  --outputs=${OUTPUTS}\
  --exec="EtaInterCalxAODAnalysis/scripts/gridExec.sh %IN"\
  --useRootCore\
  --excludedSite=${BLACKLIST}\
  --nGBPerJob=10\
  --tmpDir=/tmp\
  --extFile=JetCalibTools/*,EventShapeTools/*\
  --mergeOutput
