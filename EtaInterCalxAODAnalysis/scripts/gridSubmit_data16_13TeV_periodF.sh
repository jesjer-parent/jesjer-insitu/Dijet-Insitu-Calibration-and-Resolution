#!/bin/bash

USERNAME=jrawling
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=ANALY_RRC-KI-T1

OUTPUTS=EtaInterCal_3DHistos_data.root

INDS=data16_13TeV:data16_13TeV.00303943.physics_Main.merge.DAOD_JETM1.f716_m1620_p2689,data16_13TeV:data16_13TeV.00304006.physics_Main.merge.DAOD_JETM1.f716_m1620_p2689,data16_13TeV:data16_13TeV.00304008.physics_Main.merge.DAOD_JETM1.f716_m1620_p2689,data16_13TeV:data16_13TeV.00304128.physics_Main.merge.DAOD_JETM1.f716_m1620_p2689,data16_13TeV:data16_13TeV.00304178.physics_Main.merge.DAOD_JETM1.f716_m1620_p2689,data16_13TeV:data16_13TeV.00304198.physics_Main.merge.DAOD_JETM1.f716_m1620_p2689,data16_13TeV:data16_13TeV.00304211.physics_Main.merge.DAOD_JETM1.f716_m1620_p2689,data16_13TeV:data16_13TeV.00304243.physics_Main.merge.DAOD_JETM1.f716_m1620_p2689,data16_13TeV:data16_13TeV.00304308.physics_Main.merge.DAOD_JETM1.f716_m1620_p2689,data16_13TeV:data16_13TeV.00304337.physics_Main.merge.DAOD_JETM1.f716_m1620_p2689,data16_13TeV:data16_13TeV.00304409.physics_Main.merge.DAOD_JETM1.f716_m1620_p2689,data16_13TeV:data16_13TeV.00304431.physics_Main.merge.DAOD_JETM1.f716_m1620_p2689,data16_13TeV:data16_13TeV.00304494.physics_Main.merge.DAOD_JETM1.f716_m1620_p2689
OUTDS=data16_13TeV.periodF

echo "Submitting to "${INDS}
echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

prun --inDS=${INDS}\
  --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
  --outputs=${OUTPUTS}\
  --exec="EtaInterCalxAODAnalysis/scripts/gridExec.sh %IN"\
  --useRootCore\
  --excludedSite=${BLACKLIST}\
  --nGBPerJob=10\
  --tmpDir=/tmp\
  --extFile=JetCalibTools/*,EventShapeTools/*\
  --mergeOutput
