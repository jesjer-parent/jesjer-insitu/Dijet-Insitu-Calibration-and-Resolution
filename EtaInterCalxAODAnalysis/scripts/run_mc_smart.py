#!/bin/python

from subprocess import call,Popen
from sys import argv
import fnmatch
import os
import re
from math import ceil
import argparse
from ROOT import TFile, TTree, TString

settings=""

parser = argparse.ArgumentParser(description='Script for splitting jobs to run over many input files')
parser.add_argument('-i','--input', help='Input files',  nargs='*', required=True)
parser.add_argument('-s','--set', help='Settings to be changed in config',required=False)
args = parser.parse_args()
argsdict = vars(args)

if argsdict['set'] :
    settings=args.set

# remove input files that are not root files
for f in range(0,len(args.input)) :
    fn = args.input[f]
    if fnmatch.fnmatch(fn, '*.txt') or fnmatch.fnmatch(fn, '*.log') :
        del args.input[f]

infiles = []
for i in range(1,10):
    nevents=0
    infiles=[]
    for fn in args.input :
        if not TString(fn).Contains("JZ"+str(i)):
            continue
        print fn
        f = TFile.Open(fn,"read")
        t = f.Get("CollectionTree")
        nevents += t.GetEntries()
        infiles.append(fn)

    print "J"+str(i),"nfiles =",len(infiles),"nevents =",nevents
    log_file = open("job_J"+str(i)+".log","wb")
#    Popen(["nice","-n","19","EtaInterCalAnalysis","--set",settings,"--o","EtaInterCal_3DHists_J"+str(i)+".root","--in"]+infiles,stdout=log_file,stderr=log_file)
    Popen(["EtaInterCalAnalysis","--set",settings,"--o","EtaInterCal_3DHists_J"+str(i)+".root","--in"]+infiles,stdout=log_file,stderr=log_file)

    print "Submitted job for J"+str(i)

