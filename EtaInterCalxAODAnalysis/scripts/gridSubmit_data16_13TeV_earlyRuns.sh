#!/bin/bash

USERNAME=jrawling
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=ANALY_RRC-KI-T1

OUTPUTS=EtaInterCal_3DHistos_data.root

INDS=data16_13TeV:data16_13TeV.00297730.physics_Main.merge.DAOD_JETM1.f694_m1583_p2623,data16_13TeV:data16_13TeV.00298595.physics_Main.merge.DAOD_JETM1.f696_m1588_p2623,data16_13TeV:data16_13TeV.00298609.physics_Main.merge.DAOD_JETM1.f696_m1588_p2623,data16_13TeV:data16_13TeV.00298633.physics_Main.merge.DAOD_JETM1.f698_m1594_p2623,data16_13TeV:data16_13TeV.00298687.physics_Main.merge.DAOD_JETM1.f698_m1594_p2623,data16_13TeV:data16_13TeV.00298690.physics_Main.merge.DAOD_JETM1.f696_m1588_p2623,data16_13TeV:data16_13TeV.00298771.physics_Main.merge.DAOD_JETM1.f696_m1588_p2623,data16_13TeV:data16_13TeV.00298773.physics_Main.merge.DAOD_JETM1.f696_m1588_p2623,data16_13TeV:data16_13TeV.00298862.physics_Main.merge.DAOD_JETM1.f696_m1588_p2623,data16_13TeV:data16_13TeV.00298967.physics_Main.merge.DAOD_JETM1.f696_m1588_p2623,data16_13TeV:data16_13TeV.00299055.physics_Main.merge.DAOD_JETM1.f698_m1594_p2623,data16_13TeV:data16_13TeV.00299144.physics_Main.merge.DAOD_JETM1.f698_m1594_p2623,data16_13TeV:data16_13TeV.00299147.physics_Main.merge.DAOD_JETM1.f698_m1594_p2623,data16_13TeV:data16_13TeV.00299184.physics_Main.merge.DAOD_JETM1.f698_m1594_p2623,data16_13TeV:data16_13TeV.00299241.physics_Main.merge.DAOD_JETM1.f698_m1594_p2623,data16_13TeV:data16_13TeV.00299243.physics_Main.merge.DAOD_JETM1.f698_m1594_p2623
OUTDS=data16_13TeV.earlyPeriodA

echo "Submitting to "${INDS}
echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

prun --inDS=${INDS}\
  --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
  --outputs=${OUTPUTS}\
  --exec="EtaInterCalxAODAnalysis/scripts/gridExec.sh %IN"\
  --useRootCore\
  --excludedSite=${BLACKLIST}\
  --nGBPerJob=10\
  --tmpDir=/tmp\
  --extFile=JetCalibTools/*,EventShapeTools/*\
  --mergeOutput
