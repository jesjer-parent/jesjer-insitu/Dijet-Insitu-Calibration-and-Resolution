#!/bin/bash

USERNAME=miqueits
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=EtaInterCal_3DHistos.root

for i in 0 1 2 3 4 5 6 7 8
#for i in 0 7 8
do

    INDS=group10.perf-jets.mc15_13TeV.36102${i}.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ${i}W.DAOD_JETM1.*_r6235_r6253.v20_EXT0/
    OUTDS=mc15_13TeV.36102${i}.Pythia8EvtGen_JZ${i}W_DijetInterCal_DAOD_JETM1_rel20

    echo "Submitting to "${INDS}
    echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="EtaInterCalxAODAnalysis/scripts/gridExec.sh %IN"\
         --useRootCore\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=10\
         --tmpDir=/tmp\
         --extFile=JetCalibTools/*,EventShapeTools/*\
         --mergeOutput

done                  