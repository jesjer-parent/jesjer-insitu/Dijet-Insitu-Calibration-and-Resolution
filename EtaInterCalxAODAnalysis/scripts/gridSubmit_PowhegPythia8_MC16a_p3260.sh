#!/bin/bash

USERNAME=rhankach
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=EtaInterCal_3DHistos.root
#SITE=
#ANALY_MANC_SL6_SHORT
#UKI-NORTHGRID-MAN-HEP_LOCALGROUPDISK

for i in 1 2 3 4 5 6 7 8 9
do

	  INDS=mc16_13TeV.42600${i}.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ${i}.deriv.DAOD_JETM1.e3788_s3126_r9364_r9315_p3260/
    OUTDS=mc16a_13TeV.42600${i}.PowhegPythia8.JZ${i}.JETM1_p3260
    echo "Submitting to "${INDS}
    echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}


    prun --useAthenaPackages\
         --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="EtaInterCalxAODAnalysis/scripts/gridExec_cmake.sh %IN"\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=10\
         --tmpDir=/tmp\
         --extFile=JetCalibTools/*,EventShapeTools/*\
         --mergeOutput\
         --forceStaged

done
#to specify root version : --cmtConfig=x86_64-slc6-gcc49-opt
