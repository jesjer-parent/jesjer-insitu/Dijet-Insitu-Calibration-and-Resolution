#!/usr/bin/env python

## IMPORTANT: always use EVNT datasets to extract xsecs, filter effs, etc.

## To setup
# $ setupATLAS
# $ localSetupPyAMI pyAMI-4.1.3a-x86_64-slc6
# $ ami auth
# $ voms-proxy-init -voms atlas

## To run, provide a (space-separated) list of datasets, e.g.
# ./getMCMetaDataFromAMI.py mc15_13TeV.426131.Sherpa_CT10_jets_JZ1.evgen.EVNT.e4355 mc15_13TeV.426132.Sherpa_CT10_jets_JZ2.evgen.EVNT.e4355 mc15_13TeV.426133.Sherpa_CT10_jets_JZ3.evgen.EVNT.e4355 mc15_13TeV.426134.Sherpa_CT10_jets_JZ4.evgen.EVNT.e4355 mc15_13TeV.426135.Sherpa_CT10_jets_JZ5.evgen.EVNT.e4355 mc15_13TeV.426136.Sherpa_CT10_jets_JZ6.evgen.EVNT.e4355
#./getMCMetaDataFromAMI.py mc15_13TeV.426001.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ1.evgen.EVNT.e3788 mc15_13TeV.426002.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ2.evgen.EVNT.e3788 mc15_13TeV.426003.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ3.evgen.EVNT.e3788 mc15_13TeV.426004.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ4.evgen.EVNT.e3788 mc15_13TeV.426005.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ5.evgen.EVNT.e3788 mc15_13TeV.426006.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ6.evgen.EVNT.e3788 mc15_13TeV.426007.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ7.evgen.EVNT.e3788 mc15_13TeV.426008.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ8.evgen.EVNT.e3788 mc15_13TeV.426009.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ9.evgen.EVNT.e3788
from pyAMI.client import AMIClient
from pyAMI.auth import AMI_CONFIG, create_auth_config
from pyAMI.query import get_dataset_xsec_effic,get_dataset_info
import os
import sys

datasets = sys.argv[1:]

client = AMIClient()

num_events = []
xsecs = []
filt_eff = []

for ds in datasets :
    nevents = long(get_dataset_info(client, ds).info['totalEvents'])
    number = long(get_dataset_info(client, ds).info['datasetNumber'])
    prefix = get_dataset_info(client, ds).info['generatorName']
    xsec, effic = get_dataset_xsec_effic(client, ds)

    print str(prefix)+"."+str(number)+".NEvents:      "+str(nevents)
    print str(prefix)+"."+str(number)+".XSec:         "+str(xsec)
    print str(prefix)+"."+str(number)+".FilterEff:    "+str(effic)
    #print str(prefix)+"."+str(number)+".kFacotr:      "+str(kfactor)
    print ""

    num_events.append(str(nevents))
    xsecs.append(str(xsec))
    filt_eff.append(str(effic))

#    print prefix+".NEvents:          "+' '.join(num_events)
#    print prefix+".XSec:             "+' '.join(xsecs)
#    print prefix+".FilterEff:        "+' '.join(filt_eff)
#    print ""
