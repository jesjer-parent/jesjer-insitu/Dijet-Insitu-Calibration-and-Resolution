/*
 * Author: Michaela Queitsch-Maitland <michaela.queitsch-maitland@cern.ch>
 */

#ifndef EtaInterCalxAODAnalysis_h
#define EtaInterCalxAODAnalysis_h

#include "EtaInterCalxAODAnalysis/HelperFunctions.h"
#include "EtaInterCalxAODAnalysis/DijetBalance.h"

namespace DijetInSitu {

class EtaInterCalxAODAnalysis {

public:

	// Constructor
	EtaInterCalxAODAnalysis();
	// Destructor
	~EtaInterCalxAODAnalysis();

	// Initialise
	void Initialise(xAOD::TEvent& event,std::vector<TString> otf_settings,TString ofn,TString jetAlgo);
	// Per event analysis
	void AnalyseEvent(xAOD::TEvent& event, std::string jetAlgo);
	// Per event analysis at truth level
	void AnalyseTruthEvent(xAOD::TEvent& event, std::string jetAlgo);
	// Per event analysis
	void ProcessEvent(xAOD::TEvent& event, TString jetAlgo);
	// Perform tag and probe analysis of jet triggers
	void TagAndProbeAnalysis(xAOD::TEvent& event, TString jetAlgo);
	// Finalise
	void Finalise();

	void debug(std::string msg){
		if ( m_debug ) {
			std::cout << "   DEBUG INFO:  " << msg << std::endl;
			return;
		}
		else return;
	}

private:
	bool	CheckAtLeastOneJetInEtaRange(float etaMin,float etaMax, DijetBalance dijet);
	xAOD::TEvent* m_event;

	float m_refRegion;
	float m_centralRegion;

	TEnv* m_settings;
	std::vector<TString> m_triggers;

	bool m_isMC;
	bool m_isData;
	uint m_mcChannelNumber;
	uint m_runNumber;
  unsigned long long int m_eventNumber;

  bool m_isR21;

	// trigger info
	TString m_trigMenu;
	std::vector<TString> m_trigs;
	std::vector<TString> m_trigsCtrl;
	std::vector<TString> m_trigsFwd;
	std::vector<TString> m_trigORs;
	std::vector<double>  m_trigThresholds;
	std::vector<TString> m_tagAndProbeTrigs;

	// classification of data events ("RefFJ_J" or "RefFJ_FJ")
	// based on trigger flags and emulated decisions
	//    std::string m_trigClass;
	int m_nStrangeTrigBits; // number of events for which emulated decision for all central + forward jets is 0

	TRandom3* m_random;

	// flags for cuts
	bool m_debug;
	bool m_tagAndProbe;
	bool m_truth_daod;
	bool m_controlPlots;
	bool m_applyGRL;
	bool m_applyTrigger;
	bool m_applyPileupReweighting;
	bool m_applyJetCleaning;
	bool m_applyJetCleaningEvent;
	bool m_applyBadBatmanCleaning;
	bool m_applyJetCalib;
	bool m_EMscale;
	bool m_applyJVF;
	bool m_applyJVT;
	bool m_applyDphijj;
	bool m_applyJ3pT;
	bool m_applyJ3Cleaning;
	bool m_vetoPathEvnts;
	bool m_applyMCCleaning;
	bool m_calibDataAsMC;
	bool m_applyDijetSelection;
	bool m_ctrlTrigsOnly;
	bool m_fwdTrigsOnly;
	bool m_vetoJetsInBadEta;
	bool m_vetoEventsInBadEta;
	bool m_doTriggerEmulation;
	// flags for systematic shifts
	bool m_doNominal;
	bool m_doSystematics;
	bool m_doDphiShiftUp;
	bool m_doDphiShiftDown;
	bool m_doJet3PtFracShiftUp;
	bool m_doJet3PtFracShiftDown;
	bool m_doJVTCutTight;
	bool m_runLightWeight;
	bool m_enforceRefFJ_FJEta;
	bool m_centralAsDefaultTriggerPrefrence;
	bool m_discardTriggerOverlapEvents;
	std::vector<double> m_badEtaRange;

	TString m_pileupSuppMethod; // JVF or JVT?

			// dijet selection
	float m_jvfCut;
	std::map<TString,float> m_jvtCut;
	float m_dphijjCut;
	float m_j3pTCut;
	float m_DRcut;
	float m_j1_JVT, m_j2_JVT;
	//systematic cuts
	float m_dphijjUpCut;
	float m_dphijjDownCut;
	std::map<TString,float> m_jvtCutTight;
	std::map<TString,float> m_jvtCutLoose;
	float m_j3pTCutUp;
	float m_j3pTCutDown;

	std::vector<TString> m_jetCollections; // jet collections to loop over
	std::vector<TString> m_truthJetCollections; // truth jet collections to loop over
  TString m_HLTJetContainer;

	// Tools
  std::string m_MainDirectory; // path to $TestArea if cmake or $ROOTCOREBIN if rc
	std::map<TString,JetCalibrationTool*> m_jetCalibTools;
	JetCalibrationTool* m_jetCalibTool;
	std::map<std::string,JetCleaningTool*>  m_jetCleaningTools;
	JetCleaningTool*  m_jetCleaningTool;
	GoodRunsListSelectionTool* m_grlTool;
	TrigConf::xAODConfigTool* m_trigConfTool;
	Trig::TrigDecisionTool* m_trigDecTool;
	JetVertexTaggerTool* m_jvtTool;
	//    CP::IPileupReweightingTool* m_pileupReweightingTool;
	asg::AnaToolHandle<CP::IPileupReweightingTool> m_pileupReweightingTool;
	// ToolHandle<CP::IPileupReweightingTool> m_pileupReweightingTool;
	//CP::PileupReweightingTool* m_pileupReweightingTool;
	//    EventShapeCopier* m_eventShapeCopier;

  asg::AnaToolHandle<IJetUpdateJvt> m_updateJvt{"JetVertexTaggerTool/UpdateJVT"};

	void InitialiseTools();
	void InitialiseGRL();
	void InitialiseJetTools();
	void InitialiseCutflow(std::string prefix);
	void InitialiseTagAndProbeHists();
	void InitialiseHistograms(std::string prefix);
	void InitialiseControlHistograms(std::string prefix);
	void FillControlHistograms(std::string prefix, std::string jetAlgo, float pTavg, float Dphijj, const xAOD::Jet& j1,const xAOD::Jet& j2, xAOD::Jet* jet3);
	void CreateHistograms(std::string prefix);
	void InitialiseTrigHists(std::string prefix);
	void InitialiseJetCollectionTrees(std::string jetCollection);
	void InitialiseMiniTree();
	const xAOD::Vertex* GetPV(const xAOD::VertexContainer* pVertices);
	bool passJVF(const xAOD::Jet& j);
	bool passJVT(const xAOD::Jet& j, float cut);
	bool isPathalogicalEvent(const xAOD::EventInfo* eventInfo);
	bool isBadDataLumiBlock(const xAOD::EventInfo* eventInfo);
	bool PassAnyTrigger(const std::vector<TString> &trigs);
	bool DoesTriggerMatch( xAOD::TEvent& event, xAOD::TStore &store,TString centralTriggerName, int itrig, TString jetAlgo="AntiKt4EMTopo");
	std::string TriggerClassification(TString trig);

	void SelectOfflineJets(xAOD::TEvent& event, xAOD::TStore& store, xAOD::JetContainer &selJets, bool &badEvent, TString jetAlgo);
	void SelectOnlineJets(xAOD::TEvent& event, xAOD::TStore& store, xAOD::JetContainer &selJets, float ET_cut, bool forceCentral);
	bool matchHLTJet(const xAOD::Jet& jet,const xAOD::JetContainer onlineJets,float DRcut);
	bool matchL1RoI(const xAOD::Jet& jet,const xAOD::JetRoIContainer *L1RoIs,float ETcut, float DRcut);

	void FillHistograms(std::string prefix,DijetBalance dijet, int pTbin);
	void FillTrigHists(xAOD::TEvent& event, std::string trig);
	float getJVT(const xAOD::Jet& j);
	void WriteOutput();
	void WriteHistograms();
	void WriteMiniTree();

	// Trigger emulation
	std::pair<int,int> EmulateTriggerDecisions2015(const xAOD::JetContainer* trig_jets_HLT, const xAOD::JetRoIContainer* trig_jets_L1, bool useL1Seeds);
  std::pair<int,int> EmulateTriggerDecisionsRun2(const xAOD::JetContainer* trig_jets_HLT, const xAOD::JetRoIContainer* trig_jets_L1, bool useL1Seeds);
	bool PassHLT(float ptcut,const xAOD::JetContainer &trig_jets_HLT, bool isCentral);
	bool PassL1(float ptcut,const xAOD::JetRoIContainer &trig_jets_L1, bool isCentral);

	// truth mini-tree
	TTree* m_truth_tree;

	// mini-tree
	TTree* m_mini_tree;
	std::map<std::string,TTree*> m_jetCollectionTrees;
	int m_trigBitsFwd;
	int m_trigBitsCtrl;
	bool m_forceMCTrigs;
	bool m_pass_HLT_j15;
	bool m_pass_HLT_j15_320eta490;
        bool m_pass_HLT_j25;
        bool m_pass_HLT_j25_320eta490;
        bool m_pass_HLT_j35;
        bool m_pass_HLT_j35_320eta490;
        bool m_pass_HLT_j45;
        bool m_pass_HLT_j45_320eta490;
	bool m_pass_HLT_j60;
        bool m_pass_HLT_j60_320eta490;
        bool m_pass_HLT_j85;
        bool m_pass_HLT_j110;
	bool m_pass_HLT_j110_320eta490;
        bool m_pass_HLT_j175;
        bool m_pass_HLT_j175_320eta490;
	bool m_pass_HLT_j260;
	bool m_pass_HLT_j260_320eta490;
        bool m_pass_HLT_j360;
	bool m_pass_HLT_j360_320eta490;
        bool m_pass_HLT_j360_a10_lcw_sub_L1J100;
	bool m_pass_HLT_j420_a10_lcw_L1J100;
        bool m_pass_HLT_j460_a10_lcw_subjes_L1J100;
        bool m_pass_HLT_j400;
        bool m_pass_HLT_j400_320eta490;
	bool m_passRaw_L1_J15;
        bool m_passRaw_L1_J15_31ETA49;
	bool m_pass_HLT_j0_perf_L1RD0_FILLED;

	//pass systematic variation
	bool m_pass_Nominal;
	bool m_pass_DphiUp;
	bool m_pass_DphiDown;
	bool m_pass_JVTThight;
	bool m_pass_JVTLoose;
	bool m_pass_J3pTCutUp;
	bool m_pass_J3pTCutDown;

	float m_weight;
        float m_weight_pileup;
	int m_JX;
	float m_pTavg_truth; float m_Dphijj_truth;
	float m_j1_pT_truth, m_j1_eta_truth, m_j1_phi_truth;
	float m_j2_pT_truth, m_j2_eta_truth, m_j2_phi_truth;
	float m_j3_pT_truth, m_j3_eta_truth, m_j3_phi_truth;
	bool m_passDijetCuts_truth;
	float m_pTavg;
	float m_j1_pT, m_j1_eta, m_j1_phi, m_j1_E;
	bool m_j1_passJVT, m_j1_passJVF, m_j1_passCleaning;
	float m_j2_pT, m_j2_eta, m_j2_phi, m_j2_E;
	bool m_j2_passJVT, m_j2_passJVF, m_j2_passCleaning;
	float m_j3_pT, m_j3_eta, m_j3_phi, m_j3_E;
	bool m_j3_passJVT, m_j3_passJVF, m_j3_passCleaning;
	float m_Asym_MM, m_Asym_SM;
	float m_ref_pT, m_ref_eta, m_ref_phi;
	float m_probe_pT, m_probe_eta, m_probe_phi;
	float m_left_pT, m_left_eta, m_left_phi;
	float m_right_pT, m_right_eta, m_right_phi;
	float m_actualIntPerXing, m_avgIntPerXing;
        float m_correct_mu;
	float m_NPV;
	float m_rhoEM,m_rhoLC;
        int m_lastEmptyBunchCrossing;
        int m_lastUnpairedBunchCrossing;
	float m_pT3OverpTAvg;
	bool m_passTrigger;
	bool m_requireOneJetForward;
	bool m_atLeasteOneForwardJet;

	TString m_trigClass;
	TString m_trigHLT;
	int m_bcid;
	float m_Dphijj;

	TTree* m_trig_mini_tree;
	float m_HLT_pT;
	float m_HLT_eta;
	float m_HLT_phi;
	float m_L1_et8x8;
	float m_L1_eta;
	float m_L1_phi;
	uint m_L1_word;

	// Histograms
	//    TH1F* m_cutFlow;
	std::map<std::string,TH1F*> m_cutFlow;
	TH1F* m_triggerCounts;
	std::map<std::string,TH1F*> m_hists1D;
	std::map<std::string,TH2F*> m_hists2D;
	std::map<std::string,TH3F*> m_hists3D;

	std::map<std::string,TH1F*> m_trigHists;
	//	std::map<std::string,TTree*> m_JetVariableTree;
	// Output file
	TFile* m_fout;

};
}

#endif
