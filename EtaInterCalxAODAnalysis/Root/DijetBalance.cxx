/*
 * Author: Michaela Queitsch-Maitland <michaela.queitsch-maitland@cern.ch>
 */

#include "EtaInterCalxAODAnalysis/DijetBalance.h"

// Constructor                                                                                                                                                               
DijetInSitu::DijetBalance::DijetBalance(const xAOD::JetContainer& jets, float refRegion, bool truthJets) { 
  Balance(jets,refRegion,truthJets);
  return;
 }
// Destructor                                                                                                                                                                
DijetInSitu::DijetBalance::~DijetBalance(){}

// Calculate dijet balance variables                                                                                                                                         
void DijetInSitu::DijetBalance::Balance(const xAOD::JetContainer& jets, float refRegion, bool truthJets){

  const xAOD::Jet& jet1 = *(jets.at(0));
  const xAOD::Jet& jet2 = *(jets.at(1));

  float eta1, eta2;
  if ( truthJets ) {
    eta1 = jet1.eta(); eta2 = jet2.eta();
  }
  else {
    eta1 = jet1.auxdata<float>("DetectorEta"); eta2 = jet2.auxdata<float>("DetectorEta");
  }

  //  float eta1 = jet1.auxdata<float>("DetectorEta"); float eta2 = jet2.auxdata<float>("DetectorEta");
  m_jetInRef = false;

  m_probe_eta=-99., m_ref_eta = -99.;
  m_probe_pT=-99., m_ref_pT = -99.;

  // standard method
  if ( fabs(eta1)<refRegion ) {
    m_jetInRef = true; m_ref_eta = eta1; m_ref_pT = jet1.pt(); m_probe_eta = eta2; m_probe_pT = jet2.pt();
  }
  else if ( fabs(eta2)<refRegion ) {
    m_jetInRef = true; m_ref_eta = eta2; m_ref_pT = jet2.pt(); m_probe_eta = eta1; m_probe_pT = jet1.pt();
  }

  // matrix method
  m_right_eta = eta1 > eta2 ? eta1 : eta2;
  m_right_pT = eta1 > eta2 ? jet1.pt() : jet2.pt();
  m_left_eta = eta1 < eta2 ? eta1 : eta2;
  m_left_pT = eta1 < eta2 ? jet1.pt() : jet2.pt();

  m_pTavg = (jet1.pt()+jet2.pt())/2.0;

  m_Asym_SM = m_jetInRef ? ( m_probe_pT - m_ref_pT )/m_pTavg : -999;
  m_Asym_MM = ( m_left_pT - m_right_pT )/m_pTavg;

	//third jet variables
	if(jets.size() > 2){
		m_thirdJetExists = true;
	  const xAOD::Jet& jet3 = *(jets.at(2));
		m_j3_pT = jet3.pt();
		m_j3_eta = jet3.eta();
		m_j3_phi = jet3.phi();
	}else{	
		m_thirdJetExists = true;
		m_j3_pT = -99;
		m_j3_eta = -99;
		m_j3_phi = -99;
	}

  return;
}

//
bool DijetInSitu::DijetBalance::thirdJetExists() { return m_thirdJetExists; }
float DijetInSitu::DijetBalance::thirdJet_pT() { return m_j3_pT; }
float DijetInSitu::DijetBalance::thirdJet_eta() { return m_j3_eta; }
float DijetInSitu::DijetBalance::thirdJet_phi() { return m_j3_phi; }

float DijetInSitu::DijetBalance::pTavg() { return m_pTavg; }

float DijetInSitu::DijetBalance::Asym_SM() { return m_Asym_SM; }

float DijetInSitu::DijetBalance::Asym_MM() { return m_Asym_MM; }

float DijetInSitu::DijetBalance::right_eta() { return m_right_eta; }
float DijetInSitu::DijetBalance::right_pT() { return m_right_pT; }
float DijetInSitu::DijetBalance::right_phi() { return m_right_phi; }

float DijetInSitu::DijetBalance::left_eta() { return m_left_eta; }
float DijetInSitu::DijetBalance::left_pT() { return m_left_pT; }
float DijetInSitu::DijetBalance::left_phi() { return m_left_phi; }

float DijetInSitu::DijetBalance::probe_eta() { return m_probe_eta; }
float DijetInSitu::DijetBalance::probe_pT() { return m_right_pT; }
float DijetInSitu::DijetBalance::probe_phi() { return m_right_phi; }

float DijetInSitu::DijetBalance::ref_eta() { return m_ref_eta; }
float DijetInSitu::DijetBalance::ref_pT() { return m_ref_pT; }
float DijetInSitu::DijetBalance::ref_phi() { return m_ref_phi; }

bool DijetInSitu::DijetBalance::jetInRefRegion() { return m_jetInRef; }

