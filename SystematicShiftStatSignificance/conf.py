
ptbins=['25to40', '40to60','60to85','85to115','115to145','145to175','175to220','220to270','270to330','330to400','400to525','525to760','760to1200']
maxetabins=-4.5, -3.6, -3.4, -3.0, -2.8, -2.6, -2.5, -2.4, -2.3, -2.1, -1.8, -1.5, -1.2, -0.8, -0.4, 0.0, 0.4, 0.8, 1.2, 1.5, 1.8, 2.1, 2.3, 2.4, 2.5, 2.6, 2.8, 3.0, 3.4, 3.6, 4.5
#maxetabins=-4.5,-3.6,-3.2,-2.8,-2.4,-2.1,-1.8,-1.5,-1.2,-0.8,-0.3,0.0,0.3,0.8,1.2,1.5,1.8,2.1,2.4,2.8,3.2,3.6,4.5
shifts=["closure"]
#["syst_dphiDown", "syst_jvtTight", "syst_j3down","syst_dphiUp",  "syst_dphiDown",  "syst_j3up"]
jetAlgo="AntiKt4LCTopo"
method = "MM"
nToys = 768

#,"DphiShiftDown", "DphiShiftUp" ,"Jet3PtFracShiftDown","Jet3PtFracShiftUp" ]
