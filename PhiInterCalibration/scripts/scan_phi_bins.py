#!/bin/python

from subprocess import call,Popen
from math import pi
import sys

# settings for scanning
bin_width = pi/14  # width of bins (detector geometry)
#bin_width = pi/7  # width of bins (detector geometry)
#n_points =  14     # number of scan points
n_points =  4     # number of scan points
#n_points =  28     # number of scan points
offset = 0         # offset

for n in range(0,n_points+1):
    print offset

    out = open("logs/PhiInterCal_Data_offset"+str(offset)+".log","wb")
    Popen(["PhiInterCal","--inFile","/pc2012-data2/mqueitsch/jetetmiss/dijet-intercalibration/PhiInterCalibration/periodACDEFGHIJ.root","--outFile","/pc2012-data2/mqueitsch/jetetmiss/phi-intercalibration/results/data/PhiInterCal_Data_Offset"+str(offset),"--offset",str(offset),"--isData"],stdout=out,stderr=out)

    # out = open("logs/PhiInterCal_PowhegPythia8_offset"+str(offset)+".log","wb")
    # Popen(["PhiInterCal","--inFile","/pc2012-data2/mqueitsch/jetetmiss/dijet-intercalibration/PowhegPythia8_50ns_PhiInterCal/PowhegPythia8.root","--outFile","/pc2012-data2/mqueitsch/jetetmiss/phi-intercalibration/results/mc/PhiInterCal_PowhegPythia8_Offset"+str(offset),"--offset",str(offset),"--isMC"],stdout=out,stderr=out)

    offset += bin_width/n_points
