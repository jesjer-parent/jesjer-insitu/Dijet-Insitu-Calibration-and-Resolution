#include "TMath.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TGraphErrors.h"
#include "TGraph2DErrors.h"
#include "Utils.h"

class JES_Smoother : public TNamed {
public:
  
  typedef TGraphErrors Graph;
  typedef TGraph2DErrors Graph2D;
  typedef vector<double> VecD;    
  
  bool useGraduatedLowPtSmoothing = false;

  double widthy_original = 0.8, widthx_original = 0.8;
  //
  //  Constructor. 
  //  Parameters:
  //  - Relative width of Gaussina kernel: DeltaPt/Pt
  //  - how many steps to use in numberical integration
  //  - how wide range to integrate over
  JES_Smoother(double widthx=0.08, double widthy=0.08)
    : m_useError(false), m_logx(true), m_logy(true), m_isSyst(false), m_isSM(false)
  { m_width_x=widthx; m_width_y=widthy; widthy_original=widthy; widthx_original=widthx;}
  ~JES_Smoother() {};
  
  void error(TString msg) { 
    printf("\nJES Smoother ERROR:\n\n  %s\n\n",msg.Data()); abort(); 
  };

  inline void SetEtaBins(VecD etaBins) { m_etaBins.clear(); m_etaBins = etaBins; }
  inline void UseBinUncertainty(bool doIt=true) { m_useError=doIt; };
  inline void IsSyst(bool doIt=true) { m_isSyst=doIt; };
  inline void IsSM(bool doIt=true) { m_isSM=doIt; };
  inline void SetLog(bool logx=true, bool logy=true) { m_logx=logx; m_logy=logy; }
  inline void SetKernelWidth(double width, double widthy=-1) {
    m_width_x=width; m_width_y = (widthy==-1?width:widthy);
  }


  TH1D *SmoothGraph2D(Graph2D *g);
  TH1D *SmoothHisto2D(TH2 *histo); 
  TH1D *SmoothPtHisto(TH1 *histo) { SetLog(); return SmoothHisto(histo); }
  TH1D *SmoothEtaHisto(TH1 *histo) { SetLog(false); return SmoothHisto(histo); }
  TH1D *SmoothPtEtaHisto(TH2 *histo) { SetLog(false,true); return SmoothHisto2D(histo); }

  
  VecD MakeLogVector(int N, double min, double max) {
    VecD vec; double dx=(log(max)-log(min))/N;
    for (int i=0;i<=N;++i) vec.push_back(exp(log(min)+i*dx));
    return vec;
  }

  VecD MakeLinearVector(int N, double min, double max) {
    VecD vec; double dx=(max-min)/N;
    for (int i=0;i<=N;++i) vec.push_back(min+dx*i); return vec;
  }

  double GetSmoothedValue(Graph *g, double x) {
    if (m_logx&& x<1) error("Use SetLog(false) for this input");
    double sumw=0, sumwy=0;
    for (int i=0; i<g->GetN();++i) {
      double xi=g->GetX()[i], yi=g->GetY()[i];
      if(yi==0) continue;
      double dx = (x-xi)/m_width_x;
      if (m_logx&& xi<1) error("The use of log-widht doesn't make sense for this input!");
      if (m_logx) dx /= x;
      double wi = TMath::Gaus(dx); 
      if (m_useError) wi *= 1.0/pow(g->GetEY()[i],2);
      sumw += wi; sumwy += wi*yi;
       
    }
    return sumwy/sumw;
  }

  double GetSmoothedValue(Graph2D *g, double x, double y) {
    //    std::cout << "SMOOTHING" << std::endl;
    double sumw=0, sumwz=0;
    for (int i=0; i<g->GetN();++i) {
      double xi=g->GetX()[i], yi=g->GetY()[i], zi=g->GetZ()[i];      
      //      std::cout << "xi " << xi << " yi " << yi << " zi " << zi << std::endl;
      double dx = (x-xi)/m_width_x; if (m_logx) dx /= x;
      // USED for AntiKt6TopoEM
      //if (x<40) { dx = (x-xi)/m_width_x/( 9-0.2*x ); if (m_logx) dx /= x; }
      double dy = (y-yi)/m_width_y; if (m_logy) dy /= y;

      //      std::cout << "dx " << dx << " dy " << dy << std::endl;
      dy=0;

      double dr = sqrt(dx*dx+dy*dy);
      double wi = TMath::Gaus(dr);
      //      std::cout << "dr " << dr << " wi " << wi << std::endl;
      //      std::cout << "EZ " << g->GetEZ()[i] << std::endl;
      if (m_useError) wi *= 1.0/pow(g->GetEZ()[i],2);
      sumw += wi; sumwz += wi*zi;
      //      std::cout << "sumw " << sumw << " sumwz " << sumwz << std::endl;
    }
    //    std:: cout << sumwz << "\t" << sumw << std::endl;

    //    std::cout << "m_width_x " << m_width_x << std::endl;

    return sumwz/sumw;
  }

  double GetSmoothedStat(Graph2D *g, int myPoint, double x, double y) {
    double sumw=0, sumwz=0;
    for (int i=0; i<g->GetN();++i) {
      double xi=g->GetX()[i], yi=g->GetY()[i];
      double dx = (x-xi)/m_width_x; if (m_logx) dx /= x;
      double dy = (y-yi)/m_width_y; if (m_logy) dy /= y;
      double dr = sqrt(dx*dx+dy*dy);
      double wi = TMath::Gaus(dr);
      double zi = i==myPoint?g->GetEZ()[i]:0;
      sumw += wi; sumwz += wi*zi;
    }
    return sumwz/sumw;
  }
  
  
  TH1D *SmoothHisto(TH1 *h, double etaMax=-99) {
    return SmoothGraph(new Graph(h),etaMax);
  }

  TH1D *SmoothGraph(Graph *g, double etaMax=-99) {
    static int n_smooth=0;
    TH1D *h;
    if (0) {
      VecD bins=MakeLogVector(100,15,1500);
      h = new TH1D(Form("smooth%d",n_smooth++),"",100,&bins[0]);
    }

    VecD etabins;
    VecD etabinsUni;

    if( m_etaBins[0] < 0.0 ){
      etabins.push_back(m_etaBins[0]);
      etabins.push_back(m_etaBins[1]);
      etabinsUni = MakeUniformVecD(60, -3.0, 3.0);
    }else{
      etabinsUni = MakeUniformVecD(30, 0.0, 3.0);
    }

    for(int i=0; i<etabinsUni.size(); i++)
      etabins.push_back(etabinsUni[i]);

    etabins.push_back(m_etaBins[m_etaBins.size()-2] );
    etabins.push_back(m_etaBins[m_etaBins.size()-1] );
    
    for(int ieta=0; ieta<etabins.size(); ieta++) cout<<"eta     : "<<etabins[ieta]<<endl;

    h = new TH1D(Form("smooth%d",n_smooth++),"",etabins.size()-1,&etabins[0]);

    for (int ibin=1;ibin<=h->GetNbinsX();++ibin) {
      
      double x=h->GetBinCenter(ibin);
      double myEta=x;
      if ( etaMax!=-99 && fabs(x) > etaMax ) {
	if ( x>0 )  myEta = h->GetXaxis()->GetBinCenter(h->GetXaxis()->FindBin(etaMax-1e-6));
	else        myEta = h->GetXaxis()->GetBinCenter(h->GetXaxis()->FindBin(-etaMax+1e-6));	
      }
      
      double y=GetSmoothedValue(g,x);
      h->SetBinContent(ibin,GetSmoothedValue(g,myEta));
    }
    return h;
  }

  TH2D *MakePtEtaHisto(TString hname) {

    int NptBins=100; 
    VecD ptbins=MakeLogVector(NptBins,15,2500);
    int NetaBins=m_etaBins.size()-1;
    TH2D *h = new TH2D(hname,"",NptBins,&ptbins[0],NetaBins,&m_etaBins[0]);

    h->SetXTitle("jet p_{T} [GeV]"); h->SetYTitle("jet #eta_{det}");
    return h;
  }

  TH2D *MakePtPhiHisto(TString hname) {

    int NptBins=100; 
    VecD ptbins=MakeLogVector(NptBins,15,2500);
    int NetaBins=m_etaBins.size()-1;
    TH2D *h = new TH2D(hname,"",NptBins,&ptbins[0],NetaBins,&m_etaBins[0]);

    h->SetXTitle("jet p_{T} [GeV]"); h->SetYTitle("jet #phi_{det}");
    return h;
  }
  
  TH2D *SmoothPtEtaGraph(Graph2D *g2d, TString hname, bool iscal, double pTmin=-99, double etaMax=-99) { 

    SetLog(true,false); //SetKernelWidth(0.2,0.12);
    //std::cout << "Making hist " << hname << std::endl;
    TH2D *h = MakePtEtaHisto(hname);
    //    h->Print();
    //std::cout << h->GetNbinsX() << std::endl;
    //std::cout << h->GetNbinsY() << std::endl;
    
    for (int ieta=1;ieta<=h->GetNbinsY();++ieta)
      for (int ipt=1;ipt<=h->GetNbinsX();++ipt) {
       
	double eta=h->GetYaxis()->GetBinCenter(ieta);
	double pt=h->GetXaxis()->GetBinCenter(ipt);

	//std::cout << ieta << "\t" << eta << std::endl;
	//std::cout << ipt << "\t" << pt << std::endl;
	
	m_width_x = widthx_original;
	m_width_y = widthy_original;

	//	std::cout << "useGraduatedLowPtSmoothing ? " << useGraduatedLowPtSmoothing << std::endl;
	//	std::cout << "m_isSyst ? " << m_isSyst << std::endl;

	if(useGraduatedLowPtSmoothing){
	  if(!m_isSyst){    
	    /*
	    if(pt<35)                  { m_width_x=0.6; m_width_y=0.18; }
	    else if( pt>=35 && pt<40)  { m_width_x=0.55; m_width_y=0.16; }
	    else if( pt>=40 && pt<45 ) { m_width_x=0.5; m_width_y=0.145; }
	    else if( pt>=45 && pt<46 ) { m_width_x=0.49; m_width_y=0.135; }
	    else if( pt>=46 && pt<47 ) { m_width_x=0.48; m_width_y=0.132; }
	    else if( pt>=47 && pt<48 ) { m_width_x=0.47; m_width_y=0.13; }
	    else if( pt>=48 && pt<49 ) { m_width_x=0.46; m_width_y=0.125; }
	    else if( pt>=49 && pt<56 ) { m_width_x=0.45; m_width_y=0.12; }
	    else if( pt>=56 && pt<57 ) { m_width_x=0.44; m_width_y=0.12; }
	    else if( pt>=57 && pt<58 ) { m_width_x=0.43; m_width_y=0.12; }
	    else if( pt>=58 && pt<59 ) { m_width_x=0.42; m_width_y=0.12; }
	    else if( pt>=59 && pt<60 ) { m_width_x=0.41; m_width_y=0.12; }
	    else if( pt>=60 && pt<61 ) { m_width_x=0.40; m_width_y=0.12; }
	    else if( pt>=61 && pt<62 ) { m_width_x=0.39; m_width_y=0.12; }
	    else if( pt>=62 && pt<63 ) { m_width_x=0.38; m_width_y=0.12; }
	    else if( pt>=63 && pt<64 ) { m_width_x=0.37; m_width_y=0.12; }
	    else if( pt>=64 && pt<65 ) { m_width_x=0.36; m_width_y=0.12; }
	    else if( pt>=65 && pt<66 ) { m_width_x=0.35; m_width_y=0.12; }
	    else if( pt>=66 && pt<67 ) { m_width_x=0.34; m_width_y=0.12; }
	    else if( pt>=67 && pt<68 ) { m_width_x=0.33; m_width_y=0.12; }
	    else if( pt>=68 && pt<69 ) { m_width_x=0.32; m_width_y=0.12; }
	    else if( pt>=69 && pt<70 ) { m_width_x=0.29; m_width_y=0.12; }
	    */

	    if(pTmin<35){
	      if(pt<35)                  { m_width_x=0.58;   m_width_y=0.16; }
	      else if( pt>=35 && pt<36)  { m_width_x=0.58;  m_width_y=0.16; }
	      else if( pt>=36 && pt<37)  { m_width_x=0.57;  m_width_y=0.155; }
	      else if( pt>=37 && pt<38)  { m_width_x=0.56;  m_width_y=0.152; }
	      else if( pt>=38 && pt<39)  { m_width_x=0.55;  m_width_y=0.15; }
	      else if( pt>=39 && pt<40)  { m_width_x=0.54;  m_width_y=0.149; }
	      else if( pt>=40 && pt<41)  { m_width_x=0.53;  m_width_y=0.148; }
	      else if( pt>=41 && pt<42)  { m_width_x=0.52;  m_width_y=0.147; }
	      else if( pt>=42 && pt<43)  { m_width_x=0.51;  m_width_y=0.143; }
	      else if( pt>=43 && pt<44)  { m_width_x=0.505; m_width_y=0.138; }
	      else if( pt>=44 && pt<45)  { m_width_x=0.5;   m_width_y=0.136; }
	      else if( pt>=45 && pt<46 ) { m_width_x=0.49;  m_width_y=0.134; }
	      else if( pt>=46 && pt<47 ) { m_width_x=0.48;  m_width_y=0.132; }
	      else if( pt>=47 && pt<48 ) { m_width_x=0.47;  m_width_y=0.13; }
	      else if( pt>=48 && pt<49 ) { m_width_x=0.46;  m_width_y=0.125; }
	    }else if(pTmin>=35&&pTmin<40){
	      if( pt<40)  { m_width_x=0.54;  m_width_y=0.149; }
	      else if( pt>=40 && pt<41)  { m_width_x=0.53;  m_width_y=0.148; }
	      else if( pt>=41 && pt<42)  { m_width_x=0.52;  m_width_y=0.147; }
	      else if( pt>=42 && pt<43)  { m_width_x=0.51;  m_width_y=0.143; }
	      else if( pt>=43 && pt<44)  { m_width_x=0.505; m_width_y=0.138; }
	      else if( pt>=44 && pt<45)  { m_width_x=0.5;   m_width_y=0.136; }
	      else if( pt>=45 && pt<46 ) { m_width_x=0.49;  m_width_y=0.134; }
	      else if( pt>=46 && pt<47 ) { m_width_x=0.48;  m_width_y=0.132; }
	      else if( pt>=47 && pt<48 ) { m_width_x=0.47;  m_width_y=0.13; }
	      else if( pt>=48 && pt<49 ) { m_width_x=0.46;  m_width_y=0.125; }
	    }else if(pTmin>=40&&pTmin<45){
	      if( pt<45)  { m_width_x=0.5;   m_width_y=0.136; }
	      else if( pt>=45 && pt<46 ) { m_width_x=0.49;  m_width_y=0.134; }
	      else if( pt>=46 && pt<47 ) { m_width_x=0.48;  m_width_y=0.132; }
	      else if( pt>=47 && pt<48 ) { m_width_x=0.47;  m_width_y=0.13; }
	      else if( pt>=48 && pt<49 ) { m_width_x=0.46;  m_width_y=0.125; }
	    }else if(pTmin>=45&&pTmin<46){
	      if( pt<46 ) { m_width_x=0.49;  m_width_y=0.134; }
	      else if( pt>=46 && pt<47 ) { m_width_x=0.48;  m_width_y=0.132; }
	      else if( pt>=47 && pt<48 ) { m_width_x=0.47;  m_width_y=0.13; }
	      else if( pt>=48 && pt<49 ) { m_width_x=0.46;  m_width_y=0.125; }
	    }else if(pTmin>=46&&pTmin<47){
	      if( pt<47 ) { m_width_x=0.48;  m_width_y=0.132; }
	      else if( pt>=47 && pt<48 ) { m_width_x=0.47;  m_width_y=0.13; }
	      else if( pt>=48 && pt<49 ) { m_width_x=0.46;  m_width_y=0.125; }
	    }else if(pTmin>=47&&pTmin<48){
	      if( pt<48 ) { m_width_x=0.47;  m_width_y=0.13; }
	      else if( pt>=48 && pt<49 ) { m_width_x=0.46;  m_width_y=0.125; }
	    }else if(pTmin>=48&&pTmin<49){
	      if( pt<49 ) { m_width_x=0.46;  m_width_y=0.125; }
	    }

	    
	    if( pt>=49 && pt<56 ) { m_width_x=0.45; m_width_y=0.12; }
	    else if( pt>=56 && pt<57 ) { m_width_x=0.44; m_width_y=0.12; }
	    else if( pt>=57 && pt<58 ) { m_width_x=0.43; m_width_y=0.12; }
	    else if( pt>=58 && pt<59 ) { m_width_x=0.42; m_width_y=0.12; }
	    else if( pt>=59 && pt<60 ) { m_width_x=0.41; m_width_y=0.12; }
	    else if( pt>=60 && pt<61 ) { m_width_x=0.40; m_width_y=0.12; }
	    else if( pt>=61 && pt<62 ) { m_width_x=0.39; m_width_y=0.12; }
	    else if( pt>=62 && pt<63 ) { m_width_x=0.38; m_width_y=0.12; }
	    else if( pt>=63 && pt<64 ) { m_width_x=0.37; m_width_y=0.12; }
	    else if( pt>=64 && pt<65 ) { m_width_x=0.36; m_width_y=0.12; }
	    else if( pt>=65 && pt<66 ) { m_width_x=0.35; m_width_y=0.12; }
	    else if( pt>=66 && pt<67 ) { m_width_x=0.34; m_width_y=0.12; }
	    else if( pt>=67 && pt<68 ) { m_width_x=0.33; m_width_y=0.12; }
	    else if( pt>=68 && pt<69 ) { m_width_x=0.32; m_width_y=0.12; }
	    else if( pt>=69 && pt<70 ) { m_width_x=0.29; m_width_y=0.12; }

	  }
	}

	double myPt = pt;
	if ( pTmin!=-99 && pt < pTmin )  {
	  myPt = h->GetXaxis()->GetBinCenter(h->GetXaxis()->FindBin(pTmin+1e-6));
	}
	double myEta=eta;
	
	if ( etaMax!=-99 && fabs(eta) > etaMax ){ 
	  if ( eta>0 )  myEta = h->GetYaxis()->GetBinCenter( h->GetYaxis()->FindBin(etaMax-1e-6) );
	  else          myEta = h->GetYaxis()->GetBinCenter( h->GetYaxis()->FindBin(-etaMax+1e-6) );	  	 	  
	}

	//std::cout << myPt << std::endl;
	//std::cout << myEta << std::endl;

	double R=GetSmoothedValue(g2d,myPt,myEta);

	//	std::cout << R << std::endl;
	
	//if(pt<45&&fabs(eta)>3.0) {
	//cout<<" -------------------- pT min = "<<pTmin<<endl;
	//printf(" (eta,pt)=(%7.3f,%5.0f), R = %.3f - used (eta,pt): (%7.3f,%5.0f)\n",eta,pt,R,myEta,myPt);
	//}


	h->SetBinContent(ipt,ieta,R);
      }
    std::cout << "Hello" << std::endl;
    return h;
  }

  vector<TH2D*> CalculateStatNuisanceParamsVsPtEta(Graph2D *g2d, float pTmin) {
    vector<TH2D*> nuisVec;

    for (int pi=0;pi<g2d->GetN();++pi) {
      TH2D *h = MakePtEtaHisto(Form("Stat%d",pi));
      for (int ieta=1;ieta<=h->GetNbinsY();++ieta)
	for (int ipt=1;ipt<=h->GetNbinsX();++ipt) {
	  double eta=h->GetYaxis()->GetBinCenter(ieta);
	  double pt=h->GetXaxis()->GetBinCenter(ipt);
	
	
	  if(useGraduatedLowPtSmoothing){
	    if(!m_isSyst){
	      
	      /*
	      if(pt<35)                  { m_width_x=0.6; m_width_y=0.18; }
	      else if( pt>=35 && pt<40)  { m_width_x=0.55; m_width_y=0.16; }
	      else if( pt>=40 && pt<45 ) { m_width_x=0.5; m_width_y=0.145; }
	      else if( pt>=45 && pt<46 ) { m_width_x=0.49; m_width_y=0.135; }
	      else if( pt>=46 && pt<47 ) { m_width_x=0.48; m_width_y=0.132; }
	      else if( pt>=47 && pt<48 ) { m_width_x=0.47; m_width_y=0.13; }
	      else if( pt>=48 && pt<49 ) { m_width_x=0.46; m_width_y=0.125; }
	      else if( pt>=49 && pt<56 ) { m_width_x=0.45; m_width_y=0.12; }
	      else if( pt>=56 && pt<57 ) { m_width_x=0.44; m_width_y=0.12; }
	      else if( pt>=57 && pt<58 ) { m_width_x=0.43; m_width_y=0.12; }
	      else if( pt>=58 && pt<59 ) { m_width_x=0.42; m_width_y=0.12; }
	      else if( pt>=59 && pt<60 ) { m_width_x=0.41; m_width_y=0.12; }
	      else if( pt>=60 && pt<61 ) { m_width_x=0.40; m_width_y=0.12; }
	      else if( pt>=61 && pt<62 ) { m_width_x=0.39; m_width_y=0.12; }
	      else if( pt>=62 && pt<63 ) { m_width_x=0.38; m_width_y=0.12; }
	      else if( pt>=63 && pt<64 ) { m_width_x=0.37; m_width_y=0.12; }
	      else if( pt>=64 && pt<65 ) { m_width_x=0.36; m_width_y=0.12; }
	      else if( pt>=65 && pt<66 ) { m_width_x=0.35; m_width_y=0.12; }
	      else if( pt>=66 && pt<67 ) { m_width_x=0.34; m_width_y=0.12; }
	      else if( pt>=67 && pt<68 ) { m_width_x=0.33; m_width_y=0.12; }
	      else if( pt>=68 && pt<69 ) { m_width_x=0.32; m_width_y=0.12; }
	      else if( pt>=69 && pt<70 ) { m_width_x=0.29; m_width_y=0.12; }
	      */
	      
	      if(pTmin<35){
		if(pt<35)                  { m_width_x=0.58;   m_width_y=0.16; }
		else if( pt>=35 && pt<36)  { m_width_x=0.58;  m_width_y=0.16; }
		else if( pt>=36 && pt<37)  { m_width_x=0.57;  m_width_y=0.155; }
		else if( pt>=37 && pt<38)  { m_width_x=0.56;  m_width_y=0.152; }
		else if( pt>=38 && pt<39)  { m_width_x=0.55;  m_width_y=0.15; }
		else if( pt>=39 && pt<40)  { m_width_x=0.54;  m_width_y=0.149; }
		else if( pt>=40 && pt<41)  { m_width_x=0.53;  m_width_y=0.148; }
		else if( pt>=41 && pt<42)  { m_width_x=0.52;  m_width_y=0.147; }
		else if( pt>=42 && pt<43)  { m_width_x=0.51;  m_width_y=0.143; }
		else if( pt>=43 && pt<44)  { m_width_x=0.505; m_width_y=0.138; }
		else if( pt>=44 && pt<45)  { m_width_x=0.5;   m_width_y=0.136; }
		else if( pt>=45 && pt<46 ) { m_width_x=0.49;  m_width_y=0.134; }
		else if( pt>=46 && pt<47 ) { m_width_x=0.48;  m_width_y=0.132; }
		else if( pt>=47 && pt<48 ) { m_width_x=0.47;  m_width_y=0.13; }
		else if( pt>=48 && pt<49 ) { m_width_x=0.46;  m_width_y=0.125; }
	      }else if(pTmin>=35&&pTmin<40){
		if( pt<40)  { m_width_x=0.54;  m_width_y=0.149; }
		else if( pt>=40 && pt<41)  { m_width_x=0.53;  m_width_y=0.148; }
		else if( pt>=41 && pt<42)  { m_width_x=0.52;  m_width_y=0.147; }
		else if( pt>=42 && pt<43)  { m_width_x=0.51;  m_width_y=0.143; }
		else if( pt>=43 && pt<44)  { m_width_x=0.505; m_width_y=0.138; }
		else if( pt>=44 && pt<45)  { m_width_x=0.5;   m_width_y=0.136; }
		else if( pt>=45 && pt<46 ) { m_width_x=0.49;  m_width_y=0.134; }
		else if( pt>=46 && pt<47 ) { m_width_x=0.48;  m_width_y=0.132; }
		else if( pt>=47 && pt<48 ) { m_width_x=0.47;  m_width_y=0.13; }
		else if( pt>=48 && pt<49 ) { m_width_x=0.46;  m_width_y=0.125; }
	      }else if(pTmin>=40&&pTmin<45){
		if( pt<45)  { m_width_x=0.5;   m_width_y=0.136; }
		else if( pt>=45 && pt<46 ) { m_width_x=0.49;  m_width_y=0.134; }
		else if( pt>=46 && pt<47 ) { m_width_x=0.48;  m_width_y=0.132; }
		else if( pt>=47 && pt<48 ) { m_width_x=0.47;  m_width_y=0.13; }
		else if( pt>=48 && pt<49 ) { m_width_x=0.46;  m_width_y=0.125; }
	      }else if(pTmin>=45&&pTmin<46){
		if( pt<46 ) { m_width_x=0.49;  m_width_y=0.134; }
		else if( pt>=46 && pt<47 ) { m_width_x=0.48;  m_width_y=0.132; }
		else if( pt>=47 && pt<48 ) { m_width_x=0.47;  m_width_y=0.13; }
		else if( pt>=48 && pt<49 ) { m_width_x=0.46;  m_width_y=0.125; }
	      }else if(pTmin>=46&&pTmin<47){
		if( pt<47 ) { m_width_x=0.48;  m_width_y=0.132; }
		else if( pt>=47 && pt<48 ) { m_width_x=0.47;  m_width_y=0.13; }
		else if( pt>=48 && pt<49 ) { m_width_x=0.46;  m_width_y=0.125; }
	      }else if(pTmin>=47&&pTmin<48){
		if( pt<48 ) { m_width_x=0.47;  m_width_y=0.13; }
		else if( pt>=48 && pt<49 ) { m_width_x=0.46;  m_width_y=0.125; }
	      }else if(pTmin>=48&&pTmin<49){
		if( pt<49 ) { m_width_x=0.46;  m_width_y=0.125; }
	      }
	      
	      
	      if( pt>=49 && pt<56 ) { m_width_x=0.45; m_width_y=0.12; }
	      else if( pt>=56 && pt<57 ) { m_width_x=0.44; m_width_y=0.12; }
	      else if( pt>=57 && pt<58 ) { m_width_x=0.43; m_width_y=0.12; }
	      else if( pt>=58 && pt<59 ) { m_width_x=0.42; m_width_y=0.12; }
	      else if( pt>=59 && pt<60 ) { m_width_x=0.41; m_width_y=0.12; }
	      else if( pt>=60 && pt<61 ) { m_width_x=0.40; m_width_y=0.12; }
	      else if( pt>=61 && pt<62 ) { m_width_x=0.39; m_width_y=0.12; }
	      else if( pt>=62 && pt<63 ) { m_width_x=0.38; m_width_y=0.12; }
	      else if( pt>=63 && pt<64 ) { m_width_x=0.37; m_width_y=0.12; }
	      else if( pt>=64 && pt<65 ) { m_width_x=0.36; m_width_y=0.12; }
	      else if( pt>=65 && pt<66 ) { m_width_x=0.35; m_width_y=0.12; }
	      else if( pt>=66 && pt<67 ) { m_width_x=0.34; m_width_y=0.12; }
	      else if( pt>=67 && pt<68 ) { m_width_x=0.33; m_width_y=0.12; }
	      else if( pt>=68 && pt<69 ) { m_width_x=0.32; m_width_y=0.12; }
	      else if( pt>=69 && pt<70 ) { m_width_x=0.29; m_width_y=0.12; }
	      
	    }
	  }
	  
	  
	  double stat=GetSmoothedStat(g2d,pi,pt,eta);
	  h->SetBinContent(ipt,ieta,stat);
	}
      h->SetMinimum(0.00001); h->SetMaximum(0.02);
      nuisVec.push_back(h);
    }
    return nuisVec;
  }
  
private:

  // width of kernel
  double m_width_x, m_width_y;
  // eta bins
  VecD m_etaBins;
  // whether the width is fractional or not
  bool m_logx, m_logy, m_isSyst, m_isSM;

  // whether to use the errors
  bool m_useError;
  
};
