#ifndef PhiInterCalibration_h
#define PhiInterCalibration_h

#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TEnv.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TF1.h"
#include "TGraph2DErrors.h"

#include <iostream>

typedef unsigned int uint;

std::vector<TString> vectorise(TString str, TString sep=" ") {
  std::vector<TString> result; TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) { delete strings; return result; }
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr()) result.push_back(os->GetString());
  delete strings; return result;
}

std::vector<double> vectoriseD(TString str, TString sep=" ") {
  std::vector<double> result; std::vector<TString> vecS = vectorise(str,sep);
  for (uint i=0;i<vecS.size();++i)
  result.push_back(atof(vecS[i]));
  return result;
}

std::vector<double> makeUniformVec(int N, double min, double max) {
  std::vector<double> vec; double dx=(max-min)/N;
  for (int i=0;i<=N;++i) vec.push_back(min+i*dx);
  return vec;
}

int OptimalRebin(TH1 *h)
{
  int method=2;

  // Get optimal bin withs using Scott's Choise
  double N=h->GetEffectiveEntries();
  //Neff(h);
  //double optWidth = _optBinParam*h->GetRMS()/pow(N,1.0/3);
  double optWidth = 3.5*h->GetRMS()/pow(N,1.0/3);
  int Nbins=h->GetNbinsX();
  double range=h->GetBinLowEdge(Nbins+1)-h->GetBinLowEdge(1);
  int rebin=1;
  double prevWidth=range/Nbins;
  for (int i=1;i<Nbins;++i) {
    if (Nbins%i!=0) continue;
    double binWidth=range/Nbins*i;
    //if (binWidth>maxWidth) continue;

    if (method==1) {
      // optimistic
      if (binWidth<optWidth) rebin=i;
    } else if (method==2) {
      if (TMath::Abs(binWidth-optWidth) <
      TMath::Abs(prevWidth-optWidth)) rebin=i;
    }
    else rebin=i; // method 3

    if (binWidth>optWidth) break;
    prevWidth=binWidth;
  }
  return rebin;
}


class PhiInterCalibration {
public:

  PhiInterCalibration(TFile* infile, TEnv* settings, bool isMC, double offset = -1);
  ~PhiInterCalibration();

  // get JX slice weight
  double weightJX(int JX);
  // Fill asymmetry histograms from mini-ntuples
  TH1F* FillAsymHist(int pTavg_bin, int phi_bin, float ref_reigon);
  // Fill 3D histogram
  void Fill3DHists();
  int pTavgBin(double pTavg);
  int etaBin(double eta);
  void applyPhiOffset(float &phi);
  TH1F* AsymHist(int pTavg_bin, int eta_bin, int phi_bin);
  // Fit asymmetry distribution
  // Check if entries sufficient, perform fit, judge fit
  TF1* FitAsymHist(TH1* hA);
  double bias(double phi);
  TF1* drawBias();
  void WriteAvgPtEtaPhiGraph(TFile* f);
  TString triggerClassification(int ptbin,float pTavg);
  float lumiWeight(int ptbin, TString trig_class);

  std::vector<double> GetEtaBins(){ return eta_bins; }

private:

  void InitTree();

  TEnv* m_settings;
  bool m_isMC;
  TString m_mcName;

  TTree* m_tree;
  float m_weight;
  float m_pTavg;
  int m_JX;
  float m_j1_pT, m_j1_eta, m_j1_phi, m_j1_E;
  float m_j2_pT, m_j2_eta, m_j2_phi, m_j2_E;
  int m_trigBitsFwd;
  bool m_pass_HLT_j15;
  bool m_pass_HLT_j15_320eta490;
  bool m_pass_HLT_j25;
  bool m_pass_HLT_j25_320eta490;
  bool m_pass_HLT_j60;
  bool m_pass_HLT_j60_320eta490;

  // Central+forward jet trigger combinations + prescale & lumi weights
  // NOTE: not ideal to put here, move to settings.config
  //  std::vector<TString> m_pTbinTrigs = {"j15","j25","j25","j25","j60","j60","j60"};
  //  std::vector<TString> m_pTbinTrigs = {"j15","j25","j60"};
  std::vector<TString> m_pTbinTrigs = {"j15orj25","j60"};
  //  std::vector<float> m_ctrl_lumis = {1.0, 1.0, 1.0};
  //  std::vector<float> m_fwd_lumis = {1.0, 1.0, 1.0};
  std::vector<float> m_ctrl_lumis = {1.287106e+03, 4.69346571000000040e+03, 1.21311682140200006e+05};
  std::vector<float> m_fwd_lumis = {1.13963780520000000e+04, 6.80175354299999890e+04, 6.70431956479750015e+06};

  std::vector<float> m_cutFlow_Tot;
  std::vector<float> m_cutFlow_pTavg;
  std::vector<float> m_cutFlow_LeadRef;
  std::vector<float> m_cutFlow_LeadRefSubProbe;
  std::vector<float> m_cutFlow_SubRef;
  std::vector<float> m_cutFlow_SubRefLeadProbe;

  bool m_doOffset;
  double m_offset;

  bool m_injectBias;
  bool m_useJXweight;
  TF1* m_biasFunc;

  std::vector< TH3F* > m_3DAsymHists;
  std::vector< TH1F* > m_PtProbeHists;
  std::vector< TH1F* > m_PtRefHists;
  std::vector< TH1F* > m_PtAvgHists;
  std::map< std::string, TH1F* > m_phiHists;
  TGraph2DErrors* m_pTavgEtaPhi;
  TH3F* m_pTavg_pTavgEtaPhi;
  TH3F* m_pTprobe_pTavgEtaPhi;
  TH3F* m_pTref_pTavgEtaPhi;
  TH3F* m_eta_pTavgEtaPhi;
  TH3F* m_phi_pTavgEtaPhi;
  TH1F* m_pTavgHist;
  std::vector< TH1F* > m_pTavg_JX;
  std::vector< TH1F* > m_pTprobe_JX;

  std::vector<double> phi_bins;
  std::vector<double> pTavg_bins;
  std::vector<double> asym_bins;
  std::vector<double> eta_bins;
  std::vector<double> eta_bins_plots;
  double m_refRegion;
  double m_probeRegion_min;
  double m_probeRegion_max;

};

#endif
