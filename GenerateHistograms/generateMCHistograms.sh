#/bin/sh

inFile="/afs/cern.ch/user/j/jrawling/workDir/EtaIntercalibration/mc/NTuples/PowhegPythia8/mc15_13TeV_PowhegPythia_e3788_a766_a810_r6282_p2613_NTuple.root"
configFile="utils/settings.config"
outPath="/afs/cern.ch/user/j/jrawling/workDir/EtaIntercalibration/Results/PowhegPytiha8/3DHistos_NoJVT"
gen="PowhegPytiha8"
productionTag="p2666"
exec=histogramGenerator.exe
#nohup
./$exec ${configFile} ${inFile} ${outPath} ${gen} ${productionTag}
# &> ${outPath}/generation.log &
