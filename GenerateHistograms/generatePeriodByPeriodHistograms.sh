#!/bin/sh

configFile="utils/settings.config"
onTheFlySettings="doBootStrap=0,doNominal=1,doSystematics=0,doDphiShiftDown=0,doDphiShiftUp=0,doJet3PtFracShiftUp=0,doJet3PtFracShiftDown=0,doJVTCutTight=0"
gen="data"
productionTag="p2666"
exec=histogramGenerator.exe

#nohup
inFile=/pc2012-data2/jrawling/data16_13TeV/data16_13TeV_periodG.root
nohup ./$exec ${configFile} ${inFile} ${gen} ${productionTag} /pc2012-data2/jrawling/PeriodByPeriodBreakDown/LCTopo/periodG/ ${onTheFlySettings} &> periodGEMtopo.log &
inFile=/pc2012-data2/jrawling/data16_13TeV/data16_13TeV_periodI.root
nohup ./$exec ${configFile} ${inFile} ${gen} ${productionTag} /pc2012-data2/jrawling/PeriodByPeriodBreakDown/LCTopo/periodI/ ${onTheFlySettings} &> periodGEMtopo.log &
#&> ${outPath}/generation.log &
