exec=histogramGenerator.exe

inputFile="/pc2012-data2/jrawling/EtaInetrcalibrationAnalysis/data15_13TeV_FixedTrigger/MiniTrees/EMTopo/data15_13TeV_periodsDFGHJ_microXAOD.root"
#inputFile="/pc2012-data2/jrawling/EtaInetrcalibrationAnalysis/data15_13TeV_FixedTrigger/MiniTrees/EMTopo/periodD/00276952/EtaInterCal_3DHists_0.root"
outputFilePath="/pc2012-data2/jrawling/ZeroRadiationExtrapolation/data15_13TeV/"

logFile=${outputFilePath}"pt3/generation.log"
configFile="utils/settingsPt3.config"
#nohup ./$exec ${configFile} ${inputFile} ${outputFilePath}"pt3" &> ${logFile} &

logFile=${outputFilePath}"phiStar/generation.log"
configFile="utils/settingsphiStar.config"
nohup ./$exec ${configFile} ${inputFile} ${outputFilePath}"phiStar" &> ${logFile} &

logFile=${outputFilePath}"pt3OverPTavg/generation.log"
configFile="utils/settingsPt3OverPtAvg.config"
#nohup ./$exec ${configFile} ${inputFile} ${outputFilePath}"pt3OverPTavg" &> ${logFile} &

logFile=${outputFilePath}"Dphi/generation.log"
configFile="utils/settingsDphiJJ.config"
#nohup ./$exec ${configFile} ${inputFile} ${outputFilePath}"Dphi" &> ${logFile} &
