#!/bin/sh

#jetAlgo=$1

exec=matrix_minimization.exe
configFile="utils/Dijet_xAOD.config"
ieta="Eta0.3"
outDir="./"

for gen in PowhegPythia #Sherpa Pythia
do
    if [ "$gen" == "PowhegPythia" ] || [ "$gen" == "Pythia" ]
    then
        productionTag="p1562"
    elif [ "$gen" == "Sherpa" ]
    then
	productionTag="p2440"
    fi

    for typeOfData in EMTopo #Truth EMTopo MCAsym
    do
	if [ "$typeOfData" == "Truth" ]
	then
	    jetAlgo="AntiKt4Truth"
	elif [ "$typeOfData" == "EMTopo" ] || [ "$typeOfData" == "MCAsym" ]
	then 
	    jetAlgo="AntiKt4EMTopo"
	fi

	for systematics in syst_jvtloose # syst_dphiDown syst_dphiUp syst_j3down syst_j3up syst_jvtTight syst_jvtloose nominal
	do
	    inFile="${gen}_${typeOfData}_Data_${systematics}.root"
	    #inFile="${gen}_${typeOfData}_noDRm_${systematics}.root"
	    inDir="/pc2012-data2/rpickles/EtaInetrcalibrationAnalysis/data15_13TeV/MC/AntiKt4EMTopo/3Dhists/Grid/${gen}/${gen}_${typeOfData}_Data_${systematics}.root"
	    ./$exec ${configFile} ${jetAlgo} ${productionTag} ${ieta} ${inDir} ${outDir}${gen}_${typeOfData}_${systematics}_${ieta}_${jetAlgo}_etaBinTest.root ${systematics} ${gen}
	done
    done
done

#inDir="/pc2012-data2/rpickles/EtaInetrcalibrationAnalysis/data15_13TeV/MC/AntiKt4EMTopo/3Dhists/Grid/PowhegPythia/PowhegPythia_MCAsym_nominal.root"
#inDir="/pc2012-data2/rpickles/EtaInetrcalibrationAnalysis/data15_13TeV/MC/AntiKt4EMTopo/3Dhists/Grid/${gen}/${gen}_${typeOfData}_${systematic}.root"
#inDir="/pc2012-data2/rpickles/EtaInetrcalibrationAnalysis/data15_13TeV/MC/AntiKt4EMTopo/3Dhists/Grid/Pythia8/Pythia_MCAsym_nominal.root"
#inDir="/pc2012-data2/rpickles/EtaInetrcalibrationAnalysis/data15_13TeV/MC/AntiKt4EMTopo/3Dhists/Grid/Sherpa/Sherpa_MCAsym_nominal.root"
