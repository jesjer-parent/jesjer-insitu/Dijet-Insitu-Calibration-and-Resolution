#!/bin/sh

exec=matrix_minimization.exe

configFile="utils/Dijet_xAOD.config"
eta="Eta0.3"
jetAlgo="AntiKt4EMTopo"
productionTag="25nsFinal"
syst=""
outDir="./"

for systematics in nominal syst_dphiDown syst_dphiUp syst_j3down syst_j3up syst_jvtTight syst_jvtloose
do
    inFile="Data_${systematics}.root"
    inDir="/pc2012-data2/rpickles/EtaInetrcalibrationAnalysis/data2016/Data/Data_${systematics}.root"
    ./$exec $configFile $jetAlgo $productionTag ${eta} ${inDir} ${outDir}data16_${systematics}_${eta}_${jetAlgo}.root ${systematics}

#inDir="/pc2012-data2/rpickles/EtaInetrcalibrationAnalysis/data15_13TeV/MC/AntiKt4EMTopo/3Dhists/Grid/data/Data_nominal.root"
#./$exec $configFile $jetAlgo $productionTag ${eta} ${inDir} ${outDir}data15_Fit0.4_${eta}_${jetAlgo}.root 
done

##Forward as default 
