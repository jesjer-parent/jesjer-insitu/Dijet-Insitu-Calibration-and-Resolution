//Determination of Intercalibration Coefficients via the Matrix Method
//========================================================================
// Idea and Code: Frederik Ruehr
// with modifications by Boris Lemmer
// Code restructured by Dag Gillberg & Jim Lacey
// with modifications to calculate width by Rebecca Pickles

#ifndef MatrixMethod_cxx
#define MatrixMethod_cxx

#include "MatrixMethod_W.h"
#include "Math/GSLMinimizer.h"
#include "Math/Functor.h"

Str Result_psFile, AsymSM_psFile;

int main(int argc, char **argv) { 
  // Turn off all those "Info in <TCanvas::Print>: Current canvas added ..."
  gErrorIgnoreLevel=2000;

  if (argc<6) error("Must give 6(7) arguments: config, jetAlgo, production tag, eta binning, infile, outfile, (& MC generator, if MC)");

  printf("\n Analysis settings:\n\n%12s  %-s\n%12s  %-s\n%12s  %-s\n%12s  %-s\n%12s  %-s\n%12s  %-s\n%12s %-s\n ",
	 "Config:",argv[1],"jet algo:",argv[2],"production tag:",argv[3],"Eta binning:",argv[4],"Infile:",argv[5],"Outfile:",argv[6],"Systematics:",argv[7]);

  cout<<"N Input Arguments: "<<argc<<endl;

  configFN       =argv[1]; 
  jetAlgo        =argv[2] ;
  if (jetAlgo=="") error("No value for JetAlgo!"); 

  _productionTag = argv[3];
  _etaBinning    = argv[4];
  Str infn       = argv[5]; 
  outfileName    = argv[6];  
  systematics    = argv[7];

  _isMC = argc>8;

  printf("\n  Judging by the input file name that we are running over: %s\n",_isMC?"MC":"data");
  if (_isMC) {
    MCgen=argv[8];
    if ( MCgen!="Pythia" && MCgen!="Herwig" && MCgen!="PythiaMinbias" && MCgen!="PowhegPythia" && MCgen!="PowhegJimmy" && MCgen!="Sherpa" && MCgen!="PowhegHerwig") 
      error("Unknown MC generator: "+MCgen);
  }

  cout<<"infn = "<<infn<<endl;
  inputFile = TFile::Open(infn);
  if (inputFile==NULL) error("Problem opening "+infn); 

  Str hname;
  if(_isMC) hname = Form("%s/"+systematics+"_%s_J1_Asym3D_PtBin1",jetAlgo.Data(),jetAlgo.Data()); 
  else      hname = Form("%s/"+systematics+"_%s_HLT_j15_OR_HLT_j15_320eta490_RefFJ_J_Asym3D_PtBin1",jetAlgo.Data(),jetAlgo.Data());

  cout<<"hname = "<<hname<<endl;

  TH3D* h3detaBins = (TH3D*)inputFile->Get(hname);
  if(h3detaBins==NULL) error("Can't find histo for determining abs eta!");
  if(h3detaBins->GetXaxis()->GetBinLowEdge(1)>=0.0) _absEta = true;
  delete h3detaBins;

  TEnv *settings =Init(configFN);

  outfile = InitOutputFile(outfileName);

  int jetR=4; if (!jetAlgo.Contains("Kt4")) error("Not Kt4!");
  StrV ptRangesStr = Vectorize(settings->GetValue(Form("PtRangesAntiKt%d",jetR),""));
  if (ptRangesStr.size()==0) error("Must specify PtRangesAntiKt for minimization");
  
  ptRanges.clear(); _ptBins.clear();
  
  for (uint ri=0;ri<ptRangesStr.size();++ri) {
    ptRanges.push_back(VectorizeD(Str(ptRangesStr[ri]).ReplaceAll("-"," ")));

    if (ptRanges[ri].size()!=2) error(Str("Problem with pT-range: ")+ptRangeStr[ri]);
    if ( ri>0 && ptRanges[ri][0]!=ptRanges[ri-1][1] ) error(Str("pT-ranges ")+ptRangeStr[ri-1]+" and the consequative "+ptRangeStr[ri]+" doesn't connect.");
    
    /*  Str etaBinKey=Form("EtaBinsMM_%s_%.0fto%.0f",_etaBinning.Data(),ptRanges[ri][0],ptRanges[ri][1]);
    _etaBinMap[ri] = VectorizeD(settings->GetValue(etaBinKey,""));
    if (_etaBinMap[ri].size()==0) error("Must specify "+etaBinKey);*/
        
    if (ri==0) add(_ptBins,ptRanges[ri][0]);
    add(_ptBins,ptRanges[ri][1]);
  }

  // for merging pTavg bins in data
  _pTBINS = VectorizeD(settings->GetValue(Form("PtBinsAntiKt%d",jetR),""));
  for (uint ri=0;ri<ptRangesStr.size();++ri) {
    float lowRange  = ptRanges[ri][0];
    float highRange = ptRanges[ri][1];
    for( int ipt=0; ipt<_pTBINS.size(); ipt++){
      if(_pTBINS[ipt]>=lowRange && _pTBINS[ipt]<=highRange){
	PtBinMap[ri].push_back( _pTBINS[ipt] );
      }
    }
  }
  /*
  map<int,VecD> TempetaBinMap;
  if(_absEta){
    for (uint ri=0;ri<ptRangesStr.size();++ri) {
      VecD regBINS = _etaBinMap[ri];
      VecD absBINS;
      for(int ieta=0; ieta<=(regBINS.size()-1)/2; ieta++){
	absBINS.push_back(regBINS[ieta+(regBINS.size()-1)/2]);
      }
      TempetaBinMap[ri] = absBINS;
    }
    _etaBinMap = TempetaBinMap;
    }*/
  
  outfile->mkdir(jetAlgo); outfile->mkdir(jetAlgo+"_Histos");
  BookHisto2D(jetAlgo+"_AvgPtAvg_vs_EtaPtAvg_SM","#LTp_{T}^{avg}#RT in each bin",_etaBinsSM,_ptBins);
  BookHisto2D(jetAlgo+"_AvgEta_vs_EtaPtAvg_SM","#LT#eta_{det}#RT in each bin",_etaBinsSM,_ptBins);

  if ( _isMC ) {
    std::cout << "Booking pTavg histograms" << std::endl;
    BookHisto(jetAlgo+"_pTavg","",_ptBins);
    for ( const auto &sample : MCsamples )
      BookHisto(jetAlgo+"_pTavg_"+sample,"",_ptBins);
    
    int ih=0;
    for ( const auto &sample : MCsamples ) {
      histos[jetAlgo+"_pTavg_"+sample] = ((TH2D*)inputFile->Get(jetAlgo+"/"+systematics+"_"+jetAlgo+"_"+sample+"_PtAvg_vs_EtaDet"))->ProjectionY(jetAlgo+"_pTavg_"+sample);	
      if (ih==0) histos[jetAlgo+"_pTavg"] = ((TH2D*)inputFile->Get(jetAlgo+"/"+systematics+"_"+jetAlgo+"_"+sample+"_PtAvg_vs_EtaDet"))->ProjectionY(jetAlgo+"_pTavg");
      else histos[jetAlgo+"_pTavg"]->Add( ((TH2D*)inputFile->Get(jetAlgo+"/"+systematics+"_"+jetAlgo+"_"+sample+"_PtAvg_vs_EtaDet"))->ProjectionY() );
      ih++;
    }
  }

  Str previousTrigKey = "";
  
  for (uint ri=0;ri<ptRanges.size();++ri) {
    
    current_ptRange=ri;    
    if (ptRanges[ri].size()!=2) error("Incorrectly specified PtRanges");
    
    ptmin=ptRanges[ri][0]; ptmax=ptRanges[ri][1];
    
    ptRangeDesc=Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax);
    ptRangeStr=Form("pt%.0fto%.0f",ptmin,ptmax);
    
    //    VecD etaBins=_etaBinMap[ri];
    //    int NetaBins = etaBins.size()-1;

    if (_isMC) {
      MinimizeMC();
    } 
    else { // data!
      trigs.clear();
      Str trigKey = Form("AntiKt%d.%.0fto%.0f.TrigOR",jetR,ptmin,ptmax);
      Str trigSel = settings->GetValue(trigKey,""); trigSel.ReplaceAll(" ","");
      
      if(trigSel!="") previousTrigKey = trigKey;
      else{
	trigSel = settings->GetValue(previousTrigKey,""); trigSel.ReplaceAll(" ",""); 
	cout<<"using trigger: "<<trigSel<<" for pT range : "<<ptmin<<" - "<<ptmax<<endl;
      }
      if(trigSel=="") error(trigKey+" not specified in "+configFN);
      
      currentTrigLong=trigSel;
      
      trigSel.ReplaceAll("_OR_"," ");
      trigs=Vectorize(trigSel);

      cout<<"trigs size   == "<<trigs.size()<<endl;
      if (trigs.size()<2) error("Current trigger selection: "+trigSel+" is not a trigOR!?");

      currentTrig="";
      if ( trigs.size() > 2 ){	
	for ( int trig = 0 ; trig < trigs.size()-1 ; trig++ ){  
	  if ( trig%2 != 0 ) currentTrig+=trigs[trig]+"AND";
	  else currentTrig+=trigs[trig]+"OR";
       	}
	currentTrig+=trigs[trigs.size()-1];
      }
      else {
	currentTrig=trigs[0]+"OR"+trigs[1];
      }

      printf(  "  %.0f < pTavg < %.0f GeV\n",ptmin,ptmax);
      printf(  "  current trig: %s\n",currentTrig.Data());

      if ( trigs.size() > 2 ){
	for( int trig = 0; trig < trigs.size(); trig++ ) printf(  "  Trigger: %s\n",trigs[trig].Data());
      }
      else printf(  "  Triggers: %s %s\n",trigs[0].Data(),trigs[1].Data());

      MinimizeData();
    } // Data or MC if statement
  }//for each pT 
  
  outfile->cd();
  histos2d[jetAlgo+"_AvgPtAvg_vs_EtaPtAvg_SM"]->Write();
  histos2d[jetAlgo+"_AvgEta_vs_EtaPtAvg_SM"]->Write();
  
  if ( _isMC ) {
    histos[jetAlgo+"_pTavg"]->Write();
    for ( const auto &sample : MCsamples )
      histos[jetAlgo+"_pTavg_"+sample]->Write();  
  }

  gROOT->cd();
  MakePDF(Result_psFile);
  MakePDF(AsymSM_psFile);
  inputFile->Close();
  outfile->Close();

  cout<<"Wrote results to " << outfileName <<endl;
  cout << endl << "All Done" << endl << endl;
} // main

void MinimizeMC() {
  cout << "in MinimizeMC" << endl;

  int N = MCsamples.size();
  int lowPtBin=-1, highPtBin=-1;
  vector<TH2F*> h2d_pt_vs_eta;
  vector<TH1D*> etaSpectra;
  vector<TH1D*> pTSpectra;
  vector<TH3F*> A3D_histos;
  vector<TH3F*> A3D_histos_truth;

  h_weights.clear();

  for (int mi=0;mi<N;++mi) {

    Str prefix=""+systematics+"_"+jetAlgo+"_"+MCsamples[mi];
    Str truthprefix=""+systematics+"_"+truthJetAlgo+"_"+MCsamples[mi];
    Str suffix="";

    h2d_pt_vs_eta.push_back(Get2DHisto(prefix+"_PtAvg_vs_EtaDet"));
    if (mi==0) GetBinRange(h2d_pt_vs_eta[0]->GetYaxis(), ptmin, ptmax,lowPtBin, highPtBin);    
    etaSpectra.push_back(h2d_pt_vs_eta[mi]->ProjectionX(prefix+ptRangeStr+"_EtaSpectrum",lowPtBin,highPtBin));
    pTSpectra.push_back(h2d_pt_vs_eta[mi]->ProjectionY(prefix+ptRangeStr+"_pTSpectrum",lowPtBin,highPtBin));
    A3D_histos.push_back(GetAsymmetry3DHisto(prefix,lowPtBin,highPtBin,suffix));

    if (_useWeights) h_weights.push_back(MCFiltEff[mi]*MCxsec[mi]*MClumi/MCNevts[mi]);
    else h_weights.push_back(1);
  }

  h_weights_truth = h_weights;

  vector<TH2F*> meanHistos;
  for (int jxi=0;jxi<N;++jxi) {
    Str prefix=""+systematics+"_"+jetAlgo+"_"+MCsamples[jxi];
    meanHistos.push_back(Get2DHisto(prefix+"_PtAvgFine_vs_EtaProbeSM"));
  }

  ExtractPtEtaMean(meanHistos,h_weights,"SM",ptmin,ptmax);
  MergeHistosAndMinimize(jetAlgo+"_", A3D_histos, etaSpectra, pTSpectra, MCsamples, A3D_histos_truth);
} // MinimizeMC

void MinimizeData() {

  if (_isTrigOR) {

    vector<TH2F*> h2d_pt_vs_eta;
    vector<TH1D*> etaSpectra;
    vector<TH1D*> pTSpectra;
    vector<TH3F*> A3D_histos;
    vector<TH3F*> A3D_histos_truth;
    StrV trigDesc;
    
    if ( trigs.size() == 2 ){
      
      currentTrigDesc=Str(currentTrig).ReplaceAll("or"," OR ");
      Str prefix=""+systematics+"_"+jetAlgo+"_"+currentTrig+"_";
      inputFile->cd(jetAlgo);

      if ( gDirectory->Get(prefix+"RefFJ_J_PtAvg_vs_EtaDet")==NULL) prefix=""+systematics+"_"+jetAlgo+"_"+currentTrigLong+"_";
      
      gROOT->cd();
      
      int lowPtBin=-1, highPtBin=-1;
      
      TString prefix1 = prefix+"RefFJ_J";
      TString prefix2 = prefix+"RefFJ_FJ";
      
      trigDesc.push_back(trigs[0]); trigDesc.push_back(trigs[1]);
      
      printf("    Trig1: %10s, lumi: %.5f pb-1\n",trigs[0].Data(),CentrallumiMap[trigs[0]]*1e-6);
      printf("    Trig2: %10s, lumi: %.5f pb-1\n",trigs[1].Data(),ForwardlumiMap[trigs[1]]*1e-6);
      
      if (CentrallumiMap[trigs[0]]<1e-9||ForwardlumiMap[trigs[1]]<1e-9) error("Luminosity nonsense?");
      
      h2d_pt_vs_eta.push_back(Get2DHisto(prefix1+"_PtAvg_vs_EtaDet"));
      h2d_pt_vs_eta.push_back(Get2DHisto(prefix2+"_PtAvg_vs_EtaDet"));
      // CRASH - for AntiKt6
           
      GetBinRange(h2d_pt_vs_eta[0]->GetYaxis(), ptmin, ptmax, lowPtBin, highPtBin);
      etaSpectra.push_back(h2d_pt_vs_eta[0]->ProjectionX(prefix+trigs[0]+ptRangeStr+"_EtaSpectrum",lowPtBin,highPtBin));
      etaSpectra.push_back(h2d_pt_vs_eta[1]->ProjectionX(prefix+trigs[1]+ptRangeStr+"_EtaSpectrum",lowPtBin,highPtBin));
    
      etaSpectra[0]->SetTitle(jetAlgo+" #eta_{det} distribution, "+ptRangeDesc+", "+trigs[0]+" from "+currentTrig);
      etaSpectra[1]->SetTitle(jetAlgo+" #eta_{det} distribution, "+ptRangeDesc+", "+trigs[1]+" from "+currentTrig);

      pTSpectra.push_back(h2d_pt_vs_eta[0]->ProjectionY(prefix+trigs[0]+ptRangeStr+"_pTSpectrum",lowPtBin,highPtBin));
      pTSpectra.push_back(h2d_pt_vs_eta[1]->ProjectionY(prefix+trigs[1]+ptRangeStr+"_pTSpectrum",lowPtBin,highPtBin));
      
      pTSpectra[0]->SetTitle(jetAlgo+" pT distribution, "+ptRangeDesc+", "+trigs[0]+" from "+currentTrig);
      pTSpectra[1]->SetTitle(jetAlgo+" pT distribution, "+ptRangeDesc+", "+trigs[1]+" from "+currentTrig);

      h_weights.clear(); 
      
      // let's make an acutal x-sec measuremnt     
      if ( _useWeights ){
	h_weights.push_back(1.0/CentrallumiMap[trigs[0]]*1e6);
	h_weights.push_back(1.0/ForwardlumiMap[trigs[1]]*1e6);
      }
      else{
	h_weights.push_back(1.0);
	h_weights.push_back(1.0);
      }

      Str suffix="";
      A3D_histos.push_back(GetAsymmetry3DHisto(prefix1,lowPtBin,highPtBin,suffix));
      A3D_histos.push_back(GetAsymmetry3DHisto(prefix2,lowPtBin,highPtBin,suffix));      
      
      if (A3D_histos.size()!=2) error("Something went terribly wrong...");
      int Nevts = A3D_histos[0]->GetEntries()+A3D_histos[1]->GetEntries();
      
      if (Nevts<50) error(Form("No data for this pT-range! (%d events) ",Nevts));
      
      vector<TH2F*> meanHistos;
      meanHistos.push_back(Get2DHisto(prefix1+"_PtAvgFine_vs_EtaProbeSM"));
      meanHistos.push_back(Get2DHisto(prefix2+"_PtAvgFine_vs_EtaProbeSM"));
      
      ExtractPtEtaMean(meanHistos,h_weights,"SM",ptmin,ptmax);        
      MergeHistosAndMinimize(prefix, A3D_histos, etaSpectra, pTSpectra, trigDesc, A3D_histos_truth);      
    }
    /*    else if ( trigs.size() > 2 ){
          
      cout<<"=========================================== MERGING PTAVG BINS =========================================="<<endl;

      Str prefixFULL=currentTrig;
      
      Str tempTrigStr = currentTrig.ReplaceAll("AND"," ");
      StrV currentTrigORs = Vectorize(tempTrigStr);
      int NcurrentTrigORs = currentTrigORs.size();

      vector<TH2F*> meanHistos_sm;

      TH3F *h3dADD_j;
      TH3F *h3dADD_fj;

      TH1D *hADD_etaSpectra_j;
      TH1D *hADD_etaSpectra_fj;

      TH2F *hHadd_ptavg_etaavg_sm_j;
      TH2F *hHadd_ptavg_etaavg_sm_fj;

      Str prefix1;
      Str prefix2;
      Str suffix;

      int lowPtBin=-1, highPtBin=-1;
      Str TRIGSTR1="", TRIGSTR2="";

      for( int ior=0; ior<NcurrentTrigORs; ior++){

	Str prefix=""+systematics+"_"+jetAlgo+"_"+currentTrigORs[ior]+"_";
      	
	prefix1 = prefix+"RefFJ_J";
	prefix2 = prefix+"RefFJ_FJ";

	Str TRIG1 = trigs[2*ior];
	Str TRIG2 = trigs[2*ior+1];

	if ( ior < NcurrentTrigORs-1 ){
	  TRIGSTR1+=TRIG1+" AND ";
	  TRIGSTR2+=TRIG2+" AND ";
	}
	else{
	  TRIGSTR1+=TRIG1;
	  TRIGSTR2+=TRIG2;
	}

	printf("    Trig1: %10s, lumi: %.5f pb-1\n",TRIG1.Data(),CentrallumiMap[TRIG1]*1e-6);
	printf("    Trig2: %10s, lumi: %.5f pb-1\n",TRIG2.Data(),ForwardlumiMap[TRIG2]*1e-6);
	
	if (CentrallumiMap[TRIG1]<1e-9||ForwardlumiMap[TRIG2]<1e-9) error("Luminosity nonsense?");
	
	float PTMIN = PtBinMap[current_ptRange][ior], PTMAX = PtBinMap[current_ptRange][ior+1];
	
	h_weights.clear(); 
	
	// let's make an acutal x-sec measuremnt
	if ( _useWeights ){
	  h_weights.push_back(1.0/CentrallumiMap[TRIG1]*1e6);
	  h_weights.push_back(1.0/ForwardlumiMap[TRIG2]*1e6);
	}
	else{
	  h_weights.push_back(1.0);
	  h_weights.push_back(1.0);
	}
	
	TH2F *h2d_pTavgVSdeta_j = Get2DHisto(prefix1+"_PtAvg_vs_EtaDet");
	if(h2d_pTavgVSdeta_j==NULL) error("NULL eta HISTO J");
	TH2F *h2d_pTavgVSdeta_fj = Get2DHisto(prefix2+"_PtAvg_vs_EtaDet");
	if(h2d_pTavgVSdeta_fj==NULL) error("NULL eta HISTO FJ");

	h2d_pTavgVSdeta_j->Scale(h_weights[0]);
	h2d_pTavgVSdeta_fj->Scale(h_weights[1]);

	GetBinRange(h2d_pTavgVSdeta_j->GetYaxis(), PTMIN, PTMAX, lowPtBin, highPtBin);
	
	TH1D* etaSpectra_j = h2d_pTavgVSdeta_j->ProjectionX(prefix+TRIG1+ptRangeStr+"_EtaSpectrum",lowPtBin,highPtBin);
	TH1D* etaSpectra_fj = h2d_pTavgVSdeta_fj->ProjectionX(prefix+TRIG2+ptRangeStr+"_EtaSpectrum",lowPtBin,highPtBin);	

	Str suffix="";
	TH3F* hist3d_A_j  = GetAsymmetry3DHisto(prefix1,lowPtBin,highPtBin,suffix);
	if(hist3d_A_j->GetSumw2N()==0) hist3d_A_j->Sumw2(); 
	TH3F* hist3d_A_fj = GetAsymmetry3DHisto(prefix2,lowPtBin,highPtBin,suffix);
	if(hist3d_A_fj->GetSumw2N()==0) hist3d_A_fj->Sumw2(); 

	hist3d_A_j->Scale(h_weights[0]);
	hist3d_A_fj->Scale(h_weights[1]);
	
	TH2F *h_ptavg_etaavg_sm_j  = Get2DHisto(prefix1+"_PtAvgFine_vs_EtaProbeSM");
	TH2F *h_ptavg_etaavg_sm_fj = Get2DHisto(prefix2+"_PtAvgFine_vs_EtaProbeSM");
	h_ptavg_etaavg_sm_j->Scale(h_weights[0]);
	h_ptavg_etaavg_sm_fj->Scale(h_weights[1]);
	
	if ( ior == 0 ){
	  hHadd_ptavg_etaavg_sm_j  = (TH2F*)h_ptavg_etaavg_sm_j->Clone();
	  hHadd_ptavg_etaavg_sm_fj = (TH2F*)h_ptavg_etaavg_sm_fj->Clone();
	  hADD_etaSpectra_j = (TH1D*)etaSpectra_j->Clone();
	  hADD_etaSpectra_fj = (TH1D*)etaSpectra_fj->Clone();
	  h3dADD_j = (TH3F*)hist3d_A_j->Clone();
	  h3dADD_fj = (TH3F*)hist3d_A_fj->Clone();
	}
	else{
	  hHadd_ptavg_etaavg_sm_j->Add(h_ptavg_etaavg_sm_j);
	  hHadd_ptavg_etaavg_sm_fj->Add(h_ptavg_etaavg_sm_fj);
	  hADD_etaSpectra_j->Add(etaSpectra_j);
	  hADD_etaSpectra_fj->Add(etaSpectra_fj);
	  h3dADD_j->Add(hist3d_A_j);
	  h3dADD_fj->Add(hist3d_A_fj);
	}
      }

      trigDesc.push_back(TRIGSTR1);
      trigDesc.push_back(TRIGSTR2);
      
      h_weights.clear();
      h_weights.push_back(1.0);
      h_weights.push_back(1.0);

      meanHistos_sm.push_back(hHadd_ptavg_etaavg_sm_j);
      meanHistos_sm.push_back(hHadd_ptavg_etaavg_sm_fj);
      A3D_histos.push_back( h3dADD_j );
      A3D_histos.push_back( h3dADD_fj );
      etaSpectra.push_back(hADD_etaSpectra_j);
      etaSpectra.push_back(hADD_etaSpectra_fj);      
      
      if ( A3D_histos.size() != 2 ) error("Something went terribly wrong...");
      int Nevts = A3D_histos[0]->GetEntries()+A3D_histos[1]->GetEntries();
      
      if ( Nevts < 50 ) error(Form("No data for this pT-range! (%d events) ",Nevts));
      
      ExtractPtEtaMean(meanHistos_sm,h_weights,"SM",ptmin,ptmax);
      MergeHistosAndMinimize(prefixFULL, A3D_histos, etaSpectra, trigDesc, A3D_histos_truth);
      }*/
    else error(" Cannot understand the trigger inputs!!");
  }
} // minimize data

void ExtractPtEtaMean(vector<TH2F*> meanHistos, VecD weights, Str method, double pt_min, double pt_max) {

  int pmin, pmax, emin, emax;
  GetBinRange(meanHistos[0]->GetYaxis(),pt_min,pt_max,pmin,pmax);
  int Nh=meanHistos.size();

  TH2F *h_avgEta = histos2d[jetAlgo+"_AvgEta_vs_EtaPtAvg_"+method];
  if (h_avgEta==NULL) error("Can't access avg eta hist");

  for (int ei=0;ei<_etaBinsSM.size()-1;++ei) {

    double eta_min=_etaBinsSM[ei], eta_max=_etaBinsSM[ei+1];
    GetBinRange(meanHistos[0]->GetXaxis(),eta_min,eta_max,emin,emax);
    double sumw=0, sumw2=0, sumwx=0, sumwy=0, sumwxx=0, sumwyy=0;

    for (int ebi=emin;ebi<=emax;++ebi) {
      for (int pbi=pmin;pbi<=pmax;++pbi) {
	for (int hi=0;hi<Nh;++hi) {

	  double N=meanHistos[hi]->GetBinContent(ebi,pbi);
	  double eta=meanHistos[hi]->GetXaxis()->GetBinCenter(ebi);
	  double pt=meanHistos[hi]->GetYaxis()->GetBinCenter(pbi);

	  N *= weights[hi];
	  sumw += N; sumw2 += N*N; sumwx += N*eta; sumwy += N*pt; sumwxx += N*eta*eta; sumwyy += N*pt*pt;

	}
      }
    }

    double Neff=sumw*sumw/sumw2;
    if ( sumw==0 || Neff<1){
      cout<<"Continuing in ExtractPtEtaMean."<<endl;
      continue;
    }

    double avgEta=sumwx/sumw, avgPt=sumwy/sumw;
    double errEta=sqrt((sumwxx/sumw-avgEta*avgEta)/Neff);
    double errPt=sqrt((sumwyy/sumw-avgPt*avgPt)/Neff);

    int xbin=h_avgEta->GetXaxis()->FindBin((eta_min+eta_max)/2);
    int ybin=h_avgEta->GetYaxis()->FindBin((pt_min+pt_max)/2);
    h_avgEta->SetBinContent(xbin,ybin,avgEta);
    h_avgEta->SetBinError(xbin,ybin,errEta);
    histos2d[jetAlgo+"_AvgPtAvg_vs_EtaPtAvg_"+method]->SetBinContent(xbin,ybin,avgPt);
    histos2d[jetAlgo+"_AvgPtAvg_vs_EtaPtAvg_"+method]->SetBinError(xbin,ybin,errPt);
  } // for each eta bin
}

void MergeHistosAndMinimize(Str prefix, vector<TH3F*> A3D_histos, vector<TH1D*> etaSpectra, vector<TH1D*> pTSpectra, StrV desc, vector<TH3F*> A3D_histos_truth) {
 
  int Nhistos=A3D_histos.size();
  int Nhistos_truth=A3D_histos_truth.size();
  vector<int> SMstats(Nhistos,0), SMstats_truth(Nhistos_truth,0);

  int NbinsSM = _etaBinsSM.size()-1;
  int NptbinsSM = _ptBins.size()-1;

  printf("\n  ======\n  Standard method for %s\n",ptRangeDesc.Data()); cout << flush;

  TH1F *W_sm = new TH1F(jetAlgo+"_"+ptRangeStr+"_W_SM","Standard method width "+ptRangeDesc,NbinsSM,&_etaBinsSM[0]);
  W_sm->SetXTitle("probe jet #eta_{det}"); W_sm->SetYTitle("Width, #sigma");
  W_sm->SetMinimum(0.00); W_sm->SetMaximum(1.00); W_sm->SetMarkerSize(0.6);
  for (int i=0;i<=W_sm->GetNbinsX()+1;++i) {
    W_sm->SetBinContent(i,-99);
  }

  TAxis *fullAxis = A3D_histos[0]->GetXaxis();
  TAxis *fullAxisPt = A3D_histos[0]->GetZaxis();

  int refLowBin=-1, refHighBin=-1;
  if(!_absEta)
    GetBinRange(fullAxis, -refRegion[1], refRegion[1], refLowBin, refHighBin);
  else
    GetBinRange(fullAxis, 0, refRegion[1], refLowBin, refHighBin);
  
  if ( fabs(fullAxis->GetBinLowEdge(refLowBin)+refRegion[1]) > 1e-5 && !_absEta ) error(Form("Can't find eta bin-edge at %.2f",-refRegion[1]));
  if ( fabs(fullAxis->GetBinLowEdge(refHighBin+1)-refRegion[1]) > 1e-5 ) error(Form("Can't find eta bin-edge at %.2f",refRegion[1]));

  // make sure all projections DONT get anchored in the file .. just keep 'em in memory (noise reduction!)
  gROOT->cd(); int Nx=3, Ny=3; int cw=1; Can->Clear(); Can->Divide(Nx,Ny);

  // loop over probe jet eta bins
  //cout<<"SM N ETA BINS    : "<<NbinsSM<<endl;

  double sigma, dsigma, sig, dsig;
  vector< pair <double,double> > widthSigDsig;
 
  for (int etai=0;etai<NbinsSM;++etai) {

    int lowBin=-1, highBin=-1;
    double etaMin = _etaBinsSM[etai], etaMax = _etaBinsSM[etai+1];

    GetBinRange(fullAxis, etaMin, etaMax, lowBin, highBin);

    vector<TH1D*> Ahistos;
    int Nhistos=A3D_histos.size();
    
    for (uint hi=0;hi<Nhistos;++hi) {
      static int Aproj_i=0;

      // ref is left, probe right - mirrored situation as A = (left-right)/avg !
      TH1D *h1 = A3D_histos[hi]->ProjectionZ(Form("Aproj_sm%d",Aproj_i++),refLowBin,refHighBin,lowBin,highBin);
      // probe is left, ref right
      TH1D *h = A3D_histos[hi]->ProjectionZ(Form("Aproj_sm2%d",Aproj_i++),lowBin,highBin,refLowBin,refHighBin);
      
      InvertAndAdd(h,h1);      
      SMstats[hi] += h->GetSum();
      h->Scale(h_weights[hi]); 
      Ahistos.push_back(h);
    }

    Str Ahname=jetAlgo+"_"+ptRangeStr+"_A_etaBin"; Ahname+=etai;
    TH1D *hA = (TH1D*)Ahistos[0]->Clone(Ahname);

    for (uint hi=1;hi<Ahistos.size();++hi) hA->Add(Ahistos[hi]);
    
    outfile->cd(jetAlgo+"_Histos"); hA->Write(); gROOT->cd(); // Save the distribution    
    Can->cd(cw);

    if (cw==Nx*Ny+1) {
      Can->Print(AsymSM_psFile); cw=1;
      Can->Clear(); Can->Divide(Nx,Ny); Can->cd(cw);
    }

    double Neff = hA->GetEffectiveEntries();
    //    if (Neff<NeffMin) continue;

    double A=hA->GetMean(), dA=MeanErr(hA);
    TF1 *fit = DrawAhistos(hA,Ahistos,desc,true); cw++;
    
    tex->SetTextAlign(32); tex->SetTextColor(kBlack);
    tex->DrawLatex(0.91,0.88,ptRangeDesc);
    tex->DrawLatex(0.91,0.83,Form("%.1f #leq #eta_{ref} < %.1f",-refRegion[1],refRegion[1]));
    tex->DrawLatex(0.91,0.78,Form("%.1f #leq #eta_{probe} < %.1f",etaMin,etaMax));

    std::pair <double,double> AdA = JudgeFit(fit,A,dA,Neff);
    A = AdA.first; dA = AdA.second;    
      
    double R = (2.0+A)/(2.0-A), dR = 4.0/pow(2.0-A,2)*dA;
    sig = GetRMS(hA); dsig = RMSErr(hA);
    std::pair <double,double> sigdsig =GetWidth(fit, sig, dsig, Neff);
    //    if (sigdsig.first==-99) continue;
    sig = sigdsig.first;  dsig = sigdsig.second;
    widthSigDsig.push_back(std::make_pair(sig, dsig));
  }

  int middleBin;
  if ( NbinsSM % 2 == 0 ) middleBin = NbinsSM/2;
  else middleBin = (NbinsSM+1)/2;

  cout<<"Sigma in the reference region = "<<widthSigDsig[middleBin].first<<endl;

  for (int etai=0;etai<NbinsSM;++etai) {

    double etaMin = _etaBinsSM[etai], etaMax = _etaBinsSM[etai+1];

    cout<<"Sigma at start of new function = "<<widthSigDsig[etai].first<<endl;
    double widthCalcSig = widthSigDsig[etai].first; double widthCalcDsig = widthSigDsig[etai].second; double sigRef = widthSigDsig[9].first; double DsigRef = widthSigDsig[9].second;

    if ( etaMin >= -0.8 && etaMax <= 0.8 ){
      cout<<"etai in reference region = "<<etai<<endl;
      sigma = widthSigDsig[etai].first;  dsigma = widthSigDsig[etai].second;
    }
    else{
      Double_t sigb4sqrt = (2*pow(widthCalcSig,2))-(pow(sigRef,2));

      if( sigb4sqrt>0.00001 && sigb4sqrt<1. ){
	sigma = sqrt(sigb4sqrt);
	dsigma = sigma*(sqrt((2*DsigRef*DsigRef)/pow(sigRef,2) + (2*widthCalcDsig*widthCalcDsig)/2*pow(widthCalcSig,2)));
      }
      else{
	cout<<"Negative or greater than 1: Setting to 0 here."<<endl;                                                                                                          
	sigma = 0;
	dsigma = 0;
      }
    }
    cout<<"Sigma going into distribution = "<<sigma<<endl;
    W_sm->SetBinContent(etai+1,sigma);
    W_sm->SetBinError(etai+1,dsigma);
  }

  if (cw!=1) { Can->Print(AsymSM_psFile); }
  Can->Clear(); Can->Divide(2,2);
  tex->SetTextColor(kBlack); tex->SetTextSize(0.025); tex->SetTextAlign(22);
  tex->DrawLatex(0.5,0.5,"#font[62]{Standard Method Results}");
  tex->SetTextSize(0.04); tex->SetTextAlign(12);

  // 1. Eta spectrum
  Can->cd(1); DrawEtaSpectra(etaSpectra,false,SMstats,"SM statistics");

  // 2. pT spectrum 
  Can->cd(2); DrawEtaSpectra(pTSpectra,false,SMstats,"SM statistics pT Spectra");

  // 3. Width vs eta
  Can->cd(3); W_sm->SetMarkerSize(0.6); W_sm->Draw(); DrawGuideLines(); W_sm->Draw("same");
  PrintTrigAndPt(1); PrintData(1);

  Can->Print(Result_psFile);  
  outfile->cd(jetAlgo); W_sm->Write();
  std::cout << "end of MergeHistosAndMinimize" << std::endl;
}

std::pair<double,double> JudgeFit(TF1 *fit, double A, double dA, double Neff) {
  // Prepare for warning messages
  std::pair <double,double> result;
  tex->SetTextAlign(12); tex->SetTextSize(0.08); tex->SetTextColor(kBlack);

  if ( Neff<NeffMin ) {
    tex->DrawLatex(0.22,0.35,Form("Effective entries < %.1f",NeffMin));
    tex->DrawLatex(0.22,0.27,"This bin is ignored."); tex->SetTextSize(0.04);
    result.first = -99; result.second = -99;
    return result;
  }
  if (fit==NULL) {
    tex->DrawLatex(0.22,0.35,"Too course optimal bin-width."); 
    tex->DrawLatex(0.22,0.27,"Using mean."); tex->SetTextSize(0.04);
    result.first = A; result.second = dA; return result;
  }
  
  double Afit = fit->GetParameter(1), dAfit = fit->GetParError(1);
  double sig = fit->GetParameter(2), dsig = fit->GetParError(2);
  double chi2 = fit->GetChisquare(),  NDF = fit->GetNDF();

  bool badFit = dAfit/dA > 3 || dAfit > 0.1 || (dAfit > 0.05 && (fabs(A-Afit) > 2.0*dAfit)) || chi2/NDF>15 || fabs(Afit)>2.5*fabs(A); 

  result.first = A; result.second = dA;
  
  if ( _useFit && dAfit > 0 && !badFit && Neff>=NeffMin ) {
    result.first  = Afit;
    result.second = dAfit;
  } 

  tex->SetTextSize(0.04);
  return result;
}

std::pair<double,double> GetWidth(TF1 *fit, double rms, double drms, double Neff) {
  // Prepare for warning messages
  std::pair <double,double> result;
  if (fit==NULL) { cout<<"fit is null."<<endl; result.first = -99; result.second = -99; return result; }
  tex->SetTextAlign(12); tex->SetTextSize(0.08); tex->SetTextColor(kBlack);

  double sig = fit->GetParameter(2);
  double dsig = fit->GetParError(2);

  double sigpt = sig*sqrt(2.);
  double dsigpt = dsig*sqrt(2.);

  bool badFit = false;

  result.first = sigpt;
  result.second = dsigpt;
  cout<<"Width in function = "<<sigpt<<endl;

  tex->SetTextSize(0.04);
  return result;
}

double GetGaus() {
  double gaus=trand3.Gaus(0,1);
  if (maxNsigma<=0) return gaus;
  if (gaus>maxNsigma) gaus=maxNsigma;
  if (gaus<-maxNsigma) gaus=-maxNsigma;
  return gaus;
}

TEnv *Init(Str config) {
  
  SetAtlasStyle();
  gStyle->SetTitleBorderSize(0); gStyle->SetPalette(1);
  gStyle->SetTitleFillColor(kWhite);
  gStyle->SetTitleOffset(1.2,"X"); gStyle->SetTitleOffset(1.2,"Y");
  gStyle->SetTitleOffset(1.2,"Z");
  
  tex = new TLatex(); tex->SetNDC(); tex->SetTextSize(0.04);
  line = new TLine();

  TEnv *Settings = OpenSettingsFile(config);

  _etaBinsSM  = VectorizeD(Settings->GetValue("EtaBinsSM_"+_etaBinning,"")); 
  if (_etaBinsSM.size()==0) error("You need to specify EtaBinsSM in the config");

  VecD tempSMbins;
  if(_absEta){
    for(int ieta=0; ieta<=(_etaBinsSM.size()-1)/2; ieta++){
      tempSMbins.push_back(_etaBinsSM[ieta+(_etaBinsSM.size()-1)/2]);
      cout<<" ETA BINS SM ["<<ieta<<"] = "<<_etaBinsSM[ieta+(_etaBinsSM.size()-1)/2]<<endl;
    }
    _etaBinsSM = tempSMbins;
  }

  if(_isMC ) {
    MCsamples = Vectorize(Settings->GetValue(MCgen+".SampleNames",""));
    MCxsec    = VectorizeD(Settings->GetValue(MCgen+".CrossSections."+_productionTag,""));
    MCNevts   = VectorizeD(Settings->GetValue(MCgen+".NEvents."+_productionTag,""));
    MClumi    = Settings->GetValue("MCLumi",1.0);
    MCFiltEff =  VectorizeD(Settings->GetValue(MCgen+".FilterEff."+_productionTag,""));
    cout<<"MC generator          : "<<MCgen<<endl;
    cout<<"production tag        : "<<_productionTag<<endl;
    for(int i=0; i<MCsamples.size(); i++){ 
      cout<<"MCsample >>>>>>>>>>> "<<MCsamples[i]<<endl;
      cout<<"CX        : "<<MCxsec[i]<<endl;
      cout<<"N event   : "<<MCNevts[i]<<endl; 
      cout<<"Filt eff  : "<<MCFiltEff[i]<<endl;
    }    
    if (MCsamples.size()==0) error("No MC sample info for "+MCgen);
    
  }
  
  refRegion = VectorizeD(Settings->GetValue("ReferenceRegion","0.0 0.8") );
  if (refRegion.size()!=2) error("Incorrectly specified ReferencRegion");
  
  CentrallumiTrigs = Vectorize(Settings->GetValue("CentralLumiTriggers",""));
  Central_lumis = VectorizeD(Settings->GetValue("CentralLumis."+_productionTag,""));
  if (Central_lumis.size()!=CentrallumiTrigs.size()) error("CentralLumiTriggers and CentralLumis does not match");
  
  ForwardlumiTrigs = Vectorize(Settings->GetValue("ForwardLumiTriggers",""));
  Forward_lumis = VectorizeD(Settings->GetValue("ForwardLumis."+_productionTag,""));
  if (Forward_lumis.size()!=ForwardlumiTrigs.size()) error("ForwardLumiTriggers and ForwardLumis does not match");

  //save the lumi's conveniently
  for (uint li=0;li<Central_lumis.size();++li) CentrallumiMap[CentrallumiTrigs[li]]=Central_lumis[li];
  for (uint li=0;li<Forward_lumis.size();++li) ForwardlumiMap[ForwardlumiTrigs[li]]=Forward_lumis[li];

  trig_rndBit   = Vectorize(Settings->GetValue("pTrange_rnd",""));
  if( trig_rndBit.size()==0 ) error("must specify which triggers are random at L1+L2: required for lumi weighting");

  for (uint i=0;i<trig_rndBit.size();++i) {
    Str randTrig=trig_rndBit[i];
    cout<<"rnd trig : "<<randTrig<<endl;
    randTrig.ReplaceAll("or"," ").ReplaceAll("OR"," ");
    RNDtrigOR.push_back(VectorizeD(randTrig));
    cout<<"RND  1 : 2 = "<<RNDtrigOR[i][0]<<" : "<<RNDtrigOR[i][1]<<endl; 
  }

  Can = new TCanvas("Can","",800,600); 
  AsymSM_psFile = Str(outfileName).ReplaceAll(".root","")+"_AsymSM_distributions.ps";
  Result_psFile = Str(outfileName).ReplaceAll(".root","")+"_Results.ps";

  Can->Print(AsymSM_psFile+"[");
  Can->Print(Result_psFile+"[");
  
  return Settings;
}


void GetBinRange(TAxis *axis, double min, double max, int &minBin, int &maxBin) {
  int good=0; 

  for (uint i=1; i<=axis->GetNbins();++i) {
    double low  = axis->GetBinLowEdge(i);
    double high = axis->GetBinLowEdge(i+1);

    if (fabs(low-min)<1e-5) { minBin=i; good++; }
    if (fabs(high-max)<1e-5) { maxBin=i; good++; }
  }
  if (good!=2) error(Form("GetBinRange failed! %.2fto%.2f",min,max));
    
}

TH3F *GetAsymmetry3DHisto(Str prefix, int lowPtBin, int highPtBin, Str desc="") {
  Str ext="";
  Str NAME = prefix+Form("_Asym3D_PtBin%d%s"+desc,lowPtBin,ext.Data());
  cout<<" GETTING ASYMMETRY HISTO : "<<NAME.Data()<<endl;
  if (lowPtBin==highPtBin)
    return Get3DHisto(prefix+Form("_Asym3D_PtBin%d%s"+desc,lowPtBin,ext.Data()));

  cout<< " ADDING 3D HISTOS IN GET ASYMMETRY!!!"<<endl;
  TH3F *h = Get3DHisto(prefix+Form("_Asym3D_PtBin%d%s"+desc,lowPtBin,ext.Data()));  
  TH3F *comb = (TH3F*)h->Clone();
  for (int bin=lowPtBin+1;bin<=highPtBin;++bin) {
    comb->Add(Get3DHisto(prefix+Form("_Asym3D_PtBin%d%s"+desc,bin,ext.Data()))); }
  return comb;
}


TH3F *GetAsymmetry3DHistoTruth(Str prefix, int lowPtBin, int highPtBin, Str desc="") {
  
  Str ext="";
  if (lowPtBin==highPtBin)
    return GetTruth3DAsym(prefix+Form("_Asym3D_PtBin%d%s"+desc,lowPtBin,ext.Data()));
  TH3F *h = GetTruth3DAsym(prefix+Form("_Asym3D_PtBin%d%s"+desc,lowPtBin,ext.Data()));
  TH3F *comb = (TH3F*)h->Clone();
  for (int bin=lowPtBin+1;bin<=highPtBin;++bin) {
    comb->Add(GetTruth3DAsym(prefix+Form("_Asym3D_PtBin%d%s"+desc,bin,ext.Data()))); 
  }
  return comb;
}

void InvertAndAdd(TH1D *h, TH1D *h2) {

  int Nbins = h->GetNbinsX();
  if (h->GetSumw2N()==0) h->Sumw2();
  if (h2->GetSumw2N()==0) h2->Sumw2();

  for (int i=0;i<=Nbins+1;++i) {
    int i2 = Nbins+1-i;
    double x1=h->GetBinCenter(i), x2=h2->GetBinCenter(i2);
    double y1=h->GetBinContent(i), y2=h2->GetBinContent(i2);
    double e1=h->GetBinError(i), e2=h2->GetBinError(i2);
    h->SetBinContent(i,y1+y2); h->SetBinError(i,sqrt(e1*e1+e2*e2));
  }
}

#endif 
