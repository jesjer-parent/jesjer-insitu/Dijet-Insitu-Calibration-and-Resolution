#!/bin/sh                                                                                                                                             
code=HistFinalResolution.C
exec=draw_dijet_Resolution.exe

flagsNlibs="`$ROOTSYS/bin/root-config --cflags --glibs` -lTreePlayer -lHistPainter -lHist -lMatrix -lRIO -lTreePlayer -lMinuit -lMathCore"
rm -f $exec
gcc -std=c++11 $flagsNlibs -o $exec $code && {

    echo ; echo "Compilation successful" ; echo
 
    for Type in FinalData
    do
	config=${1:-"${Type}.config"}
	plotType=${2:-"${Type}"}
	./$exec $config $plotType
    done
}
#rm -f $exec
