/*
 *  Utils.h -- a collection of helper functions for
 *             HEP analysis
 */

#ifndef Utils_h
#define Utils_h

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <bitset>
#include <time.h>

#include "TROOT.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TChain.h"
#include "TFile.h"
#include "TEnv.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TString.h"
#include "TROOT.h"
#include "TApplication.h"
#include "TBox.h"
#include "TColor.h"
#include "TMarker.h"

#include "TH2.h"
#include "TH3.h"

#include <TGraphErrors.h>
#include <TGraph2DErrors.h>
#include "TProfile2D.h"
#include "TLorentzVector.h"

using namespace std;

typedef vector<TString> StrV;
typedef TString Str;
typedef unsigned int uint;
typedef vector<double> VecD;
typedef vector< vector<double> > VecVecD;
typedef vector<float> VecF;
typedef vector<int> VecI;
typedef TGraphErrors Graph;
typedef TGraph2DErrors Graph2D;

bool _verbose=false;


int mark[] = {20,21,22,23,26,25,24,27,28,29,30,20,2,3,5,31,32,33,34};
int mark2[] = {24,25,26,27,28,29,30,2,3,5,31,32,33,34,20,21,22,23,24};

///////////////////////////////////////////////////////////////////////////////////////

void error(Str msg) {
  printf("ERROR:\n\n  %s\n\n",msg.Data());
  abort();
}

//******************************************************************
//**************** vectorization methods/functions
//******************************************************************

StrV Vectorize(Str str, Str sep=" ") {
  StrV result; TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) return result;
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr())
    if (os->GetString()[0]!='#') result.push_back(os->GetString());
    else break;
  return result;
}

VecD VectorizeD(Str str) {
  VecD result; StrV vecS = Vectorize(str);
  for (uint i=0;i<vecS.size();++i)
    result.push_back(atof(vecS[i]));
  return result;
}

VecD MakeUniformVecD(int N, double min, double max) {
  VecD vec; double dx=(max-min)/N;
  for (int i=0;i<=N;++i) vec.push_back(min+i*dx);
  return vec;
}


VecD MakeLogVector(int N, double min, double max) {
  VecD vec; double dx=(log(max)-log(min))/N;
  for (int i=0;i<=N;++i) vec.push_back(exp(log(min)+i*dx));
  return vec;
}


//******************************************************************
//**************** functions for obtaining histos ect.
//******************************************************************

TObject* Get(TFile *f, Str jetAlgo, Str objName) {
  f->cd(jetAlgo);
  TObject *obj = gDirectory->Get(objName);
  if (obj==NULL) error("Can't access "+objName+" in "+f->GetName());
  return obj;
}

TH1D *GetHisto(TFile *f, Str jetAlgo, Str hname)
{ return (TH1D*)Get( f, jetAlgo, hname ); }
TH1D *GetHisto2(TFile *f, Str hn)
{ if (f->Get(hn)==NULL) error("Cannot access: "+hn+" in file "+f->GetName()); return (TH1D*)f->Get(hn); }

TH2D *Get2DHisto(TFile *f, Str jetAlgo, Str hname)
{ return (TH2D*)Get( f, jetAlgo, hname ); }
TH2D *Get2DHisto(TFile *f, Str hn)
{ if (f->Get(hn)==NULL) error("Cannot access: "+hn+" in file "+f->GetName()); return (TH2D*)f->Get(hn); }

//TH2F *GetTH2F(TFile *f, Str hn)
//{ if (f->Get(hn)==NULL) error("Cannot access: "+hn+" in file "+f->GetName()); return (TH2F*)f->Get(hn); }
//TH1F *GetTH1F(TFile *f, Str hn)
//{ if (f->Get(hn)==NULL) error("Cannot access: "+hn+" in file "+f->GetName()); return (TH1F*)f->Get(hn); }


//******************************************************************
//**************** functions to retrive calibration input points
//******************************************************************

Graph2D *GetCalibrationPoints(Graph2D *d, Graph2D *mcPrediction) {
  Graph2D *calib = new Graph2D();
  for (int i=0;i<d->GetN();++i) {
    double x=d->GetX()[i], y=d->GetY()[i];
    double ex=d->GetEX()[i], ey=d->GetEY()[i];
    double W_d = d->GetZ()[i], dW_d = d->GetEZ()[i];
    double W_mc = mcPrediction->GetZ()[i], dW_mc = mcPrediction->GetEZ()[i];
    double c=W_mc/W_d;
    //    cout<<"W_mc = "<<W_mc<<" W_d = "<<W_d<<" c = "<<c<<endl;
    double err_c=c*sqrt(dW_d*dW_d*pow(W_mc/W_d/W_d,2) + dW_mc*dW_mc/(W_d*W_d) );
    calib->SetPoint(i,x,y,c);
    calib->SetPointError(i,ex,ey,err_c);
  }
  return calib;
}

Graph *Ratio(Graph *g1, Graph *g2) {
  Graph *gr = new Graph();
  for (int i=0;i<g1->GetN();++i) {
    double x=g1->GetX()[i];
    double ex=g1->GetEX()[i];
    double W_g1 = g1->GetY()[i], dW_g1 = g1->GetEY()[i];
    double W_g2 = g2->GetY()[i], dW_g2 = g2->GetEY()[i];
    double c = 1;
    double err_c = 0;
    if ( W_g2 > 0. && W_g1 > 0. ){
      c=W_g2/W_g1;
      err_c=c*sqrt(dW_g1*dW_g1*pow(W_g2/W_g1/W_g1,2) + dW_g2*dW_g2/(W_g1*W_g1));
    }
    gr->SetPoint(i,x,c);
    gr->SetPointError(i,ex,err_c);
  }
  return gr;
}

Graph *RMSaverageGraph(Graph *g1, Graph *g2, Graph *g3){
  Graph *gr = new Graph();
  for (int i=0;i<g1->GetN();++i) {
    double x=g1->GetX()[i];
    double ex=g1->GetEX()[i];
    double W_g1 = g1->GetY()[i], dW_g1 = g1->GetEY()[i];
    double W_g2 = g2->GetY()[i], dW_g2 = g2->GetEY()[i];
    double W_g3 = g3->GetY()[i], dW_g3 = g3->GetEY()[i];
    double c=0.;
    double err_c=0.;

    vector<pair<double,double>> cVec;
    if (W_g1>0.001) cVec.push_back(make_pair(W_g1,dW_g1));
    if (W_g2>0.001) cVec.push_back(make_pair(W_g2,dW_g2));
    if (W_g3>0.001) cVec.push_back(make_pair(W_g3,dW_g3));

    //Find weighted average first:
    double WeightedAvg=0;
    double WAvg_err=0;
    for(int j = 0; j<cVec.size(); j++){
      WeightedAvg+=(cVec[j].first/(cVec[j].second*cVec[j].second));
    }
    double addedError=0;
    for(int k = 0; k<cVec.size(); k++){
      addedError+=1/(cVec[k].second*cVec[k].second);
    }
    WeightedAvg = WeightedAvg/addedError;
    WAvg_err = (dW_g1*dW_g1+dW_g2*dW_g2+dW_g3*dW_g3);

    //Then use to find difference from RMS
    double TempRMSDiff=0.;
    for(int j = 0; j<cVec.size(); j++){
      TempRMSDiff+=(cVec[j].first-WeightedAvg)*(cVec[j].first-WeightedAvg);
      cout<<"MCTruth = "<<cVec[j].first<<" WA = "<<WeightedAvg<<" TempRMSDiff iteration ["<<j<<"] = "<< TempRMSDiff<<endl;
    }

    c = sqrt(TempRMSDiff/cVec.size());
    err_c = (dW_g1*dW_g1+dW_g2*dW_g2+dW_g3*dW_g3);

    gr->SetPoint(i,x,c);
    gr->SetPointError(i,ex,err_c);
  }
  return gr;
}

Graph *averageGraph(Graph *g1, Graph *g2, Graph *g3){
  Graph *gr = new Graph();
  for (int i=0;i<g1->GetN();++i) {
    double x=g1->GetX()[i];
    double ex=g1->GetEX()[i];
    double W_g1 = g1->GetY()[i], dW_g1 = g1->GetEY()[i];
    double W_g2 = g2->GetY()[i], dW_g2 = g2->GetEY()[i];
    double W_g3 = g3->GetY()[i], dW_g3 = g3->GetEY()[i];
    //    cout<<"In average graph: W_g1 = "<<W_g1<<" W_g2 = "<<W_g2<<" W_g3 = "<<W_g3<<endl;
    //cout<<" errors: dW_g1 = "<<dW_g1<<" dW_g2 = "<<dW_g2<<" dW_g3 = "<<dW_g3<<endl;
    double c=0.;
    double err_c=0.;

    vector<pair<double,double>> cVec;
    if (W_g1>0.001) cVec.push_back(make_pair(W_g1,dW_g1));
    if (W_g2>0.001) cVec.push_back(make_pair(W_g2,dW_g2));
    if (W_g3>0.001) cVec.push_back(make_pair(W_g3,dW_g3));

    for(int j = 0; j<cVec.size(); j++){
      c+=(cVec[j].first/(cVec[j].second*cVec[j].second));
    }
    double addedError=0;
    for(int k = 0; k<cVec.size(); k++){
      addedError+=1/(cVec[k].second*cVec[k].second);
    }
    c = c/addedError;
    err_c = (dW_g1*dW_g1+dW_g2*dW_g2+dW_g3*dW_g3);

    gr->SetPoint(i,x,c);
    gr->SetPointError(i,ex,err_c);
  }
  return gr;
}

TH1D *averageHist(TH1D *h1, TH1D *h2, TH1D *h3){
  TH1D *hr = (TH1D*)h1->Clone();
  for (int i=1;i<=h1->GetNbinsX();++i) {
    double W_g1 = h1->GetBinContent(i), dW_g1 = h1->GetBinError(i); //NEED TO SORT OUT STAT ERROR
    double W_g2 = h2->GetBinContent(i), dW_g2 = h2->GetBinError(i);
    double W_g3 = h3->GetBinContent(i), dW_g3 = h3->GetBinError(i);
    double c=0., err_c=0.;

    vector<pair<double,double>> cVec;
    if (W_g1>0.001) cVec.push_back(make_pair(W_g1,dW_g1));
    if (W_g2>0.001) cVec.push_back(make_pair(W_g2,dW_g2));
    if (W_g3>0.001) cVec.push_back(make_pair(W_g3,dW_g3));

    for(int j = 0; j<cVec.size(); j++){
      c+=(cVec[j].first/(cVec[j].second*cVec[j].second));
      err_c+=(cVec[j].second*cVec[j].second);
    }
    double addedError=0;
    for(int k = 0; k<cVec.size(); k++){
      addedError+=1/(cVec[k].second*cVec[k].second);
    }
    c = c/addedError;
    err_c = err_c/addedError;

    hr->SetBinContent(i,c);
    hr->SetBinError(i,err_c);
  }
  return hr;
}

TH1D *NonWeightedAverageHist(TH1D *h1, TH1D *h2, TH1D *h3){
  TH1D *hr = (TH1D*)h1->Clone();
  for (int i=1;i<=h1->GetNbinsX();++i) {
    double W_g1 = h1->GetBinContent(i), dW_g1 = h1->GetBinError(i);
    double W_g2 = h2->GetBinContent(i), dW_g2 = h2->GetBinError(i);
    double W_g3 = h3->GetBinContent(i), dW_g3 = h3->GetBinError(i);
    double c=0., err_c=0.;

    vector<pair<double,double>> cVec;
    if (W_g1>0.001) cVec.push_back(make_pair(W_g1,dW_g1));
    if (W_g2>0.001) cVec.push_back(make_pair(W_g2,dW_g2));
    if (W_g3>0.001) cVec.push_back(make_pair(W_g3,dW_g3));

    for(int j = 0; j<cVec.size(); j++){
      c+=cVec[j].first;
      err_c+=(cVec[j].second*cVec[j].second);
    }
    c = c/cVec.size();
    err_c = err_c/cVec.size();

    hr->SetBinContent(i,c);
    hr->SetBinError(i,err_c);
  }
  return hr;
}


Graph *squareDifference(Graph *g1, Graph *g2){
  Graph *gr = new Graph();
  for (int i=0;i<g1->GetN();++i) {
    double x=g1->GetX()[i];
    double ex=g1->GetEX()[i];
    double W_g1 = g1->GetY()[i], dW_g1 = g1->GetEY()[i];
    double W_g2 = g2->GetY()[i], dW_g2 = g2->GetEY()[i];
    //    cout<<"W_g1 = "<<W_g1<<" W_g2 = "<<W_g2<<endl;
    double c=1.;
    double err_c=1.;

    if ( (W_g1*W_g1)-(W_g2*W_g2) < 0. || W_g1 < 0. || W_g2 < 0.){
      c = 0.;
      err_c = 0.;
    }
    else{
      c=sqrt((W_g1*W_g1)-(W_g2*W_g2));
      err_c=c*(2*sqrt((dW_g1*dW_g1*W_g1*W_g1) + (dW_g2*dW_g2*W_g2*W_g2)));
    }

    gr->SetPoint(i,x,c);
    gr->SetPointError(i,ex,err_c);
    //    std::cout<<" (squareDifference) i = "<<i<<" ; x = "<<x<<" resolution = "<<c<<std::endl;
  }
  return gr;
}

TH1D *SquareDifferenceHist(TH1D *h1, TH1D *h2){
  TH1D *hr = (TH1D*)h1->Clone();
  for (int i=1;i<=h1->GetNbinsX();++i) {
    double W_g1 = h1->GetBinContent(i), dW_g1 = h1->GetBinError(i); //NEED TO SORT OUT STAT ERROR!!!!!
    double W_g2 = h2->GetBinContent(i), dW_g2 = h2->GetBinError(i);
    double c=1., err_c=1.;

    if ( (W_g1*W_g1)-(W_g2*W_g2) < 0. || W_g1 < 0. || W_g2 < 0.){
      c = 0.;
      err_c = 0.;
    }
    else{
      c=sqrt((W_g1*W_g1)-(W_g2*W_g2));
      err_c=c*(2*sqrt((dW_g1*dW_g1*W_g1*W_g1) + (dW_g2*dW_g2*W_g2*W_g2)));
    }

    hr->SetBinContent(i,c);
    hr->SetBinError(i,err_c);
    //    std::cout<<" (squareDifference) i = "<<i<<" ; x = "<<x<<" resolution = "<<c<<std::endl;                                      
  }
  return hr;
}

Graph *Difference(Graph *g1, Graph *g2){
  Graph *gr = new Graph();
  for (int i=0;i<g1->GetN();++i) {
    double x=g1->GetX()[i];
    double ex=g1->GetEX()[i];
    double W_g1 = g1->GetY()[i], dW_g1 = g1->GetEY()[i];
    double W_g2 = g2->GetY()[i], dW_g2 = g2->GetEY()[i];
    //    cout<<"W_g1 = "<<W_g1<<" W_g2 = "<<W_g2<<endl;
    double c=1.;
    double err_c=1.;

    if ( W_g1 < 0.0001 || W_g2 < 0.0001 || std::isnan(-W_g1) || std::isnan(-W_g2) ){
      c=0.;
      err_c=0.;
    }
    else{
      c=W_g1-W_g2;
      err_c=c*(sqrt((dW_g1*dW_g1*W_g1*W_g1) + (dW_g2*dW_g2*W_g2*W_g2)));
    }
    //    cout<<"W_g1 = "<<W_g1<<" W_g2 = "<<W_g2<<" c = "<<c<<endl;
    gr->SetPoint(i,x,c);
    gr->SetPointError(i,ex,err_c);
  }
  return gr;
}

TH1D *DifferenceHist(TH1D *h1, TH1D *h2){
  TH1D *hr = (TH1D*)h1->Clone();
  for (int i=1;i<=h1->GetNbinsX();++i) {
    double W_g1 = h1->GetBinContent(i), dW_g1 = h1->GetBinError(i);
    double W_g2 = h2->GetBinContent(i), dW_g2 = h2->GetBinError(i);
    double c=1., err_c=1., avg=1.;

    if ( W_g1 < 0. || W_g2 < 0. || std::isnan(-W_g1) || std::isnan(-W_g2) ){
      c = 0.;
      err_c = 0.;
    }
    else{
      c=sqrt((W_g1-W_g2)*(W_g1-W_g2));
    }
    hr->SetBinContent(i,c);
  }
  return hr;
}

Graph2D* TrimGraph(Graph2D *g1, Graph2D *g2, Graph2D *g3, double maxPt=1500) {
  double maxErr =  0.04;
  int n=g1->GetN(), nn=0; Graph2D *g = new Graph2D();
  if (n!=g2->GetN()||n!=g3->GetN()){
    cout<<" Npoints g1 : g2 : g3 == "<<n<<" : "<<g2->GetN()<<" : "<<g3->GetN()<<endl;
    error("Trimming issue!");
  }
  for (int i=0;i<n;++i) {
    if (g1->GetZ()[i]<=0||g2->GetZ()[i]<=0||g3->GetZ()[i]<=0) continue;
    bool high_pT = ( g1->GetX()[i]>100 || g2->GetX()[i]>100 || g3->GetX()[i]>100 );
    if ( high_pT &&(g1->GetEZ()[i]>maxErr||g2->GetEZ()[i]>maxErr||g3->GetEZ()[i]>maxErr)) continue;
    g->SetPoint(nn,g1->GetX()[i],g1->GetY()[i],g1->GetZ()[i]);
    g->SetPointError(nn,g1->GetEX()[i],g1->GetEY()[i],g1->GetEZ()[i]);
    nn++;
  }
  return g;
}

Graph2D* GetCalibration2DGraph(TFile *f, Str jetAlgo, Str method, bool isMCRes, double pTmax=0 ){
  Graph2D *calibGraph = new Graph2D();
  Str endMethod = method;
  TH2D *avg_pt  = Get2DHisto(f,jetAlgo+"_AvgPtAvg_vs_EtaPtAvg_"+endMethod);
  TH2D *avg_eta = Get2DHisto(f,jetAlgo+"_AvgEta_vs_EtaPtAvg_"+endMethod);
  int Npt=avg_pt->GetNbinsY(), Neta=avg_eta->GetNbinsX();
  for (int ipt=1;ipt<=Npt;++ipt) {
    double ptmin = avg_pt->GetYaxis()->GetBinLowEdge(ipt);
    double ptmax = avg_pt->GetYaxis()->GetBinLowEdge(ipt+1);
    if(pTmax>0 && pTmax<=ptmin){
      continue;
    }
    Str Whname=jetAlgo+"/"+jetAlgo+"_"+Form("pt%.0fto%.0f_W_",ptmin,ptmax)+method;
    TH1D *hW = GetHisto2(f,Whname);
    if (hW->GetNbinsX()!=Neta) error("Binning problem");
    for (int ieta=1;ieta<=Neta;++ieta) {
      int n=calibGraph->GetN();
      double W=hW->GetBinContent(ieta);
      double dW       = hW->GetBinError(ieta);
      double pt       = avg_pt->GetBinContent(ieta,ipt);
      double err_pt   = avg_pt->GetBinError(ieta,ipt);
      double eta      = avg_eta->GetBinContent(ieta,ipt);
      double err_eta  = avg_eta->GetBinError(ieta,ipt);
      double etamin = avg_pt->GetXaxis()->GetBinLowEdge(ieta);
      double etamax = avg_pt->GetXaxis()->GetBinLowEdge(ieta+1);
      calibGraph->SetPoint(n,pt,eta,W);
      calibGraph->SetPointError(n,err_pt,err_eta,dW);
    }
  }
  return calibGraph;
}
// printf("In GetCalibration2DGraph ( ptmin : ptmax :n : pt : eta : W : ieta )  = ( %.0f : %.0f : %d : %.0f : %.1f : %.4f : %d )\\n",ptmin, ptmax, n, pt, eta, W, ieta);

TH2D* GetCalibration2DHist(TFile *f, Str jetAlgo, Str method, bool isMCRes, double pTmax=0 ){
  Str endMethod = method;
  TH2D *avg_pt  = Get2DHisto(f,jetAlgo+"_AvgPtAvg_vs_EtaPtAvg_"+endMethod);
  TH2D *avg_eta = Get2DHisto(f,jetAlgo+"_AvgEta_vs_EtaPtAvg_"+endMethod);
  int Npt=avg_pt->GetNbinsY(), Neta=avg_eta->GetNbinsX();

  vector<double> pTBins; //Getting pTBins
  for (int ipt=1;ipt<=Npt;++ipt) {
    double ptLowEdge = avg_pt->GetYaxis()->GetBinLowEdge(ipt);
    pTBins.push_back(ptLowEdge);
  }
  double ptLast = avg_pt->GetYaxis()->GetBinLowEdge(Npt+1);
  pTBins.push_back(ptLast);

  vector<double> etaBins; //Getting EtaBins
  for (int ieta=1;ieta<=Neta;++ieta) {
    double etaLowEdge = avg_eta->GetXaxis()->GetBinLowEdge(ieta);
    etaBins.push_back(etaLowEdge);
  }
  double etaLast = avg_eta->GetXaxis()->GetBinLowEdge(Neta+1);
  etaBins.push_back(etaLast);  

  TH2D *calibHist = new TH2D("calibHist","WidthDistribution",Npt,&pTBins[0],Neta,&etaBins[0]);
 
  for (int ipt=1;ipt<=Npt;++ipt) {
    double ptmin = avg_pt->GetYaxis()->GetBinLowEdge(ipt);
    double ptmax = avg_pt->GetYaxis()->GetBinLowEdge(ipt+1);
    if(pTmax>0 && pTmax<=ptmin) continue;
    Str Whname=jetAlgo+"/"+jetAlgo+"_"+Form("pt%.0fto%.0f_W_",ptmin,ptmax)+method;
    TH1D *hW = GetHisto2(f,Whname);
    if (hW->GetNbinsX()!=Neta) error("Binning problem");

    for (int ieta=1;ieta<=Neta;++ieta) {
      double W=hW->GetBinContent(ieta);
      double dW       = hW->GetBinError(ieta);
      double pt       = avg_pt->GetBinContent(ieta,ipt);
      double err_pt   = avg_pt->GetBinError(ieta,ipt);
      double eta      = avg_eta->GetBinContent(ieta,ipt);
      double err_eta  = avg_eta->GetBinError(ieta,ipt);
      double etamin = avg_pt->GetXaxis()->GetBinLowEdge(ieta);
      double etamax = avg_pt->GetXaxis()->GetBinLowEdge(ieta+1);
      //      cout<<"( pt : eta : W ) = ( "<<pt<<" : "<<eta<<" : "<<W<<" )"<<endl;
      Int_t bin = calibHist->GetBin(ipt,ieta);
      calibHist->SetBinContent(bin,W);
      calibHist->SetBinError(bin,dW);
    }
  }
  return calibHist;
}

void PrintPoint(Graph2D *g, int i) {
  printf("Point %3d (x,y,z) = (%6.1f,%6.2f,%7.4f ) +/- (%6.1f,%6.2f,%7.4f)\n",
	 i,g->GetX()[i],g->GetY()[i],g->GetZ()[i],g->GetEX()[i],g->GetEY()[i],g->GetEZ()[i]);
}

Graph2D *GetAvg(Graph2D *g1, Graph2D *g2) {
  if (g1->GetN()!=g2->GetN()) error("GetAvg error.");
  Graph2D *g = new Graph2D();
  for (int i=0;i<g1->GetN();++i) {
    double x1=g1->GetX()[i], y1=g1->GetY()[i];
    double ex1=g1->GetEX()[i], ey1=g1->GetEY()[i];
    double z1=g1->GetZ()[i], z2=g2->GetZ()[i];
    double e1=g1->GetEZ()[i], e2=g2->GetEZ()[i];
    g->SetPoint(i,x1,y1,(z1+z2)/2);
    g->SetPointError(i,ex1,ey1,0.5*sqrt(e1*e1+e2*e2));
  }
  return g;
}

Graph2D *GetWeightedAvg(Graph2D *g1, Graph2D *g2) {
  if (g1->GetN()!=g2->GetN()) error("GetAvg error.");
  Graph2D *g = new Graph2D();
  for (int i=0;i<g1->GetN();++i) {
    double x1=g1->GetX()[i], y1=g1->GetY()[i];
    double ex1=g1->GetEX()[i], ey1=g1->GetEY()[i];
    double z1=g1->GetZ()[i], z2=g2->GetZ()[i];
    double e1=g1->GetEZ()[i], e2=g2->GetEZ()[i];
    g->SetPoint(i,x1,y1,(z1*e1+z2*e2)/(e1+e2));
    g->SetPointError(i,ex1,ey1,0.5*sqrt(e1*e1+e2*e2));
  }
  return g;
}


//******************************************************************
//**************** functions to retrive calibration pointshistos and values
//******************************************************************


VecI GetWvsPtPoints(Graph2D *g2d, double etamin, double etamax, double maxPt=1500) {
  VecI res;
  for (int pi=0;pi<g2d->GetN();++pi) {
    double eta=g2d->GetY()[pi];
    if(g2d->GetX()[pi]>maxPt) continue;
    if (etamin<eta&&eta<etamax) res.push_back(pi);
  }
  return res;
}

VecI GetWvsEtaPoints(Graph2D *g2d, double ptmin, double ptmax) {
  VecI res;
  for (int pi=0;pi<g2d->GetN();++pi) {
    double pt=g2d->GetX()[pi], W=g2d->GetZ()[pi];
    if (ptmin<pt&&pt<ptmax) res.push_back(pi);
  }
  return res;
}

Graph *GetWvsEtaGraph(Graph2D *g2d, VecI points) {
  Graph *res = new Graph(); double sumpt=0;

  for (int pi=0;pi<points.size();++pi) {
    int n=res->GetN();
    int index=points[pi];    
    res->SetPoint(n,g2d->GetY()[index],g2d->GetZ()[index]);
    res->SetPointError(n,g2d->GetEY()[index],g2d->GetEZ()[index]);
  }
  return res;
}

Graph *GetWvsEtaGraph(Graph2D *g2d, double ptmin, double ptmax) {
  // fix the pt range
  Graph *res = new Graph(); double sumpt=0;

  for (int pi=0;pi<g2d->GetN();++pi) {
    double pt=g2d->GetX()[pi];
    double W=g2d->GetZ()[pi];
    if (ptmin<pt&&pt<ptmax) {
      int n=res->GetN(); sumpt+=pt;
      res->SetPoint(n,g2d->GetY()[pi],g2d->GetZ()[pi]);
      res->SetPointError(n,g2d->GetEY()[pi],g2d->GetEZ()[pi]);
    }
  }

  int n=res->GetN();
  return res;
}

Graph *GetWvsPtGraph(Graph2D *g2d, double etamin, double etamax) {

  Graph *res = new Graph(); double sumeta=0;
  for (int pi=0;pi<g2d->GetN();++pi) {
    double eta=g2d->GetY()[pi];
    if (etamin<eta&&eta<etamax) {
      int n=res->GetN(); sumeta+=eta;
      res->SetPoint(n,g2d->GetX()[pi],g2d->GetZ()[pi]);
      res->SetPointError(n,g2d->GetEX()[pi],g2d->GetEZ()[pi]);
    }
  }
  int n=res->GetN();
  return res;
}


Graph *GetWvsPtGraph(Graph2D *g2d, VecI points) {
  Graph *res = new Graph(); double sumeta=0;
  for (int pi=0;pi<points.size();++pi) {
    int n=res->GetN();
    int index=points[pi];
    res->SetPoint(n,g2d->GetX()[index],g2d->GetZ()[index]);
    res->SetPointError(n,g2d->GetEX()[index],g2d->GetEZ()[index]);
  }
  return res;
}



double GetAvgEta(Graph2D *g2d, VecI points) {
  double sumw=0, sumwx=0;
  for (int pi=0;pi<points.size();++pi) {
    int i=points[pi];
    double e=g2d->GetEY()[i]; if (e<=0) continue;
    double w=1.0/e/e; sumw+=w; sumwx+=w*g2d->GetY()[i];
  }
  return sumw==0?0:sumwx/sumw;
}

double GetAvgPt(Graph2D *g2d, VecI points) {
  double sumw=0, sumwx=0;
  for (int pi=0;pi<points.size();++pi) {
    int i=points[pi]; double e=g2d->GetEX()[i]; if (e<=0) continue;
    double w=1.0/e/e; sumw+=w; sumwx+=w*g2d->GetX()[i];
  }
  return sumw==0?0:sumwx/sumw;
}


TH1D *GetSmoothVsEta(TH2D *h2d, double avg, double min, double max) {
  static int si=0;
  TH1D *h = new TH1D(Form("vseta%d",++si),"",200,min,max);
  for (int i=1;i<=h->GetNbinsX();++i) {
    double eta=h->GetBinCenter(i);
    h->SetBinContent(i,h2d->Interpolate(avg,eta));
  }
  return h;
}

TH1D *GetSmoothVsPt(TH2D *h2d, double avg, double min, double max) {
  static int si=0;
  VecD bins=MakeLogVector(200,min,max);
  TH1D *h = new TH1D(Form("vspt%d",++si),"",200,&bins[0]);
  for (int i=1;i<=h->GetNbinsX();++i) {
    double pt=h->GetBinCenter(i);
    h->SetBinContent(i,h2d->Interpolate(pt,avg));
  }
  return h;

}


//************** dealing with errors

TH1D* AddError(TH1D *h, TH1D *herr, int color) {
  static int a=0;
  TH1D *hh = (TH1D*)herr->Clone(Form("cerr%d",a++));
  for (int i=1;i<=h->GetNbinsX();++i) {

    double binctr = h->GetXaxis()->GetBinCenter(i);
    int errbin = herr->GetXaxis()->FindBin(binctr);
    hh->SetBinContent(i,h->GetBinContent(i));
    hh->SetBinError(i,herr->GetBinContent(errbin)*h->GetBinContent(i));
  }
  hh->SetFillColor(color);
  hh->SetMarkerStyle(1);
  return hh;
}


TH2D *AddInQuad(vector<TH2D*> hvec, Str hname) {
  TH2D *total = (TH2D*)hvec[0]->Clone(hname);
  total->Reset();
  for (int ipt=0;ipt<=total->GetNbinsX()+1;++ipt) {
    for (int ieta=0;ieta<=total->GetNbinsY()+1;++ieta) {
      double z=0, sumz2=0;
      for (int hi=0;hi<hvec.size();++hi) {
	z=hvec[hi]->GetBinContent(ipt,ieta);
	sumz2+=z*z;
      }
      total->SetBinContent(ipt,ieta,sqrt(sumz2));
    }
  }
  return total;
}

TH1D *AddInQuad(TH1D *h1, TH1D *h2, TH1D *h3, TH1D *h4){
  TH1D *total = (TH1D*)h1->Clone();
  for ( int bini=1; bini<=total->GetNbinsX(); bini++ ){
    double x=0., y=0., z=0., q=0., sum2=0.;
      x=h1->GetBinContent(bini);
      y=h2->GetBinContent(bini);
      z=h3->GetBinContent(bini);
      q=h4->GetBinContent(bini);
      sum2=(x*x)+(y*y)+(z*z)+(q*q);
      total->SetBinContent(bini,sqrt(sum2));
  }
  return total;
}

TH1D *AddInQuad(TH1D *h1, TH1D *h2, TH1D *h3){
  TH1D *total = (TH1D*)h1->Clone();
  for ( int bini=1; bini<=total->GetNbinsX(); bini++ ){
    double x=0., y=0., z=0., sum2=0.;
    x=h1->GetBinContent(bini);
    y=h2->GetBinContent(bini);
    z=h3->GetBinContent(bini);
    sum2=(x*x)+(y*y)+(z*z);
    total->SetBinContent(bini,sqrt(sum2));
  }
  return total;
}


TH1D* AddErrorToUnity(TH1D *herr, int color) {
  static int a=0;
  TH1D *hh = (TH1D*)herr->Clone(Form("cerr%d",a++));
  for (int i=1;i<=herr->GetNbinsX();++i) {
    hh->SetBinContent(i,1.0);
    hh->SetBinError(i,herr->GetBinContent(i));
  }

  hh->SetFillColor(color);
  hh->SetMarkerStyle(1);
  return hh;
}

// methods for opening TEnv files
TEnv *OpenSettingsFile(Str fileName) {
  if (fileName=="") error("No config file name specified. Cannot open file!");
  TEnv *settings = new TEnv();
  int status=settings->ReadFile(fileName.Data(),EEnvLevel(0));
  if (status!=0) error(Form("Cannot read file %s",fileName.Data()));
  return settings;
}

StrV ReadFile(TString fileName) {
  StrV lines;
  ifstream file(fileName.Data());
  string line, lastline="weeee";
  while (getline(file,line)) {
    if (line==lastline) continue;
    if (line[0]==' ') continue;
    StrV subLines=Vectorize(line,",");
    for (int i=0;i<subLines.size();++i)
      lines.push_back(subLines[i]);
  }
  return lines;
}

//**************** cool method to keep track of runtimes ****************************//

Str getTime()
{
  time_t aclock;
  ::time( &aclock );
  return Str(asctime( localtime( &aclock )));
}



void PrintTime()
{
  static bool first=true;
  static time_t start;
  if(first) { first=false; ::time(&start); }
  time_t aclock; ::time( &aclock );
  char tbuf[25]; ::strncpy(tbuf, asctime( localtime( &aclock ) ),24);
  tbuf[24]=0;
  cout <<  "Current time: " << tbuf
       << " ( "<< ::difftime( aclock, start) <<" s elapsed )" << std::endl;
}




#endif // #ifdef Utils_h
