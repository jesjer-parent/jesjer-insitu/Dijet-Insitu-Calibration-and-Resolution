#include "Utils_W.h"
#include "DrawingUtils.h"
#include "atlasstyle/AtlasStyle.C"
#include "atlasstyle/AtlasLabels.C"
#include <string>
#include "TLegend.h"

// Boolians for pdf plot type (nominal or systematic uncertainty cross check)
bool nominal = false;
bool MCnominal = false;
bool MCIntrinsic = false;
bool MCTruthWeighting = false;
bool ExperimentalSystShift = false;
bool MCIntrinsicClosure = false;
bool FinalJERPlots = true;

Str outfn = "DijetResolution_RootFileTest";
Str plotType="";
Str plotCheck="nominal";

void GetPtEtaBins(Str jetAlgo, Str file, VecD &etaBins, VecD& ptBins);
TH1D *ProjectVsEta(TH2D *h, double avgPt, double maxPt = -1 );
TH1D *ProjectVsPt(TH2D *h, double eta);
TH1D *GetSystEnvelopeError(TH1D *nominal, vector<TH1D*> UncertHist);
TH1D *GetNonClosureError(TH1D *h1, TH1D *h2);
TH1D *GetWeightedAverageError(TH1D *h1, TH1D *h2, TH1D *h3);
TH1D *GetAllUncertainties(TH1D *h1, TH1D *h2, TH1D *h3, TH1D *h4);
TH1D *GetSystUncertainties(TH1D *h1, TH1D *h2, TH1D *h3);
TH1D *GetStatError(TH1D *h);

void InitialiseStyleSettings(){
  gErrorIgnoreLevel=2000;SetAtlasStyle();gStyle->SetPalette(1);gStyle->SetNumberContours(100);gStyle->SetPadLeftMargin(0.12);gStyle->SetPadTopMargin(0.04);gStyle->SetPadBottomMargin(0.12);
  return;
}

void ATLASLabel(Double_t x,Double_t y,const char* text,Color_t color=1)
{
  TLatex l;l.SetTextAlign(22);l.SetNDC();l.SetTextSize(0.045);l.SetTextFont(72);l.SetTextColor(color);
  double delx = 0.15;
  l.DrawLatex(x,y,"ATLAS"); l.DrawLatex(x+0.18,y-0.09,"#sqrt{s} = 13 TeV  #int L dt = 36.1 fb^{-1}");
  if (text) {
    TLatex p;p.SetTextAlign(22);p.SetNDC();p.SetTextSize(0.045);p.SetTextFont(42);p.SetTextColor(color);p.DrawLatex(x+delx,y,text);
  }
}

int main(int argc, char **argv) {
  InitialiseStyleSettings();
  TCanvas *can  = new TCanvas("can","",800,600);
  Str config; StrV jetAlgos;

  if ( argc<2 ) error("Need to provide two arguments, e.g. plots.config MCvsData");
  for (int i=1;i<=argc;++i) {
    Str arg=argv[i];
    if (arg.Contains("config")) config = arg;
    else if (arg=="") continue;
    else {
      if ( arg.Contains("FinalData") ) plotType = "FinalData";
      else if ( arg.Contains("FinalMC") ) plotType = "FinalMC";
      else error("Don't recognise plot type "+plotType);
    }
  }
  cout << "Config file:  " << config << endl;

  TFile * outfile = new TFile("Resolution_"+plotType+".root", "RECREATE");
  outfn += "_"+plotType+"_DijetBalanceMethod.ps";
  can->Print(outfn+"[");

  // Get input names from config file
  TEnv *settings = OpenSettingsFile(config);
  jetAlgos =  Vectorize(settings->GetValue(plotType+".JetAlgos","") );
  StrV files = Vectorize(settings->GetValue(plotType+".InputFiles",""));
  if ( files.size()==0 ) error("Failed to get input files.");
  StrV methods = Vectorize(settings->GetValue(plotType+".Methods",""));
  int jetR=4; if (!jetAlgos[0].Contains("Kt4")) error("Not Kt4!");
  Str jetAlgo = jetAlgos[0];

  // get pTavg and eta bins from input files
  VecD etaBins, ptBins;
  GetPtEtaBins(jetAlgo,files[0],etaBins,ptBins);

  // get 2D Width Hists
  vector< TH2D* > Hist2D; TFile* f;
  for ( int fi=0; fi<files.size(); fi++ ) {
    f = Open(files[fi]);
    Hist2D.push_back( GetCalibration2DHist(f,jetAlgos[fi],methods[fi],true) );
  }

  //---------------------------------------------------------------------------------------------------------
  // HISTOGRAMS OF RESOLUTION VS ETA
  //---------------------------------------------------------------------------------------------------------
  TH1D *heta = new TH1D("heta","",1,etaBins[0],etaBins[etaBins.size()-1]); FormatHisto(heta,1);
  TLatex* tex = new TLatex();tex->SetNDC();tex->SetTextColor(kBlack);tex->SetTextSize(0.04);tex->SetTextAlign(12);

  // loop over pT bins
  for (int ipt=0;ipt<ptBins.size()-1;++ipt) {
    float ptmin=ptBins[ipt]; float ptmax=ptBins[ipt+1]; float ptavg = (ptmin+ptmax)/2;
    can->Clear(); heta->Draw();

    vector< TH1D* > eta_Hist;
    for ( int ig=0; ig<Hist2D.size(); ++ig ) {
      heta = ProjectVsEta(Hist2D[ig],ptavg,ptmax);
      heta->SetName(Form("Resolution_pt%.0fto%.0f",ptmin,ptmax));
      eta_Hist.push_back(heta);
    } // Projection of width vs eta from 3D distributions

    // Average of nominal width distributions
    TH1D *averageMCTruthHist = averageHist(eta_Hist[1],eta_Hist[2],eta_Hist[3]);
    TH1D *averageMCResHist = averageHist(eta_Hist[4],eta_Hist[5],eta_Hist[6]); averageMCResHist->GetYaxis()->SetTitle("#sigma(p_{T})/p_{T}"); averageMCResHist->GetXaxis()->SetTitle("#eta_{det}");
    TH1D *averageMCRecoHist = averageHist(eta_Hist[7],eta_Hist[8],eta_Hist[9]);

    // Producing the nominal resolution of width(data) - width(avgMCTruth)
    TH1D *Res_Diff = SquareDifferenceHist(eta_Hist[0],averageMCTruthHist); FormatHisto(Res_Diff,1);
    Res_Diff->GetYaxis()->SetTitle("#sigma(p_{T})/p_{T}"); Res_Diff->GetXaxis()->SetTitle("#eta_{det}");
    Res_Diff->SetMinimum(0.);Res_Diff->SetMaximum(0.8);Res_Diff->GetXaxis()->SetLabelColor(1);Res_Diff->GetXaxis()->SetTitleOffset(1.0);
    Res_Diff->Draw();

    // Individual MC resolutions (Reco - Truth)
    TH1D *PowhegPythia_RecoMinusTruth = SquareDifferenceHist(eta_Hist[7],eta_Hist[1]);
    TH1D *Sherpa_RecoMinusTruth = SquareDifferenceHist(eta_Hist[8],eta_Hist[2]);
    TH1D *Pythia_RecoMinusTruth = SquareDifferenceHist(eta_Hist[9],eta_Hist[3]);

    //-------------------- UNCERTAINTY CALCULATIONS ------------------------------------

    // Calculation of uncertainty from non-closure test of MCRes Vs (MCReco - MCTruth)
    TH1D *MCDifference_RecoMinusTruth = SquareDifferenceHist(averageMCRecoHist,averageMCTruthHist);
    TH1D *NonClosureErrorHist = GetNonClosureError(MCDifference_RecoMinusTruth,averageMCResHist);

    // Calculation of uncertainty from weighting the MCTruth for the average (MCTruth average vs weighted MCTruth average) 
    TH1D *TruthWeightingError = GetWeightedAverageError(eta_Hist[1],eta_Hist[2],eta_Hist[3]);

    // Calculation of uncertainty from all systematic uncertainties
    vector< TH1D* > SystNP;
    for ( int ig=0; ig<5; ig++){
      TH1D *NPData = eta_Hist[10+(ig*4)];
      TH1D *NPAvgTruth = averageHist(eta_Hist[11+(ig*4)],eta_Hist[12+(ig*4)],eta_Hist[13+(ig*4)]);
      SystNP.push_back(SquareDifferenceHist(NPData,NPAvgTruth));
    }//for loop to get resolution for each shift
    TH1D *SystErrorEnvelope = GetSystEnvelopeError(Res_Diff, SystNP);

    TH1D *StatError = GetStatError(Res_Diff); // Collect Statistical Error

    // ADD ALL UNCERTAINTIES AND ADD TO HISTOGRAM
    TH1D *allErrors = GetAllUncertainties(NonClosureErrorHist,TruthWeightingError,SystErrorEnvelope,StatError);
    for ( int bini=1; bini<=Res_Diff->GetNbinsX(); bini++ ){
      Res_Diff->SetBinError(bini,allErrors->GetBinContent(bini));
    }

    outfile->cd(); Res_Diff->Write(); MCDifference_RecoMinusTruth->Write(Form("MCResolution_pt%.0fto%.0f",ptmin,ptmax)); //Writing nominal data and MC histogram to root file

    // Producing Uncertainty Histograms
    SystErrorEnvelope->Draw(); SystErrorEnvelope->Write(Form("Uncertainty_SystEnvelope_pt%.0fto%.0f",ptmin,ptmax));
    TruthWeightingError->Draw(); TruthWeightingError->Write(Form("Uncertainty_TruthWeighting_pt%.0fto%.0f",ptmin,ptmax));
    NonClosureErrorHist->Draw(); NonClosureErrorHist->Write(Form("Uncertainty_MCNonClosure_pt%.0fto%.0f",ptmin,ptmax));
    StatError->Draw(); StatError->Write(Form("Uncertainty_Statistical_pt%.0fto%.0f",ptmin,ptmax));
    allErrors->Draw(); allErrors->Write(Form("Uncertainty_Total_pt%.0fto%.0f",ptmin,ptmax));

    // Separating Syst and Stat errors for plots
    TH1D *ErrorBars = (TH1D*)Res_Diff->Clone("Errors"); ErrorBars->SetFillColor(kRed); ErrorBars->SetFillStyle(3335);
    TH1D *SystError = GetSystUncertainties(NonClosureErrorHist,TruthWeightingError,SystErrorEnvelope);
    for ( int bini=1; bini<=Res_Diff->GetNbinsX(); bini++ ){
      Res_Diff->SetBinError(bini,StatError->GetBinContent(bini));
      ErrorBars->SetBinError(bini,SystError->GetBinContent(bini));
    }

    // Pad for neater plots
    TPad* mainPad = new TPad("mainPad","", 0.00, 0.00, 0.99, 0.99); mainPad->Draw(); mainPad->cd();
    if (nominal || FinalJERPlots) Res_Diff->Draw();
    else if (MCnominal) {
      FormatHisto(MCDifference_RecoMinusTruth,1); MCDifference_RecoMinusTruth->GetYaxis()->SetTitle("#sigma(p_{T})/p_{T}"); MCDifference_RecoMinusTruth->GetXaxis()->SetTitle("#eta_{det}");
      MCDifference_RecoMinusTruth->SetMinimum(0.);MCDifference_RecoMinusTruth->SetMaximum(0.8);MCDifference_RecoMinusTruth->GetXaxis()->SetLabelColor(1);MCDifference_RecoMinusTruth->GetXaxis()->SetTitleOffset(1.0);
      MCDifference_RecoMinusTruth->Draw();
    }
    else if (MCIntrinsic){
      FormatHisto(averageMCResHist,1); averageMCResHist->GetYaxis()->SetTitle("#sigma(p_{T})/p_{T}"); averageMCResHist->GetXaxis()->SetTitle("#eta_{det}");
      averageMCResHist->SetMinimum(0.);averageMCResHist->SetMaximum(0.8);averageMCResHist->GetXaxis()->SetLabelColor(1);averageMCResHist->GetXaxis()->SetTitleOffset(1.0);
      averageMCResHist->Draw();
  }
    else if (MCTruthWeighting){
      eta_Hist[3]->SetMinimum(0.); eta_Hist[3]->SetMaximum(0.6); FormatHisto(eta_Hist[3],7); eta_Hist[3]->Draw();
    }
    else if (ExperimentalSystShift){
      SystErrorEnvelope->SetFillColor(kAzure-9);SystErrorEnvelope->SetLineColor(kAzure-9);SystErrorEnvelope->SetFillStyle(3002);SystErrorEnvelope->SetMinimum(0.);SystErrorEnvelope->SetMaximum(0.3);
      SystErrorEnvelope->Draw("hist");
    }
    else if (MCIntrinsicClosure){
      averageMCResHist->SetMinimum(0.); averageMCResHist->SetMaximum(0.6); FormatHisto(averageMCResHist,2); averageMCResHist->GetYaxis()->SetTitle("#sigma(p_{T})/p_{T}"); averageMCResHist->GetXaxis()->SetTitle("#eta_{det}"); averageMCResHist->Draw();
    }

    ATLASLabel(0.15,0.89,(char*)"Internal",kBlack);
    DrawText(tex,"Dijet Balance Method",kBlack,0.83,0.15);
    DrawText(tex,"#sqrt{s} = 13 TeV, 36.1 fb^{-1}",kBlack,0.77,0.15);
    DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4",kBlack,0.71,0.15);
    DrawText(tex,Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),kBlack,0.9,0.58);

    auto legend = new TLegend(0.42,0.65,0.92,0.85);

    // PLOTS FOR NOMINAL DISTRIBUTION AND CROSS CHECKS FOR SYSTEMATICS (CHECK BOOLIANS AT TOP OF SCRIPT)
    if (nominal){
      ErrorBars->Draw("e2p same");
      Res_Diff->Draw("ep same");
      legend->AddEntry(Res_Diff,"Dijet Balance, #sqrt{[#sigma_{jet}^{EM+JES}]^{2} - [#sigma_{Truth}^{MC}]^{2}} ","pl");
      legend->AddEntry(ErrorBars,"Systematic Uncertainty","f");
    }
    else if (MCnominal){
      plotCheck = "MCnominal";
      MCDifference_RecoMinusTruth->Draw("ep same");
      FormatHisto(PowhegPythia_RecoMinusTruth,2); FormatHisto(Sherpa_RecoMinusTruth,3); FormatHisto(Pythia_RecoMinusTruth,4);
      PowhegPythia_RecoMinusTruth->Draw("ep same"); Sherpa_RecoMinusTruth->Draw("ep same"); Pythia_RecoMinusTruth->Draw("ep same");
      legend->AddEntry(MCDifference_RecoMinusTruth,"MC Dijet Balance Avg, #sqrt{[#sigma_{jet}^{MC}]^{2} - [#sigma_{Truth}^{MC}]^{2}} ","pl");
      legend->AddEntry(PowhegPythia_RecoMinusTruth, "Powheg+Pythia Dijet Balance","pl");
      legend->AddEntry(Sherpa_RecoMinusTruth, "Sherpa Dijet Balance","pl");
      legend->AddEntry(Pythia_RecoMinusTruth, "Pythia Dijet Balance","pl");

    }
    else if (MCIntrinsic){
      plotCheck = "MCIntrinsic";
      averageMCResHist->Draw("ep same");
      FormatHisto(eta_Hist[4],5); FormatHisto(eta_Hist[5],6); FormatHisto(eta_Hist[6],7);
      eta_Hist[4]->Draw("ep same"); eta_Hist[5]->Draw("ep same"); eta_Hist[6]->Draw("ep same");
      legend->AddEntry(eta_Hist[4],"Powheg+Pythia Intrinsic","pl");
      legend->AddEntry(eta_Hist[5],"Sherpa Intrinsic","pl");
      legend->AddEntry(eta_Hist[6],"Pythia8 Intrinsic","pl");
      legend->AddEntry(averageMCResHist,"Average MC Intrinsic","pl");
    }
    else if (MCTruthWeighting){
      plotCheck = "MCTruthWeighting";
      FormatHisto(eta_Hist[1],5); FormatHisto(eta_Hist[2],6); FormatHisto(eta_Hist[3],7);
      eta_Hist[1]->Draw("ep same"); eta_Hist[2]->Draw("ep same"); eta_Hist[3]->Draw("ep same");
      TH1D *AverageTruth = NonWeightedAverageHist(eta_Hist[1],eta_Hist[2],eta_Hist[3]); FormatHisto(AverageTruth,2); AverageTruth->Draw("ep same");
      TH1D *WeightedAverageTruth = averageHist(eta_Hist[1],eta_Hist[2],eta_Hist[3]); FormatHisto(WeightedAverageTruth,3); WeightedAverageTruth->Draw("ep same");
      TH1D *WeightedAverageError = DifferenceHist(AverageTruth, WeightedAverageTruth); FormatHisto(WeightedAverageError,4); WeightedAverageError->Draw("ep same");

      legend->AddEntry(eta_Hist[1],"Powheg+Pythia Truth","pl");
      legend->AddEntry(eta_Hist[2],"Sherpa Truth","pl");
      legend->AddEntry(eta_Hist[3],"Pythia8 Truth","pl");
      legend->AddEntry(AverageTruth,"Average MC Truth","pl");
      legend->AddEntry(WeightedAverageTruth,"Weighted Average MC Truth","pl");
      legend->AddEntry(WeightedAverageError,"Weighted Average Error","pl");
    }
    else if (ExperimentalSystShift){
      plotCheck = "ExperimentalSystShift";
      SystErrorEnvelope->Draw("hist same");
      vector< TH1D* > SystNPDiff;
      for ( int i=0; i<5; i++ ){
	SystNPDiff.push_back(DifferenceHist(Res_Diff, SystNP[i])); FormatHisto(SystNPDiff[i],i+2); SystNPDiff[i]->Draw("ep same");
      }
      legend->AddEntry(SystErrorEnvelope,"SystErrorEnvelope","f");
      legend->AddEntry(SystNPDiff[0],"dphiDown","pl");
      legend->AddEntry(SystNPDiff[1],"dphiUp","pl");
      legend->AddEntry(SystNPDiff[2],"j3down","pl");
      legend->AddEntry(SystNPDiff[3],"j3up","pl");
      legend->AddEntry(SystNPDiff[4],"jvtTight","pl");
    }
    else if (MCIntrinsicClosure){
      plotCheck = "MCIntrinsicClosure";
      FormatHisto(MCDifference_RecoMinusTruth,1); MCDifference_RecoMinusTruth->Draw("ep same");
      FormatHisto(averageMCResHist,2); averageMCResHist->Draw("ep same");
      FormatHisto(NonClosureErrorHist,3); NonClosureErrorHist->Draw("ep same");
      legend->AddEntry(MCDifference_RecoMinusTruth,"MCReco - MCTruth","pl");
      legend->AddEntry(averageMCResHist,"Average Intrinsic MC Resolution","pl");
      legend->AddEntry(NonClosureErrorHist,"Non Closure Error","pl");
    }
    else if (FinalJERPlots){
      plotCheck = "FinalJERPlots";
      ErrorBars->Draw("e2p same");
      Res_Diff->Draw("ep same");
      FormatHisto(averageMCResHist,3); averageMCResHist->Draw("ep same");
      legend->AddEntry(averageMCResHist,"Average MC Intrinsic","pl");
      legend->AddEntry(Res_Diff,"Dijet Balance, #sqrt{[#sigma_{jet}^{EM+JES}]^{2} - [#sigma_{Truth}^{MC}]^{2}} ","pl");
      legend->AddEntry(ErrorBars,"Systematic Uncertainty","f");
    }


    legend->Draw();

    // Print out pdf document for neater plots
    can->Print(outfn); can->Print(Form(plotType+"/"+plotCheck+"_%1.0fpTavg%1.0f_"+plotType+"_DijetBalanceMethod.pdf",ptmin,ptmax));
  }

  //---------------------------------------------------------------------------------------------------------
  // HISTOGRAMS OF RESOLUTION VS PT (Everything again but vs pT - apart from checks on systematics)
  //---------------------------------------------------------------------------------------------------------
  TH1D *hpt = new TH1D("hpt","",1,ptBins[0],ptBins[ptBins.size()-1]); FormatHisto(hpt,1);
  
  // loop over eta bins
  for (int ieta=0;ieta<etaBins.size()-1;++ieta) {
    float etamin=etaBins[ieta]; float etamax=etaBins[ieta+1]; float etaavg = (etamin+etamax)/2;
    can->Clear(); hpt->Draw();
    
    vector< TH1D* > pt_Hist;
    for ( int ig=0; ig<Hist2D.size(); ++ig ) {
      hpt = ProjectVsPt(Hist2D[ig],etaavg);
      hpt->SetName(Form("Resolution_eta%.1fto%.1f",etamin,etamax));
      pt_Hist.push_back(hpt);
    } // Pulling the width vs pT projections from the 3D histograms

    // Average nominal width distributions
    TH1D *averageMCTruthHist = averageHist(pt_Hist[1],pt_Hist[2],pt_Hist[3]);
    TH1D *averageMCResHist = averageHist(pt_Hist[4],pt_Hist[5],pt_Hist[6]);
    TH1D *averageMCRecoHist = averageHist(pt_Hist[7],pt_Hist[8],pt_Hist[9]);
    // Producing the nominal resolution of width(data) - width(avgMCTruth)
    TH1D *Res_Diff = SquareDifferenceHist(pt_Hist[0],averageMCTruthHist); FormatHisto(Res_Diff,1);
    Res_Diff->SetYTitle("#sigma(p_{T})/p_{T}"); Res_Diff->SetXTitle("p_{T} [GeV]");
    Res_Diff->SetMinimum(0.0);Res_Diff->SetMaximum(0.8);Res_Diff->GetXaxis()->SetLabelColor(1);Res_Diff->GetXaxis()->SetTitleOffset(1.1);
    Res_Diff->Draw();

    // Individual MC resolutions (Reco - Truth)                                                                                           
    TH1D *PowhegPythia_RecoMinusTruth = SquareDifferenceHist(pt_Hist[7],pt_Hist[1]);
    TH1D *Sherpa_RecoMinusTruth = SquareDifferenceHist(pt_Hist[8],pt_Hist[2]);
    TH1D *Pythia_RecoMinusTruth = SquareDifferenceHist(pt_Hist[9],pt_Hist[3]);

    //-------------------- UNCERTAINTY CALCULATIONS ------------------------------------

    // Calculation of uncertainty from non-closure test of MCRes Vs (MCReco - MCTruth)
    TH1D *MCDifference_RecoMinusTruth = SquareDifferenceHist(averageMCRecoHist,averageMCTruthHist);
    TH1D *NonClosureErrorHist = GetNonClosureError(MCDifference_RecoMinusTruth,averageMCResHist);

    // Calculation of uncertainty from weighting the MCTruth for the average (MCTruth average vs weighted MCTruth average)
    TH1D *TruthWeightingError = GetWeightedAverageError(pt_Hist[1],pt_Hist[2],pt_Hist[3]);

    // Calculation of uncertainty from all experimental systematic uncertainties
    vector< TH1D* > SystNP;
    for ( int ig=0; ig<5; ig++){
      TH1D *NPData = pt_Hist[10+(ig*4)];
      TH1D *NPAvgTruth = averageHist(pt_Hist[11+(ig*4)],pt_Hist[12+(ig*4)],pt_Hist[13+(ig*4)]);
      SystNP.push_back(SquareDifferenceHist(NPData,NPAvgTruth));
    } //for loop to get resolution for each shift
    TH1D *SystErrorEnvelope = GetSystEnvelopeError(Res_Diff, SystNP);

    TH1D *StatError = GetStatError(Res_Diff); // Collect Statistical Error

    // ADD ALL UNCERTAINTIES AND ADD TO HISTOGRAM
    TH1D *allErrors = GetAllUncertainties(NonClosureErrorHist,TruthWeightingError,SystErrorEnvelope,StatError);
    for ( int bini=1; bini<=Res_Diff->GetNbinsX(); bini++ ){
      Res_Diff->SetBinError(bini,allErrors->GetBinContent(bini));
      //      cout<<"Bin Error ("<<bini<<") = "<<allErrors->GetBinContent(bini)<<endl;
    }

    outfile->cd(); Res_Diff->Write();    

    // Producing Uncertainty histograms
    SystErrorEnvelope->Draw(); SystErrorEnvelope->Write(Form("Uncertainty_SystEnvelope_eta%.1fto%.1f",etamin,etamax));
    TruthWeightingError->Draw(); TruthWeightingError->Write(Form("Uncertainty_TruthWeighting_eta%.1fto%.1f",etamin,etamax));
    NonClosureErrorHist->Draw(); NonClosureErrorHist->Write(Form("Uncertainty_MCNonClosure_eta%.1fto%.1f",etamin,etamax));
    StatError->Draw(); StatError->Write(Form("Uncertainty_Statistical_eta%.1fto%.1f",etamin,etamax));
    allErrors->Draw(); allErrors->Write(Form("Uncertainty_Total_eta%.1fto%.1f",etamin,etamax));

    // Separating Syst and Stat errors for plots
    TH1D *ErrorBars = (TH1D*)Res_Diff->Clone("Errors"); ErrorBars->SetFillColor(kRed); ErrorBars->SetFillStyle(3335);
    TH1D *SystError = GetSystUncertainties(NonClosureErrorHist,TruthWeightingError,SystErrorEnvelope);
    for ( int bini=1; bini<=Res_Diff->GetNbinsX(); bini++ ){
      Res_Diff->SetBinError(bini,StatError->GetBinContent(bini));
      ErrorBars->SetBinError(bini,SystError->GetBinContent(bini));
    }

    // Pad for neater plots
    TPad* mainPad = new TPad("mainPad","", 0.00, 0.00, 0.99, 0.99); mainPad->Draw(); mainPad->cd();
    if (nominal || FinalJERPlots) Res_Diff->Draw();
    else if (MCnominal) {
      FormatHisto(MCDifference_RecoMinusTruth,1); MCDifference_RecoMinusTruth->GetYaxis()->SetTitle("#sigma(p_{T})/p_{T}"); MCDifference_RecoMinusTruth->GetXaxis()->SetTitle("p_{T}");
      MCDifference_RecoMinusTruth->SetMinimum(0.);MCDifference_RecoMinusTruth->SetMaximum(0.8);MCDifference_RecoMinusTruth->GetXaxis()->SetLabelColor(1);MCDifference_RecoMinusTruth->GetXaxis()->SetTitleOffset(1.0);
      MCDifference_RecoMinusTruth->Draw();
    }
    else if (MCIntrinsic){
      FormatHisto(averageMCResHist,1); averageMCResHist->GetYaxis()->SetTitle("#sigma(p_{T})/p_{T}"); averageMCResHist->GetXaxis()->SetTitle("p_{T}");
      averageMCResHist->SetMinimum(0.);averageMCResHist->SetMaximum(0.8);averageMCResHist->GetXaxis()->SetLabelColor(1);averageMCResHist->GetXaxis()->SetTitleOffset(1.0);
      averageMCResHist->Draw();
    }
    else if (MCTruthWeighting){
      pt_Hist[3]->SetMinimum(0.); pt_Hist[3]->SetMaximum(0.6); FormatHisto(pt_Hist[3],7); pt_Hist[3]->Draw();
    }
    else if (ExperimentalSystShift){
      SystErrorEnvelope->SetFillColor(kAzure-9);SystErrorEnvelope->SetLineColor(kAzure-9);SystErrorEnvelope->SetFillStyle(3002);SystErrorEnvelope->SetMinimum(0.);SystErrorEnvelope->SetMaximum(0.3);
      SystErrorEnvelope->Draw("hist");
    }
    else if (MCIntrinsicClosure){
      averageMCResHist->SetMinimum(0.); averageMCResHist->SetMaximum(0.6); FormatHisto(averageMCResHist,2); averageMCResHist->GetYaxis()->SetTitle("#sigma(p_{T})/p_{T}"); averageMCResHist->GetXaxis()->SetTitle("p_{T}"); averageMCResHist->Draw();
    }

    ATLASLabel(0.15,0.89,(char*)"Internal",kBlack);
    DrawText(tex,"Dijet Balance Method",kBlack,0.83,0.15);
    DrawText(tex,"#sqrt{s} = 13 TeV, 36.1 fb^{-1}",kBlack,0.77,0.15);
    DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4",kBlack,0.71,0.15);
    DrawText(tex,Form("%.1f #leq #eta_{det} < %.1f",etamin,etamax),kBlack,0.9,0.58);
    
    auto legend = new TLegend(0.42,0.65,0.92,0.85);
    // PLOTS FOR NOMINAL DISTRIBUTION AND CROSS CHECKS FOR SYSTEMATICS (CHECK BOOLIANS AT TOP OF SCRIPT)                                  
    if (nominal){
      ErrorBars->Draw("e2p same");
      Res_Diff->Draw("ep same");
      legend->AddEntry(Res_Diff,"Dijet Balance, #sqrt{[#sigma_{jet}^{EM+JES}]^{2} - [#sigma_{Truth}^{MC}]^{2}} ","pl");
      legend->AddEntry(ErrorBars,"Systematic Uncertainty","f");
    }
    else if (MCnominal){
      plotCheck = "MCnominal";
      MCDifference_RecoMinusTruth->Draw("ep same");
      FormatHisto(PowhegPythia_RecoMinusTruth,2); FormatHisto(Sherpa_RecoMinusTruth,3); FormatHisto(Pythia_RecoMinusTruth,4);
      PowhegPythia_RecoMinusTruth->Draw("ep same"); Sherpa_RecoMinusTruth->Draw("ep same"); Pythia_RecoMinusTruth->Draw("ep same");
      legend->AddEntry(MCDifference_RecoMinusTruth,"MC Dijet Balance Avg, #sqrt{[#sigma_{jet}^{MC}]^{2} - [#sigma_{Truth}^{MC}]^{2}} ","pl");
      legend->AddEntry(PowhegPythia_RecoMinusTruth, "Powheg+Pythia Dijet Balance","pl");
      legend->AddEntry(Sherpa_RecoMinusTruth, "Sherpa Dijet Balance","pl");
      legend->AddEntry(Pythia_RecoMinusTruth, "Pythia Dijet Balance","pl");
    }
    else if (MCIntrinsic){
      plotCheck = "MCIntrinsic";
      averageMCResHist->Draw("ep same");
      FormatHisto(pt_Hist[4],5); FormatHisto(pt_Hist[5],6); FormatHisto(pt_Hist[6],7);
      pt_Hist[4]->Draw("ep same"); pt_Hist[5]->Draw("ep same"); pt_Hist[6]->Draw("ep same");
      legend->AddEntry(pt_Hist[4],"Powheg+Pythia Intrinsic","pl");
      legend->AddEntry(pt_Hist[5],"Sherpa Intrinsic","pl");
      legend->AddEntry(pt_Hist[6],"Pythia8 Intrinsic","pl");
      legend->AddEntry(averageMCResHist,"Average MC Intrinsic","pl");
    }
    else if (MCTruthWeighting){
      plotCheck = "MCTruthWeighting";
      FormatHisto(pt_Hist[1],5); FormatHisto(pt_Hist[2],6); FormatHisto(pt_Hist[3],7);
      pt_Hist[1]->Draw("ep same"); pt_Hist[2]->Draw("ep same"); pt_Hist[3]->Draw("ep same");
      TH1D *AverageTruth = NonWeightedAverageHist(pt_Hist[1],pt_Hist[2],pt_Hist[3]); FormatHisto(AverageTruth,2); AverageTruth->Draw("ep same");
      TH1D *WeightedAverageTruth = averageHist(pt_Hist[1],pt_Hist[2],pt_Hist[3]); FormatHisto(WeightedAverageTruth,3); WeightedAverageTruth->Draw("ep same");
      TH1D *WeightedAverageError = DifferenceHist(AverageTruth, WeightedAverageTruth); FormatHisto(WeightedAverageError,4); WeightedAverageError->Draw("ep same");

      legend->AddEntry(pt_Hist[1],"Powheg+Pythia Truth","pl");
      legend->AddEntry(pt_Hist[2],"Sherpa Truth","pl");
      legend->AddEntry(pt_Hist[3],"Pythia8 Truth","pl");
      legend->AddEntry(AverageTruth,"Average MC Truth","pl");
      legend->AddEntry(WeightedAverageTruth,"Weighted Average MC Truth","pl");
      legend->AddEntry(WeightedAverageError,"Weighted Average Error","pl");
    }
    else if (ExperimentalSystShift){
      plotCheck = "ExperimentalSystShift";
      SystErrorEnvelope->Draw("hist same");
      vector< TH1D* > SystNPDiff;
      for ( int i=0; i<5; i++ ){
        SystNPDiff.push_back(DifferenceHist(Res_Diff, SystNP[i])); FormatHisto(SystNPDiff[i],i+2); SystNPDiff[i]->Draw("ep same");
      }
      legend->AddEntry(SystErrorEnvelope,"SystErrorEnvelope","f");
      legend->AddEntry(SystNPDiff[0],"dphiDown","pl");
      legend->AddEntry(SystNPDiff[1],"dphiUp","pl");
      legend->AddEntry(SystNPDiff[2],"j3down","pl");
      legend->AddEntry(SystNPDiff[3],"j3up","pl");
      legend->AddEntry(SystNPDiff[4],"jvtTight","pl");
    }
    else if (MCIntrinsicClosure){
      plotCheck = "MCIntrinsicClosure";
      FormatHisto(MCDifference_RecoMinusTruth,1); MCDifference_RecoMinusTruth->Draw("ep same");
      FormatHisto(averageMCResHist,2); averageMCResHist->Draw("ep same");
      FormatHisto(NonClosureErrorHist,3); NonClosureErrorHist->Draw("ep same");
      legend->AddEntry(MCDifference_RecoMinusTruth,"MCReco - MCTruth","pl");
      legend->AddEntry(averageMCResHist,"Average Intrinsic MC Resolution","pl");
      legend->AddEntry(NonClosureErrorHist,"Non Closure Error","pl");
    }
    else if (FinalJERPlots){
      plotCheck = "FinalJERPlots";
      ErrorBars->Draw("e2p same");
      Res_Diff->Draw("ep same");
      FormatHisto(averageMCResHist,3); averageMCResHist->Draw("ep same");
      legend->AddEntry(averageMCResHist,"Average MC Intrinsic","pl");
      legend->AddEntry(Res_Diff,"Dijet Balance, #sqrt{[#sigma_{jet}^{EM+JES}]^{2} - [#sigma_{Truth}^{MC}]^{2}} ","pl");
      legend->AddEntry(ErrorBars,"Systematic Uncertainty","f");
    }

    legend->Draw();

    // Print out pdf document for neater plots
    can->Print(outfn); can->Print(Form(plotType+"/"+plotCheck+"_eta%.1fto%.1f_"+plotType+"_DijetBalanceMethod.pdf",etamin,etamax));
  }
  return 0;
}

void GetPtEtaBins(Str jetAlgo, Str file, VecD &etaBins, VecD& ptBins) {

  TFile *f_data_temp = Open(file);
  TH2D *h_pT_temp  = Get2DHisto(f_data_temp,jetAlgo+"_AvgPtAvg_vs_EtaPtAvg_"+"SM");
  for( int ipt=0; ipt<=h_pT_temp->GetNbinsY(); ipt++) ptBins.push_back(h_pT_temp->GetYaxis()->GetBinLowEdge(ipt+1) );
  int NptBins=ptBins.size()-1;
  for( int ieta=0; ieta<=h_pT_temp->GetNbinsX(); ieta++) etaBins.push_back(h_pT_temp->GetXaxis()->GetBinLowEdge(ieta+1) );
  int NetaBins=etaBins.size()-1;
  delete h_pT_temp;
  f_data_temp->Close();

  return;
}

TH1D *ProjectVsEta(TH2D *h, double avgPt, double maxPt ) {
  int ptBin=h->GetXaxis()->FindBin(avgPt);
  if (maxPt>0 && avgPt>maxPt) ptBin=h->GetXaxis()->FindBin(maxPt);
  TH1D *p = h->ProjectionY(Form("%s_%d",h->GetName(),ptBin),ptBin,ptBin);
  return p;
}

TH1D *ProjectVsPt(TH2D *h, double eta) {
  int etaBin=h->GetYaxis()->FindBin(eta);
  TH1D *p = h->ProjectionX(Form("%s_pt%d",h->GetName(),etaBin),etaBin,etaBin);
  p->SetLineColor(kViolet+3); p->SetLineWidth(3);
  return p;
}

//-----------------------------------------------
// STATISTICAL UNCERTAINTY FUNCTION
//-----------------------------------------------

TH1D *GetStatError(TH1D *h){
  TH1D *StatError = (TH1D*)h->Clone();
  for ( int bini=1; bini<=h->GetNbinsX(); bini++ ){
    StatError->SetBinContent(bini,h->GetBinError(bini));
  }
  return StatError;
}

//-----------------------------------------------
// EXPERIMENTAL UNCERTAINTY FUNCTIONS
//-----------------------------------------------

// Vector of all errors from systematic variations
TH1D *GetSystEnvelopeError(TH1D *nominal, vector<TH1D*> UncertHist){

  TH1D *SystErrorHist = (TH1D*)nominal->Clone();

   vector< TH1D* > SystNPDiff;
   for ( int i=0; i<5; i++ ){
     SystNPDiff.push_back(DifferenceHist(nominal, UncertHist[i]));
   }

   for ( int bini=1; bini<=nominal->GetNbinsX(); bini++ ){
     double maxUncert = 0.0;
     for ( const auto& h : SystNPDiff ){
       if ( maxUncert <= h->GetBinContent(bini) && h->GetBinContent(bini) <=0.13){
	 maxUncert = h->GetBinContent(bini);
       }
     }
     //     cout<<"maxUncert = "<<maxUncert<<endl;
     SystErrorHist->SetBinContent(bini,maxUncert);
   }
   return SystErrorHist;
}

//-----------------------------------------------
// MC UNCERTAINTY FUNCTIONS
//-----------------------------------------------

// MCRes vs (MCReco-MCTruth) Dijet balance
TH1D *GetNonClosureError(TH1D *h1, TH1D *h2){

  TH1D *NonClosure = DifferenceHist(h1, h2);
  return NonClosure;
}

// Average MC Truth vs Weighted average MC Truth 
TH1D *GetWeightedAverageError(TH1D *h1, TH1D *h2, TH1D *h3){

  TH1D *AverageTruth = NonWeightedAverageHist(h1, h2, h3);
  TH1D *WeightedAverageTruth = averageHist(h1, h2, h3);
  TH1D *WeightedAverageError = DifferenceHist(AverageTruth, WeightedAverageTruth);
  return WeightedAverageError;

}

//-----------------------------------------------
// Add all uncertainties for final plot
//-----------------------------------------------

TH1D *GetAllUncertainties(TH1D *h1, TH1D *h2, TH1D *h3, TH1D *h4){
  TH1D *AllUncertainties = AddInQuad(h1, h2, h3, h4);
  return AllUncertainties;
}

TH1D *GetSystUncertainties(TH1D *h1, TH1D *h2, TH1D *h3){
  TH1D *SystUncert = AddInQuad(h1, h2, h3);
  return SystUncert;
}
