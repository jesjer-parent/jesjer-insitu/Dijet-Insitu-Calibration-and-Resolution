#include "dijetSkim/dijetSkim.h"	
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "CPAnalysisExamples/errorcheck.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranchElement.h"

const char* APP_NAME = "Skim";

int main(int argc, char* argv[]) {

  CHECK(xAOD::Init());
  xAOD::TStore store; 
  //xAOD::TEvent event;
  //xAOD::TEvent event(xAOD::TEvent::kBranchAccess);
  
  xAOD::TEvent::EAuxMode mode = xAOD::TEvent::kBranchAccess; // xAOD::TEvent::kClassAccess;
  xAOD::TEvent event(mode);

  std::string filenames = argv[1];
  std::vector<std::string> infiles;
  std::stringstream ss(filenames);
  std::string s;
  while (std::getline(ss, s, ',')) {
    infiles.push_back(s);
  }

  if (infiles.size() < 1) {
    std::cout << "Failed to find input files" << std::endl;
    abort();
  }

  TFile* f = TFile::Open(infiles[0].c_str(), "READ");
  if (!f->IsOpen()) {
    std::cout << "Failed to open file " << infiles[0].c_str() << std::endl;
    abort();
  }
  event.readFrom(f);
	 
  dijetSkim dijetSkim;
  dijetSkim.Initialize(event);
  
  
  //dijetSkim.fwrite()->Branch(TriggerMenu);

  for (const auto &file : infiles) {
    std::cout << "Opening " << file << std::endl;

    TFile* f = TFile::Open(file.c_str(), "READ");
	
    event.readFrom(f);
    const unsigned int n_entries = event.getEntries();
	   
    std::cout << " Processing file: " << file << std::endl;
    std::cout << " Entries in file: " << n_entries << std::endl;
	
    //for (unsigned int ientry = 0; ientry < 100; ++ientry) {
    for (unsigned int ientry = 0; ientry < n_entries; ++ientry) {
      if (ientry % 100 == 0) std::cout << " Processing event " << ientry << " / " << n_entries << std::endl;
	     
      event.getEntry(ientry);
      dijetSkim.Execute(event);
    }
    TTree *t = (TTree*)f->Get("MetaData");
    TBranchElement *b = (TBranchElement*)t->GetBranch("TriggerMenu");
    //dijetSkim.fwrite()->cd("MetaData");
    //dijetSkim.fwrite()->cd();
    //TFile *fsec = new TFile("test.root","RECREATE");
    //b->Write();
    //t->Write("MetaData",TObject::kOverwrite);
    std::cout<< "written TTree Metadata " << std::endl;
    //t->ShowMembers();
  }

  dijetSkim.Finalize(event);

  return 0;
}
