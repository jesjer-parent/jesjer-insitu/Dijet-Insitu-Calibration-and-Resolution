#ifndef VBFZNUNUSKIM_H
#define VBFZNUNUSKIM_H

#include "PATInterfaces/SystematicRegistry.h"
#include "xAODTriggerCnv/TriggerMenuMetaDataTool.h"
#include <memory>
#include <map>
	
namespace xAOD {
  class TEvent;
}
class TH1F;
class TFile;
class TString;
namespace CP {
  class PileupReweightingTool;
}

class dijetSkim {
   
public:

  dijetSkim();
  ~dijetSkim();

  int Initialize(xAOD::TEvent& event);
  int Execute(xAOD::TEvent& event);
  int Finalize(xAOD::TEvent& event);
  TFile *fwrite(){return fOut;};
	   
private:

  xAOD::TEvent* m_event;
  TFile* fOut;
  TH1F* sumOfWeights;
  float mcEventWeight;
  bool m_isMC;
  xAODMaker::TriggerMenuMetaDataTool *m_triggertool;
  std::unique_ptr<CP::PileupReweightingTool> m_pileupReweightingTool; 

};
	
#endif
