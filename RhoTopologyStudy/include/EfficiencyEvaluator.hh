#ifndef EFFICIENCY_EVALUATOR_HH
#define EFFICIENCY_EVALUATOR_HH

#include "TEnv.h"
#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TGraph.h"
#include "TH2F.h"
#include "TH1F.h"
#include "TStyle.h"
#include "TProfile.h"

#include "TMVA/Factory.h"
#include "TMVA/Tools.h"

#include <vector>
#include <map>

// TODO:
//  2. Evaluate optimum cuts based on maximal separation between bg and signal histograms 

class EfficiencyEvaluator{
private:
  TString m_settingsFilePath;
  TEnv * m_settings;
  TString m_inputFilePath, m_outputFilePath;
  TFile *m_inputFile, *m_outputFile;
  TString m_name;
  TString m_treeName;
  TTree* m_ttree;

  std::map< TString, TH2F * > m_hists2D;
  float m_rhoEM, m_rhoLC, m_mu, m_NPV,m_weight;
  bool m_passNominal;
  void FillHistograms();
  void ReadSettings();
  void DisplaySettings();
  void CreateHistograms(TString name);
  void Draw2D(TString name);
public:
  EfficiencyEvaluator(TString settingsFilePath);
  ~EfficiencyEvaluator();

  void Process();
  void Draw();
  void Save();
};

#endif
