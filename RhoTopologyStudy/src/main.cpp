#include <iostream>

//Root includes
#include "TString.h"
#include "TEnv.h"
#include "TFile.h"
#include "TH1.h"

//project specific includes
#include "EfficiencyEvaluator.hh"
using namespace std;

int main(int argc, char* argv[]){
  //read in the settings file if the user provides one
  TString settingsFile = "share/settings.config";
  if(argc > 1){
    settingsFile = argv[1];
  }

  //Create a new efficiency processor
  EfficiencyEvaluator efficiencyEvaluator(settingsFile);

  //create the curves we're after
  efficiencyEvaluator.Process();

  //draw and save 
  efficiencyEvaluator.Draw();
  efficiencyEvaluator.Save();
}
