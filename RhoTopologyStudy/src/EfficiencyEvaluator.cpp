#include "EfficiencyEvaluator.hh"

#include "TMath.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TH1F.h"

#include <iostream>
using namespace std;

EfficiencyEvaluator::EfficiencyEvaluator(TString settingsFilePath):
  m_settingsFilePath(settingsFilePath),
  m_settings(0){
    cout << "======================================" << endl;
    cout << "=======Rho Topology Evaluator=========" << endl;
    cout << "======================================" << endl;

    ReadSettings();
    DisplaySettings();
}

EfficiencyEvaluator::~EfficiencyEvaluator(){
  if(m_settings!= 0)
    delete m_settings;

  m_inputFile->Close();
  delete m_inputFile;

  m_outputFile->Close();
  delete m_outputFile;

  cout << "======================================" << endl;
}

std::vector<TString> Vectorize(TString str, TString sep=" ") {
  std::vector<TString> result;
   TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) return result;
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr())
    if (os->GetString()[0]!='#')
      result.push_back(os->GetString());
    else
      break;
  return result;
}

vector<float> VectorizeD(TString str) {
  std::vector<float>  result; std::vector<TString>  vecS = Vectorize(str);
  for (uint i=0;i<vecS.size();++i) 
    result.push_back(atof(vecS[i]));
  return result;
}


void printProgress (double percentage)
{
    const int PBWIDTH = 60;
    int val = (int) (percentage * 100);
    int lpad = (int) (percentage * PBWIDTH);
    int rpad = PBWIDTH - lpad;
    printf ("\r%3d%% [%.*s%*s]", val, lpad, "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||", rpad, "");
    fflush (stdout);
}


void EfficiencyEvaluator::CreateHistograms(TString name){
  cout << "Creating histograms with prefix " << name << "..." << endl;
  m_hists2D["rhoEMVsMu"] = new TH2F(name+"_muVsRhoEM", ";<#mu>;#rho_{EM} [GeV];", 30,5,40,30 ,0, 40 );
  m_hists2D["rhoLCVsMu"] = new TH2F(name+"_muVsRhoLC", ";<#mu>;#rho_{LC} [GeV];", 30,5,40,30 ,0, 40);

  m_hists2D["rhoEMVsNPV"] = new TH2F(name+"_NPVVsRhoEM", ";NPV;#rho_{EM} [GeV];", 30,0,40,30 ,0, 40 );
  m_hists2D["rhoLCVsNPV"] = new TH2F(name+"_NPVVsRhoLC", ";NPV;#rho_{LC} [GeV];", 30,0,40,30 ,0, 40);
}

void EfficiencyEvaluator::FillHistograms(){
  m_hists2D["rhoEMVsMu"]->Fill(m_mu,m_rhoEM,m_weight);
  m_hists2D["rhoLCVsMu"]->Fill(m_mu,m_rhoLC,m_weight);
  m_hists2D["rhoEMVsNPV"]->Fill(m_NPV,m_rhoEM,m_weight);
  m_hists2D["rhoLCVsNPV"]->Fill(m_NPV,m_rhoLC,m_weight);
}
void EfficiencyEvaluator::ReadSettings(){
  m_settings = new TEnv(m_settingsFilePath);
  if(!m_settings){
    cout << "ERROR: failed to read settings file: " << m_settingsFilePath << endl;
    exit(-1);
  }
  //read and open the input files
  m_inputFilePath     = m_settings->GetValue("inputFile","");
  m_outputFilePath     = m_settings->GetValue("outputFile","");
  m_name               = m_settings->GetValue("name","");
  m_treeName               = m_settings->GetValue("treeName","");
  //open the input files
  m_inputFile     = new TFile(m_inputFilePath, "READ");
  if(!m_inputFile){
    cout << "ERROR: failed to read input file: " << m_inputFilePath << endl;
    exit(-1);
  }
  m_outputFile     = new TFile(m_outputFilePath, "RECREATE");
  if(!m_outputFile){
    cout << "ERROR: failed to (re)create background file: " << m_outputFilePath << endl;
    exit(-1);
  }
}

void EfficiencyEvaluator::DisplaySettings(){
  cout << "Input file    : " << m_inputFilePath << endl;
  cout << "Output file     : " << m_outputFilePath << endl;
  cout << "Name: " << m_name << endl;
  cout << "Input Tree: " << m_treeName << endl;

  m_ttree = (TTree*)m_inputFile->Get(m_treeName);
  if(!m_ttree){
    cout << "ERROR: Failed to open tree: " << m_treeName << endl;
    exit(-1);
  }
  cout << "Reading " << m_ttree->GetEntries() << " from input treee" << m_treeName << endl;
  CreateHistograms(m_name);
  m_ttree->SetBranchAddress("rhoEM",&m_rhoEM);
  m_ttree->SetBranchAddress("rhoLC",&m_rhoLC);
  m_ttree->SetBranchAddress("avgIntPerXing",&m_mu);
  m_ttree->SetBranchAddress("NPV",&m_NPV);
  m_ttree->SetBranchAddress("weight",&m_weight);
  m_ttree->SetBranchAddress("pass_Nominal",&m_passNominal);


}
void EfficiencyEvaluator::Process(){

  for( unsigned int i = 0 ; i < m_ttree->GetEntries(); i++) {
    if(i%100000==0)printProgress((float)(i)/float(m_ttree->GetEntries()));
    
      m_ttree->GetEntry(i);
    //cout << "filling histogram " << i << endl;
    if(m_passNominal)
     FillHistograms();
  }
}

void EfficiencyEvaluator::Draw2D(TString name){
   TCanvas *c1 = new TCanvas("c1","show profile",600,900);
   c1->Divide(1,2);
   c1->cd(1);
   m_hists2D[name]->Draw("colz");
   c1->cd(2);

   //use a TProfile to convert the 2-d to 1-d problem
   TProfile *prof = m_hists2D[name]->ProfileX();
   gStyle->SetOptFit();
   prof->Fit("pol1");
   prof->Write();

   c1->Print("out/"+m_name+"_"+name+".pdf");
}
void EfficiencyEvaluator::Draw(){
  Draw2D("rhoEMVsMu");
  Draw2D("rhoLCVsMu");
  Draw2D("rhoEMVsNPV");
  Draw2D("rhoLCVsNPV");
}

void EfficiencyEvaluator::Save(){
  m_outputFile->cd();
 for(auto & hist: m_hists2D){
  hist.second->Write();
 }
 m_outputFile->Close();
}
