/********************************
::>> Script for producing eta-intercalibration residual correction and uncertainties for early 2015 data
::>> July 22 2015: Jim Lacey
********************************/

#include "utils/Utils.h"
#include "utils/DrawingUtils.h"
#include "utils/AtlasStyle.C"
#include "utils/JES_Smoother.C"

#include "TLegend.h"
#include <map>

bool _drawSyst          = false;
bool _includeSyst       = false;

double _refRegion       = 0.8;
double _pTSmoothSig     = 0.2;
double _etaSmoothSig    = 0.01;
double _pTSmoothSigSys  = 0.4;
double _etaSmoothSigSys = 0.13;

//VecD ETABINS, PTBINS;

Str _ps;


void Save(Graph2D *g, Str name) { g->SetName(name); g->Write(); }
void DrawSubPad(double FIGURE2_RATIO = 0.36);
void SymmetrizeVsEta_UseMaxErr(TH2D *h, bool useMaxUncert, double ptMax);
Graph2D *GetTheorySys(Graph2D *mc1, Graph2D *mc2, Graph2D *data, Str name);
void RemoveRefUncertainty(TH2D* h);
void DrawMainResultVsEta(Graph *g1,Graph *g2,Graph *g3,Graph* gr1,Graph* gr2,TH1D *calib,TH1D *hsyst,TH1D *hstat,double ptMin,double ptMax,double etaMin,double etaMax, bool drawUncert=true, bool isSystematic=false);
void DrawMainResultVsPt(Graph *g1,Graph *g2,Graph *g3,Graph* gr1,Graph* gr2,TH1D *calib,TH1D *hsyst,TH1D *hstat,double ptMin,double ptMax,double etaMin,double etaMax, bool drawUncert=true, bool isSystematic=false);
void DrawCalibrationResidualsVsEta( TH1D *calib,TH1D *hsyst,TH1D *hstat,TH1D *calib2,TH1D *hsyst2,TH1D *hstat2,double ptMin,double ptMax,double etaMin,double etaMax, double etaMax2);

TH2D* Merge2DHistFromSlices(TH2D* histogramA, TH2D* histogramB, double a, double b, Str histogramName);

VecD GetEtaBins(double maxEta);
double GetMaxPtAkt4(double eta);
double GetMaxPtAkt6(double eta);
double GetMaxEtaAkt4(double pt);
double GetMaxEtaAkt6(double pt);
double GetMaxPt(double eta, Str name);
double GetMaxEta(double pt, Str name);
TH1D *CopyCentral(TH1D *fullCalib, double maxEta, bool removeCtr=false, bool CtrUnity = false);
TH1D *CopyCentral(TH1D *fullCalib, double minPt, double maxPt);
TH1D* DrawCalibrationVsEta2(TFile* fn, Str hname, double avgPt);
TH1D *ProjectVsEta(TH2D *h, double avgPt, double maxPt = -1 );
TH1D *ProjectVsPt(TH2D *h, double eta);
TH2D* CreateErrorEnvelopeHistogram(TH2D* envelopeSys,vector<TH2D*> error_histogramsFromEnvelope);
int main(int argc, char **argv) {

  gErrorIgnoreLevel=2000;

  /*
   *  1.a. Read in from command line
   */
  if(argc < 11){
    cerr << "Please provide all 11 arguments to this monstrosity..." << endl;
    abort();
   }
  Str inDataFileName       = argv[1];
  Str inNominalMCFilename  = argv[2];
  Str inSystMCFileName     = argv[3];
  Str inDataFileName2      = argv[4];
  Str inNominalMCFilename2 = argv[5];
  Str inSystMCFileName2    = argv[6];
  Str calibFileName        = argv[7];
  Str uncertFileName       = argv[8];
  Str calibFileName2       = argv[9];
  Str uncertFileName2      = argv[10];
  Str method = "MM";
  Str jetAlgo             = argv[11];

  printf("\n======================\n");
  printf("  data for      eta < 2.1 :       %s\n\n",inDataFileName.Data());
  printf("  mc for        eta < 2.1:       %s\n\n",inNominalMCFilename.Data());
  printf("  sys mc for    eta < 2.1:       %s\n\n",inSystMCFileName.Data());
  printf("  calib mc for  eta < 2.1:       %s\n\n",calibFileName.Data());
  printf("  uncert mc for eta < 2.1:       %s\n\n",uncertFileName.Data());
  printf("  data for eta      > 2.1:       %s\n\n",inDataFileName2.Data());
  printf("  mc for eta        > 2.1:       %s\n\n",inNominalMCFilename2.Data());
  printf("  sys mc for eta    > 2.1:       %s\n\n",inSystMCFileName2.Data());
  printf("  calib mc for  eta < 2.1:       %s\n\n",calibFileName2.Data());
  printf("  uncert mc for eta < 2.1:       %s\n\n",uncertFileName2.Data());
  printf("  jet algorithm:                 %s\n\n", jetAlgo.Data());


  /*
   *  1.b. Determine the eta and pT binning from histos in the input files
   */

  TFile *f_data_temp = Open(inDataFileName2);
  TH2D *h_pT_temp  = Get2DHisto(f_data_temp,jetAlgo+"_AvgPtAvg_vs_EtaPtAvg_"+"MM");
  VecD  ptBins;
  for( int ipt=0; ipt<=h_pT_temp->GetNbinsY(); ipt++) ptBins.push_back(h_pT_temp->GetYaxis()->GetBinLowEdge(ipt+1) );
  int NptBins=ptBins.size()-1;
  delete h_pT_temp;
  f_data_temp->Close();

  /*
   *  1.d. Init ps file
   */

  SetAtlasStyle();
  _tex = new TLatex();
  _tex->SetNDC();
  _tex->SetTextSize(TEXTSIZE);

  _can = new TCanvas();
  _can->SetMargin(0.12,0.08,0.15,0.05);
  _ps="Feb16_EtaIntercalibration13TeV25ns_ComparisonNewOld_"+method+"_"+jetAlgo+".ps";
  _can->Print(_ps+"[");

  //TODO:
  //EITHER - TCutG in region, make new 2D hists for before and after then merge
  // OR see if can make calibration JUST in 2.1 region!


  /*
   *  1.d. Read in necessary trim the graphs
   */

  TFile *f_data, *f_mc1, *f_mc2;
  f_data   = Open(inDataFileName);
  f_mc1    = Open(inNominalMCFilename);
  f_mc2    = Open(inSystMCFileName);


  map<Str,Graph2D*> calibration_2Dgraph;

  // use numerical inversion to correctmap pTavg to the pT of the probe jet
  bool numInv = true;
  double freezePt = 760.0;
  Graph2D *data_2Dgraph_full    = GetCalibration2DGraph(f_data,jetAlgo,method,numInv);
  Graph2D *nom_mc_2Dgraph_full  = GetCalibration2DGraph(f_mc1,jetAlgo,method,numInv);
  Graph2D *syst_mc_2Dgraph_full = GetCalibration2DGraph(f_mc2,jetAlgo,method,numInv);

  cout << "Trimming first set" << endl;
  calibration_2Dgraph ["data_2Dgraph"]    = TrimGraph( data_2Dgraph_full,   nom_mc_2Dgraph_full, syst_mc_2Dgraph_full, freezePt );
  calibration_2Dgraph ["nom_mc_2Dgraph"]  = TrimGraph( nom_mc_2Dgraph_full, data_2Dgraph_full,   syst_mc_2Dgraph_full, freezePt );

    printf("Number of trimmed measurements for (data,mc1) = (%d,%d)\n",
  	 calibration_2Dgraph ["data_2Dgraph"]->GetN(),
     calibration_2Dgraph ["nom_mc_2Dgraph"]->GetN());

  TFile *f_data_2, *f_mc1_2, *f_mc2_2;
  f_data_2  = Open(inDataFileName2);
  f_mc1_2   = Open(inNominalMCFilename2);
  f_mc2_2   = Open(inSystMCFileName2);

  Graph2D *data_2Dgraph_full2    = GetCalibration2DGraph(f_data_2,jetAlgo,method,numInv);
  Graph2D *nom_mc_2Dgraph_full2  = GetCalibration2DGraph(f_mc1_2,jetAlgo,method,numInv);
  Graph2D *syst_mc_2Dgraph_full2 = GetCalibration2DGraph(f_mc2_2,jetAlgo,method,numInv);

  cout << "Trimming second set" << endl;
  calibration_2Dgraph ["data_2Dgraph2"]    = TrimGraph( data_2Dgraph_full2,   nom_mc_2Dgraph_full2, syst_mc_2Dgraph_full2, freezePt );
  calibration_2Dgraph ["nom_mc_2Dgraph2"]  = TrimGraph( nom_mc_2Dgraph_full2, data_2Dgraph_full2,   syst_mc_2Dgraph_full2, freezePt );
  cout << "done.";

  printf("Number of trimmed measurements for (data,mc1,mc2) = (%d,%d)\n",
  calibration_2Dgraph ["data_2Dgraph2"]->GetN(),
  calibration_2Dgraph ["nom_mc_2Dgraph2"]->GetN());
  map<Str,Graph2D*> calibration_graph_points;
  map<Str,TH2D*> smooth_calibration_histos;
  map<Str,TH2D*> stat_histos;

  // Get the calibration points: c = R_MC / R_data  .... just the ratio since no calibration is derived at this point.
  calibration_graph_points["calib_2Dgraph_nomMC"]  = GetCalibrationPoints(calibration_2Dgraph ["data_2Dgraph"],calibration_2Dgraph ["nom_mc_2Dgraph"]);
  calibration_graph_points["calib_2Dgraph_nomMC2"]  = GetCalibrationPoints(calibration_2Dgraph ["data_2Dgraph2"],calibration_2Dgraph ["nom_mc_2Dgraph2"]);

  double pTMIN = 525.0, pTMAX=freezePt;
  VecI points = GetRvsEtaPoints(calibration_2Dgraph ["data_2Dgraph"],pTMIN,pTMAX);
  double pT_freeze_uncert = GetAvgPt(calibration_graph_points["calib_2Dgraph_nomMC"],points);
  //Get what thingum Staterr_sys

  TFile *f_calibration, *f_uncertantity;
  f_calibration = Open(calibFileName);
  f_uncertantity = Open(uncertFileName);

  TFile *f_calibration2, *f_uncertantity2;
  f_calibration2 = Open(calibFileName2);
  f_uncertantity2 = Open(uncertFileName2);

  //read in calibraiton
  smooth_calibration_histos["calib_nomMC"]  = Get2DHisto(f_calibration, jetAlgo+"_EtaInterCalibration");
  smooth_calibration_histos["calib_nomMC2"] = Get2DHisto(f_calibration2, jetAlgo+"_EtaInterCalibration");


  Str UncertJAlgoStr = jetAlgo;
  Str UncertJAlgoEMopo = "AntiKt4_EMJES";
  UncertJAlgoStr.ReplaceAll("LCTopo","").ReplaceAll("EMTopo","").ReplaceAll("TopoEM","");
  Str uncertHistName = jetAlgo.Contains("LC")? "_LCJES":"_EMJES";
  uncertHistName = UncertJAlgoStr+uncertHistName;

  //Get smoothed stat errors...
  stat_histos["stat"]  = Get2DHisto(f_uncertantity, "EtaIntercalibration_TotalStat_"+UncertJAlgoEMopo);
  stat_histos["stat2"] = Get2DHisto(f_uncertantity2, "EtaIntercalibration_TotalStat_"+uncertHistName);

  TH2D *total_sys2015    = Get2DHisto(f_uncertantity,"EtaIntercalibration_TotalSyst_"+UncertJAlgoEMopo);
  TH2D *total_sys2015_2  = Get2DHisto(f_uncertantity2,"EtaIntercalibration_TotalSyst_"+uncertHistName);
  vector<TH2D*>  statNuisVec;
//  for (int si=0;si < calibration_2Dgraph ["data_2Dgraph2"]->GetN();++si)  statNuisVec.push_back( Get2DHisto(f_calibration2, Form("StatNuisanceParams/Stat%d",si) ) );

  /*
   * 2.a Make merged histograms over the ranges
   */
  cout << "Attempting to merger two 2D hists..." << endl;
  TH2D* mergedSmoothCalibration =  Merge2DHistFromSlices(  smooth_calibration_histos["calib_nomMC"],  smooth_calibration_histos["calib_nomMC2"], -2.2,2.2, "Etantercalibration_Calibration_theClone");
  cout << "Merge complete."<< endl <<"Attempting to save..." << endl;
  TFile *of_cal = new TFile("Feburary6_EtaInterCalibration_"+jetAlgo+"_Calibration.root","RECREATE");
  printf("Saving results to\n  %s\n\n",of_cal->GetName());
  of_cal->cd();
  mergedSmoothCalibration->Write();
  //of_cal->mkdir("StatNuisanceParams");
  //of_cal->cd("StatNuisanceParams");
  //for (int si=0;si<statNuisVec.size();++si) statNuisVec[si]->Write();
  of_cal->Close();
  cout<<"Save complete.";
  /*
   *  2.a Plot vs eta
   */
  for (int ipt=0;ipt<ptBins.size()-1;++ipt) {

    if(ptBins[ipt]>=pT_freeze_uncert) continue;

    VecI points = GetRvsEtaPoints(calibration_2Dgraph["data_2Dgraph"],ptBins[ipt],ptBins[ipt+1]);

    double min=-4.1, max=4.1;
    double avg=GetAvgPt(calibration_graph_points["calib_2Dgraph_nomMC"],points);

    TH1D *smooth_calib_nomMC = GetSmoothVsEta(smooth_calibration_histos["calib_nomMC"],avg,min,max);
    //GetSmoothVsEta(smooth_calibration_histos["calib_nomMC"],avg,min,max);
    TH1D *smooth_sys = GetSmoothVsEta(total_sys2015,avg,min,max);
    TH1D *smooth_stat = GetSmoothVsEta(stat_histos["stat"],avg,min,max);

    TH1D *smooth_calib_nomMC2 = GetSmoothVsEta(smooth_calibration_histos["calib_nomMC2"],avg,min,max);
    TH1D *smooth_sys2         = GetSmoothVsEta(total_sys2015_2,avg,min,max);
    TH1D *smooth_stat2        = GetSmoothVsEta(stat_histos["stat2"],avg,min,max);


    //////////////////////////
    // draw main results
    //////////////////////////
    DrawCalibrationResidualsVsEta(
				  smooth_calib_nomMC,
				  smooth_sys,
				  smooth_stat,

				  smooth_calib_nomMC2,
				  smooth_sys2,
			    smooth_stat2,

				  ptBins[ipt],
				  ptBins[ipt+1],
          0.0,
				  fabs(smooth_calib_nomMC->GetBinContent(smooth_calib_nomMC->FindFirstBinAbove(-50))),
				  fabs(smooth_calib_nomMC2->GetBinContent(smooth_calib_nomMC2->FindFirstBinAbove(-50))) );
  }
	cout << "Drawn the eta dependence" << endl;
  cout << "done."<<endl;


  /*
   *  3. Save results
   */


  _can->Print(_ps+"]");
  gSystem->Exec(Form("ps2pdf %s",_ps.Data()));
  gSystem->Exec(Form("rm %s",_ps.Data()));
  printf("\nProduced\n  %s\n\n",_ps.ReplaceAll(".ps",".pdf").Data());


}

/*
 * CreateErrorEnvelopeHistogram: Sets the envelopeHist to be the envelope of error_histogramsFromEnvelope
 * @error_histogramsFromEnvelope: A list of the errors histograms to create the envelope from
																- the envelope is the largest of these errors for each point
 *
 */
TH2D* CreateErrorEnvelopeHistogram(TH2D* envelopeHist, vector<TH2D*> error_histogramsFromEnvelope){

	int Neta=envelopeHist->GetNbinsY();
	for (int i=1;i<=envelopeHist->GetNbinsX();++i) {
	  double pt=envelopeHist->GetXaxis()->GetBinCenter(i);
	  for (int ei=1;ei<=Neta;++ei) {
			double maxUncert = 0.0;

			for(const auto& h : error_histogramsFromEnvelope){
				if(maxUncert <= h->GetBinContent(i,ei))
					maxUncert =  h->GetBinContent(i,ei);
			}


    	envelopeHist->SetBinContent(i,ei,maxUncert);
//      envelopeHist->SetBinContent(i,Neta-ei+1,maxUncert);
	  }
	}

}

Graph2D *GetTheorySys(Graph2D *mc1, Graph2D *mc2, Graph2D *data, Str name) {


  Graph2D *sys = new Graph2D();
  sys->SetName(name);

  for (int i=0;i<mc1->GetN();++i) {

    double x=mc1->GetX()[i], y=mc1->GetY()[i];
    double ex=mc1->GetEX()[i], ey=mc1->GetEY()[i];

    double R_data = data->GetZ()[i];
    double R_data_err = data->GetEZ()[i];
    double R_mc1 = mc1->GetZ()[i];
    double R_mc1_err = mc1->GetEZ()[i];
    double R_mc2 = mc2->GetZ()[i];
    double R_mc2_err = mc2->GetEZ()[i];

    // error treatment:
    // compute the difference between the two MC generators and normalize by their average
    // this is the relativ systematic difference between the predictions
    double diff = fabs( R_mc1 - R_mc2 ) / 2.0;
    double avg = ( R_mc1 + R_mc2 ) / 2.0;
    double rel_sys = diff/avg;

    // avg is just a normalization and shouldn't be included in the error propagation
    // then we essentially have a normalized RMS :: NRMS = RMS/avg
    double sys_err = 0.5/avg*sqrt( pow(R_mc2_err,2) + pow(R_mc1_err,2) );

    // take the full difference as the error: multiply by factor of 2
    sys->SetPoint(i,x,y,2.0*rel_sys);
    //if(sys->GetZ()[i]==0) sys_err = 0.0;
    sys->SetPointError(i,ex,ey,sys_err);

    if (fabs(y)<_refRegion) {
      sys->SetPoint(i,x,y,0);
    }

  }
  return sys;
}




void SymmetrizeVsEta_UseMaxErr(TH2D *h, bool noUncertDecreaseWithIncereasEta, double ptMax) {
  int Neta=h->GetNbinsY();
  for (int i=1;i<=h->GetNbinsX();++i) {
    double pt=h->GetXaxis()->GetBinCenter(i);
    double maxUncert = 0.0;
    for (int ei=Neta/2+1;ei<=Neta;++ei) {
      double eta=h->GetYaxis()->GetBinCenter(ei);
      //if (pt>ptMax) pt=ptMax;
      double r1=h->Interpolate(pt,eta);
      double r2=h->Interpolate(pt,-eta);
      double w=(TMath::Erf( (eta-_refRegion) /0.05 ) +1 )/2;
      double uncert=r1>r2?r1*w:r2*w;
      if(noUncertDecreaseWithIncereasEta){
				if(maxUncert<uncert) maxUncert = uncert;
				else uncert = maxUncert;
      }
      h->SetBinContent(i,ei,uncert);
      h->SetBinContent(i,Neta-ei+1,uncert);
    }
  }
}

void RemoveRefUncertainty(TH2D* h){
  for(int i=0; i<h->GetNbinsX(); i++){
    for(int j=0; j<h->GetNbinsY(); j++){
      double eta = h->GetYaxis()->GetBinCenter(j);
      double w=(TMath::Erf((fabs(eta)-_refRegion)/0.05)+1)/2;
      h->SetBinContent(i,j,h->GetBinContent(i,j)*w);
    }
  }
}

double SUBFIGURE_MARGIN = 0.06;
void DrawSubPad(double FIGURE2_RATIO) {
  _can->SetBottomMargin(FIGURE2_RATIO);
  TPad *p = new TPad( "p_test", "", 0, 0, 1, 1.0 - SUBFIGURE_MARGIN, 0, 0, 0);
  p->SetFillStyle(0);
  p->SetMargin(0.12,0.04,0.125,1.0 - FIGURE2_RATIO);
  p->Draw();
  p->cd();
}
/*
 * @Merge2DHistFromSlices: Takes histogramA and histogramB and returns the combination of A in range a to b, and B otherwsie
 * It is somewhat assumed that histogramB has a finer binning.
 */
TH2D* Merge2DHistFromSlices(TH2D* histogramA, TH2D* histogramB, double a, double b, Str histogramName){
  if(!histogramB || !histogramA){
    cout << "histogramA or histogramB failed to be read properly...." << endl;
    abort();
  }

  TH2D* finalHistogram = (TH2D*)histogramB->Clone(histogramName);
  //loop over both directions
  cout << "merging histograms (" << histogramA->GetNbinsX() << "," << histogramA->GetNbinsY()
       <<") and(" << histogramB->GetNbinsX() << "," << histogramB->GetNbinsY() << ")"<< endl;

  //pT is the x direction.
  for(int  i = 0; i < histogramB->GetNbinsX(); i++){
    if(histogramA->GetYaxis()->GetBinLowEdge(i) > 525){
      a = -1.7;
      b = 1.7;
    }
    //cout << "pT Bin" << i << " " << histogramB->GetYaxis()->GetBinLowEdge(i) << endl;
    //eta is the y direction
    for(int j = 0; j < histogramB->GetNbinsY(); j++){
      double eta_edge=histogramA->GetYaxis()->GetBinLowEdge(j);
      //cout << "eta_edge = " << eta_edge << "\t";
      if(eta_edge > a &&  eta_edge < b){
        //cout << "Setting bin content.." << endl;
        double binContentA = histogramA->GetBinContent(i,j);
        finalHistogram->SetBinContent(i,j, binContentA);
      }
    }
  }

  return finalHistogram;
}

double Max(double a, double b){
  return (a <= b? a:b);
}
void DrawCalibrationResidualsVsEta( TH1D *calib,TH1D *hsyst,TH1D *hstat,TH1D *calib2,TH1D *hsyst2,TH1D *hstat2,double ptMin,double ptMax,double etaMin,double etaMax, double etaMax2){
  if(ptMin >= 25) etaMax = 4.1;
  if(ptMin >= 85) etaMax = 3.6;
  if(ptMin >= 145) etaMax = 3.2;
  if(ptMin >= 270) etaMax = 2.8;
  if(ptMin >= 330) etaMax = 2.4;
  if(ptMin >= 400) etaMax = 2.1;
  if(ptMin >= 525) etaMax = 1.7;

  if(ptMin >= 25) etaMax2 = 4.1;
  if(ptMin >= 85) etaMax2 = 3.6;
  if(ptMin >= 270) etaMax2 = 3.2;
  if(ptMin >= 330) etaMax2 = 3.0;
  if(ptMin >= 525) etaMax2 = 2.8;
  if(ptMin >= 760) etaMax2 = 2.1;


  _can->Clear();
  _can->SetMargin(0.12,0.04,0.15,0.00);
  _can->SetLogx(0);

  static TH1F *mall = new TH1F("mallp","",1,-4.5,4.5);
  FormatHisto(mall,1);
  mall->SetMaximum(1.32);
  mall->SetMinimum(0.86);
  mall->SetXTitle("#font[52]{#eta}_{det}");
  mall->SetYTitle("MC_{Relative response} / Data_{Relative response}");
  mall->Draw();

  DrawATLASLabel();
  PrintEtaBinInfo(ptMin,ptMax);
  PrintIntegratedLumi();
  DrawTextR("Data 2015, #sqrt{#font[52]{s}} = 13 TeV",kBlack,0.78,0.93);
  TH1D* trimmedCalib     = CopyCentral(calib, etaMax, false, true);
  TH1D* trimmedCalibStat = CopyCentral(hstat, etaMax, true);
  TH1D* trimmedCalibSyst = CopyCentral(hsyst, etaMax);

  TH1D *h_calib_stat  = AddError( trimmedCalib, trimmedCalibStat );
  TH1D *h_calib_full  = AddInQuad( trimmedCalibSyst, trimmedCalibStat );
  TH1D *h_calib_total = AddError( trimmedCalib, h_calib_full);

  h_calib_total->SetMarkerSize(0);
  h_calib_total->SetFillColor(kGreen+1);

  h_calib_stat->SetMarkerSize(0);
  h_calib_stat->SetFillColor(kBlue+1);

  trimmedCalib->SetLineColor(kViolet+3);
  trimmedCalib->SetLineStyle(1);
  trimmedCalib->SetLineWidth(4);

  TH1D* trimmedCalib2     = CopyCentral(calib2, etaMax2, false, true);
  TH1D* trimmedCalibStat2 = CopyCentral(hstat2, etaMax2, true);
  TH1D* trimmedCalibSyst2 = CopyCentral(hsyst2, etaMax2);

  TH1D *h_calib_stat2  = AddError( trimmedCalib2, trimmedCalibStat2 );
  TH1D *h_calib_full2  = AddInQuad( trimmedCalibSyst2, trimmedCalibStat2 );
  TH1D *h_calib_total2 = AddError( trimmedCalib2, h_calib_full2);

  h_calib_total2->SetMarkerSize(0);
  h_calib_total2->SetFillColor(kGreen+1);

  h_calib_stat2->SetMarkerSize(0);
  h_calib_stat2->SetFillColor(kBlue+1);

  trimmedCalib2->SetLineColor(kViolet+3);
  trimmedCalib2->SetLineStyle(1);
  trimmedCalib2->SetLineWidth(4);


  TLegend leg(0.13,0.59,0.58,0.80);
  leg.SetFillStyle(0);
  leg.SetBorderSize(0);
  leg.AddEntry(trimmedCalib,"#eta-intercalibration","l");
  leg.AddEntry(h_calib_total,"Total uncertainty","f");
  leg.AddEntry(h_calib_stat,"Statistical component","f");
  leg.Draw();

  h_calib_total->GetXaxis()->SetRangeUser(-Max(2.2,etaMax),Max(2.2,etaMax));
  h_calib_stat->GetXaxis()->SetRangeUser(-Max(2.2,etaMax),Max(2.2,etaMax));
  trimmedCalib->GetXaxis()->SetRangeUser(-Max(2.2,etaMax),Max(2.2,etaMax));

  h_calib_total->Draw("sameE5");
  h_calib_stat->Draw("sameE5");
  trimmedCalib->Draw("samel");

  static int cloneIterator = 0;
  TH1D* h_calib_total2C1 = (TH1D*)h_calib_total2->Clone(Form("hnew+%d",cloneIterator));cloneIterator++;
  TH1D* h_calib_stat2C1 = (TH1D*)h_calib_stat2->Clone(Form("hnew+%d",cloneIterator));cloneIterator++;
  TH1D* trimmedCalib2C1 = (TH1D*)trimmedCalib2->Clone(Form("hnew+%d",cloneIterator));cloneIterator++;

  TH1D* h_calib_total2C2 = (TH1D*)h_calib_total2->Clone(Form("hnew+%d",cloneIterator));cloneIterator++;
  TH1D* h_calib_stat2C2 = (TH1D*)h_calib_stat2->Clone(Form("hnew+%d",cloneIterator));cloneIterator++;
  TH1D* trimmedCalib2C2 = (TH1D*)trimmedCalib2->Clone(Form("hnew+%d",cloneIterator));cloneIterator++;

  h_calib_total2C1->GetXaxis()->SetRangeUser(-etaMax2,-Max(2.2,etaMax)+0.1);
  h_calib_stat2C1->GetXaxis()->SetRangeUser(-etaMax2,-Max(2.2,etaMax)+0.1);
  trimmedCalib2C1->GetXaxis()->SetRangeUser(-etaMax2,-Max(2.2,etaMax)+0.1);

  h_calib_total2C2->GetXaxis()->SetRangeUser(Max(2.2,etaMax)-0.1,etaMax2);
  h_calib_stat2C2->GetXaxis()->SetRangeUser(Max(2.2,etaMax)-0.1,etaMax2);
  trimmedCalib2C2->GetXaxis()->SetRangeUser(Max(2.2,etaMax)-0.1,etaMax2);

  h_calib_total2C1->Draw("sameE5");
  h_calib_stat2C1->Draw("sameE5");
  trimmedCalib2C1->Draw("samel");

  h_calib_total2C2->Draw("sameE5");
  h_calib_stat2C2->Draw("sameE5");
  trimmedCalib2C2->Draw("samel");

  DrawGuideLines(-4.5,4.5);

  mall->Draw("sameaxis");

  _can->Print(_ps);
  _can->Clear();

}

VecD GetEtaBins(double maxEta) {
  double etabins[] = {-4.5, -4.4, -4.3, -4.2, -4.1, -4.0, -3.9, -3.8, -3.7, -3.6, -3.5, -3.4, -3.3, -3.2, -3.1, -3.0, -2.9, -2.8, -2.7, -2.6, -2.5, -2.4, -2.3, -2.2, -2.1, -2.0, -1.9, -1.8, -1.7, -1.6, -1.5, -1.4, -1.3, -1.2, -1.1, -1.0, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4.0, 4.1, 4.2, 4.3, 4.4, 4.5};
  VecD aeBins;
  for ( int i=0; i<91; ++i ) if ( etabins[i] >= -maxEta && etabins[i] <= maxEta ) add(aeBins,etabins[i]);
  VecD bins;
  for (uint i=0;i<aeBins.size();++i) add(bins,aeBins[i]);
  return bins;
}



TH1D *CopyCentral(TH1D *fullCalib, double maxEta, bool removeCtr, bool CtrUnity) {
  static int index = 0;
  maxEta+=0.2;
  if(maxEta>3.6) maxEta=4.1;
  VecD etaBins = GetEtaBins(maxEta);
  TH1D *calib = new TH1D(Str(fullCalib->GetName())+Form("_copy%d",index++),"",etaBins.size()-1,&etaBins[0]);
  for (int i=1;i<=calib->GetNbinsX();++i) {
    if(calib->GetXaxis()->GetBinCenter(i)<0.0){
      if(calib->GetXaxis()->GetBinLowEdge(i) < -maxEta) continue;
    }else{
      if(calib->GetXaxis()->GetBinLowEdge(i+1) > maxEta) continue;
    }
    calib->SetBinContent(i,fullCalib->Interpolate(calib->GetBinCenter(i)));
    if(removeCtr){
      double eta_edge=calib->GetXaxis()->GetBinLowEdge(i);
      if(eta_edge<=0 && eta_edge >= -0.8 ) calib->SetBinContent(i,0.0);
      if(eta_edge>=0 && eta_edge <= 0.7 )  calib->SetBinContent(i,0.0);
    }
    if(CtrUnity){
      double eta_edge=calib->GetXaxis()->GetBinLowEdge(i);
      if(eta_edge<=0 && eta_edge >= -0.8 ) calib->SetBinContent(i,1.0);
      if(eta_edge>=0 && eta_edge <= 0.7 )  calib->SetBinContent(i,1.0);
    }

  }
  return calib;
}

TH1D *CopyCentral(TH1D *fullCalib, double minPt, double maxPt) {
  static int index = 0;
  int low = fullCalib->FindBin(minPt), high = fullCalib->FindBin(maxPt);
  VecD bins;
  int Nbins=high-low+1;
  double min=fullCalib->GetBinLowEdge(low), max=fullCalib->GetBinLowEdge(high+1);
  for (int i=0;i<=Nbins;++i) bins.push_back(exp(log(min)+(log(max)-log(min))*i/Nbins));
  TH1D *calib = new TH1D(Str(fullCalib->GetName())+Form("_copy%d",index++),"",Nbins,&bins[0]);
  for (int i=1;i<=calib->GetNbinsX();++i)
    calib->SetBinContent(i,fullCalib->Interpolate(calib->GetBinCenter(i)));
  return calib;
}



double GetMaxPtAkt4(double eta) {
  if (fabs(eta)<=1.2) return 1500;
  if (fabs(eta)<=1.8) return 1200;
  if (fabs(eta)<=2.1) return 760;
  if (fabs(eta)<=2.4) return 525;
  if (fabs(eta)<=2.8) return 330;
  if (fabs(eta)<=3.2) return 180;
  if (fabs(eta)<=3.6) return 145;
  return 85;
}


double GetMaxPtAkt6(double eta) {
  if (fabs(eta)<=1.2) return 1500;
  if (fabs(eta)<=1.8) return 1200;
  if (fabs(eta)<=2.1) return 760;
  if (fabs(eta)<=2.4) return 480;
  if (fabs(eta)<=2.8) return 380;
  if (fabs(eta)<=3.2) return 290;
  if (fabs(eta)<=3.6) return 180;
  return 100;
}


double GetMaxEtaAkt4(double pt) {
  if (pt<=85) return 4.5;
  if (pt<=145) return 3.6;
  if (pt<=270) return 3.2;
  if (pt<=330) return 2.8;
  if (pt<=525) return 2.4;
  if (pt<=760) return 2.1;
  if (pt<=1200) return 1.8;
  return 1.2;
}

double GetMaxEtaAkt6(double pt) {
  if (pt<=100) return 4.5;
  if (pt<=135) return 3.6;
  //if (pt<=180) return 3.2;
  if (pt<=290) return 3.2;
  if (pt<=380) return 2.8;
  if (pt<=480) return 2.1;
  if (pt<=760) return 2.1;
  if (pt<=1200) return 1.8;
  return 1.2;
}

double GetMaxPt(double eta, Str name) {
  if (name.Contains("Kt4")) return GetMaxPtAkt4(eta);
  return GetMaxPtAkt6(eta);
}

double GetMaxEta(double pt, Str name) {
  if (name.Contains("Kt4")) return GetMaxEtaAkt4(pt);
  return GetMaxEtaAkt6(pt);
}



///////////////////////////////

TH1D *ProjectVsEta(TH2D *h, double avgPt, double maxPt ) {
  int ptBin=h->GetXaxis()->FindBin(avgPt);
  if (maxPt>0 && avgPt>maxPt) ptBin=h->GetXaxis()->FindBin(maxPt);
  TH1D *p = h->ProjectionY(Form("%s_%d",h->GetName(),ptBin),ptBin,ptBin);
  return p;
}


TH1D *ProjectVsPt(TH2D *h, double eta) {
  int etaBin=h->GetYaxis()->FindBin(eta);
  TH1D *p = h->ProjectionX(Form("%s_pt%d",h->GetName(),etaBin),etaBin,etaBin);
  p->SetLineColor(kViolet+3); p->SetLineWidth(3);
  return p;
}

///////////////////////////////
