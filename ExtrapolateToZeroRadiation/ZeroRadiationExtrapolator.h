#ifndef ZERORADIATIONEXTRAPOLATOR_H
#define ZERORADIATIONEXTRAPOLATOR_H

#include "utils/Utils.h"
#include "utils/AtlasStyle.h"

#include <utility>
#include <vector>
#include "TEnv.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TString.h"
#include "TGraphErrors.h"
#include "TLine.h"
#include "TLatex.h"
#include "TRandom3.h"
#include "TF1.h"
#include "TRandom3.h"
#include "TLorentzVector.h"
#include "TSystem.h"
#include "TFile.h"

extern float GeV;

/// A tool to take an opened input fill and produce a root file with all the necessary 3D histograms to run MinimizeMatrixMethod
class ZeroRadiationExtrapolator{
public:
  ZeroRadiationExtrapolator( TString settingsFilePath );
  ~ZeroRadiationExtrapolator();

  void PerformExtrapolation();

  void WriteOutput();
  void PrintOutput();
private:
  TEnv* m_settings;
  TString m_configFileName;

  TString m_jetAlgorithm;
  TString m_thirdJetVariable;
  std::vector<TString> m_inputFilePaths;
  std::vector<TFile*>  m_inputFiles;
  std::vector<double>  m_ptBins;
  std::vector<double>  m_thirdJetVariableBins;
  std::map<TString, std::vector<double>> m_responsePointForGivenEta;

  ///////////////////////////////////
  //Variables for the extrapolation//
  ///////////////////////////////////


  //Collection of R(eta) graphs AFTER the extrapolation.
  std::map<TString,TH1F*> m_responseVsEtaAtZeroRadiation;

  //Collection of R(THIRD JET VARIABLE) graphs
  std::map<TString, TGraph*> m_responseVsThirdJetVariable;
  std::map<TString, TF1*> m_responseVsThirdJetVariableFit;
  std::vector<double> m_thirdJetVariablePoints;
  

  void InitialiseSettings();
  void InitialiseFiles();
  void GenerateResponseAtZeroRadiation(float ptMin, float ptMax);
  void DisplayWelcomeMessage();
  void DisplaySettings();
  void CreateResponseVsThirdJetVarCurves(map<TString, TH1F*> responseVsEtaCurves, float ptMin, float ptMax);
  void FitResponseVsThirdJetVarCurves(float ptMin, float ptMax);
  map<TString, TH1F*>  GetResponseCurves(float ptMin, float ptMax);
};
#endif
