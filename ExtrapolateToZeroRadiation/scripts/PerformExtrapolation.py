import os
from subprocess import call,Popen
import datetime

HistogramGenerator = "/afs/hep.man.ac.uk/u/jrawling/EtaIntercalWorkingBranch/GenerateHistograms/histogramGenerator.exe"
HistogramGeneratorSettings = "/afs/hep.man.ac.uk/u/jrawling/EtaIntercalWorkingBranch/GenerateHistograms/utils/settings.config"

inputNTuple="/pc2013-data5/jrawling/data2016/data16_13TeV_periodAtoI_runsNotFinished.root"
HistogramOutPath = "/pc2013-data5/jrawling/ZeroRadExtrapolation/pt3/"


HistogramOutPath = "/pc2013-data5/jrawling/ZeroRadExtrapolation/phiStar/"
HistogramOutPath = "/pc2013-data5/jrawling/ZeroRadExtrapolation/pt3OverPtAvg/"
HistogramOutPath = "/pc2013-data5/jrawling/ZeroRadExtrapolation/Dphijj/"

###Don't forget to check the settings files for the binnings of this variable in GenerateHistograms/utils/settings.config!!

thirdJetVariable="pt3"
thirdJetVariableBins="15 25 35 55 80 300"
thirdJetVariable="phiStar"
thirdJetVariableBins="0 0.001 0.01 0.1 0.5 1.0"
thirdJetVariable="pt3OverpTAvg"
thirdJetVariableBins="0 0.15 0.25 0.35 0.45 0.6"# 0.8 1.0"
thirdJetVariable="Dphijj"
thirdJetVariableBins="0 0.15 0.35 0.5 3.14"

thirdJetVariableBinList = [float(k) for k in thirdJetVariableBins.split(' ')]
nBins=len(thirdJetVariableBinList) - 1

Minimizer = "/afs/hep.man.ac.uk/u/jrawling/EtaIntercalWorkingBranch/MinimiseResponseMatrix/matrix_minimization.exe"
SettingsPath = "/afs/hep.man.ac.uk/u/jrawling/EtaIntercalWorkingBranch/MinimiseResponseMatrix/utils/Dijet_xAOD_forExtrapolation.config"

triggerLumiSettings = "25nsFinal"
jetAlgo = "AntiKt4EMTopo"
etaBinning = "Eta2016v12"
eventGenerator=""
##Set to 1 and it'll only run over the first 10000 events
testRun=0
##pc2012
MinimizationOuptutPath =HistogramOutPath + "MMOutput/"
inputPath = HistogramOutPath

current_time = datetime.datetime.now().time()
print current_time.isoformat()

###### Set the number to be submitted at a time and how many to submit per batch ####
'''
ps = {}
for j in range(0,nBins):
    onTheFlySettings="thirdJetVariable="+thirdJetVariable+",thirdJetBin="+`j`+","+thirdJetVariable+".Bins="+thirdJetVariableBins+",doThirdJetExtrapolation=1,doBootStrap=0,doNominal=0,doSystematics=0,doDphiShiftDown=0,doDphiShiftUp=0,doJet3PtFracShiftUp=0,doJet3PtFracShiftDown=0,doJVTCutTight=0,testRun="+`testRun`
    logFile=HistogramOutPath+"generation_"+thirdJetVariable+"_Bin"+`j`+".log"
    print "Logfile: " + logFile
    print "nice "+" -n "+" 10 "+HistogramGenerator+ " " +HistogramGeneratorSettings+ " " +inputNTuple+ " " +HistogramOutPath+ " " +onTheFlySettings
    args = ["nice","-n","10",HistogramGenerator,HistogramGeneratorSettings,inputNTuple,HistogramOutPath,onTheFlySettings]

    log_file = open(logFile,"wb")
    p = Popen(args,stdout=log_file,stderr=log_file)
    ps[p.pid] = p
while ps:
    pid, status = os.wait()
    if pid in ps:
        del ps[pid]
        print "Waiting for %d processes..." % len(ps)
print "done."
'''
current_time = datetime.datetime.now().time()
print current_time.isoformat()

ps = {}
for j in range(3,nBins):
    inputFile = inputPath + "Data_3DHistograms_"+thirdJetVariable+"_Bin"+`j`+".root"
    fullPrefix = thirdJetVariable +"_"+ "{0:.3f}".format(thirdJetVariableBinList[j]) + "To"+ "{0:.3f}".format(thirdJetVariableBinList[j+1])
    logFile=MinimizationOuptutPath +"minimization"+ "{0:.3f}".format(thirdJetVariableBinList[j]) + "To"+ "{0:.3f}".format(thirdJetVariableBinList[j+1])+".log"
    print "Logfile: " + logFile

    outputFile = MinimizationOuptutPath + "data16_13TeV_" + fullPrefix + "_" + etaBinning+ "_" + triggerLumiSettings + "_"+jetAlgo + ".root"

    print "nice "+" -n "+" 10 "+Minimizer + " "+  SettingsPath + " " + jetAlgo  + " "+ triggerLumiSettings  + " "+ etaBinning  + " "+ inputFile  + " "+ outputFile  + " "+fullPrefix;
    if eventGenerator != "":
        args = ["nice","-n","10",Minimizer, SettingsPath,jetAlgo, triggerLumiSettings, etaBinning, inputFile,outputFile, fullPrefix, eventGenerator]
    else:
        args = ["nice","-n","10",Minimizer, SettingsPath,jetAlgo, triggerLumiSettings, etaBinning, inputFile,outputFile, fullPrefix]
    log_file = open(logFile,"wb")
    p = Popen(args,stdout=log_file,stderr=log_file)
    ps[p.pid] = p

while ps:
    pid, status = os.wait()
    if pid in ps:
        del ps[pid]
        print "Waiting for %d processes..." % len(ps)


current_time = datetime.datetime.now().time()
print current_time.isoformat()

print "done"
