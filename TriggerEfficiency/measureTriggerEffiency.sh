#!/bin/sh

inFile="/hepgpu1-data3/jrawling/data16_13TeV/data16_13TeV_25fb.root"
configFile="utils/settings.config"
outPath="~/2016EtaIntercal/FinalCalibration/EMTopo/TriggerEfficiency/"
exec=triggerEfficiency.exe
./$exec ${configFile} ${inFile} ${outPath}
#&> ${outPath}/generation.log &
