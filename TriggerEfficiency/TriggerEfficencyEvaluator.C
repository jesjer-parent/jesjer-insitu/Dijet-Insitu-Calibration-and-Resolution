#include "TriggerEfficiencyEvaluator.h"
#include <algorithm>
#include <iomanip>
#include <math.h>
#include <cstdlib>
#include <TRandom3.h>


TriggerEfficiencyEvaluator::TriggerEfficiencyEvaluator(std::string configFileName, std::string inputFileName, std::string outputFilePath):
  m_outputFilePath(outputFilePath),
  m_configFileName(configFileName),
  m_inputFileName(inputFileName)
{
  m_outputFilePath += "/";
  //Ignore some warnings from ROOT.
	gErrorIgnoreLevel=2000;
  ReadSettings();
  DisplayWelcomeMessage();
  InitialiseHistograms();
}

TriggerEfficiencyEvaluator::~TriggerEfficiencyEvaluator(){
  for(auto hist : m_hists1D)
    delete hist.second;

  for(unsigned int i = 0; i < m_fullTriggerCombination.size()-1;i++){
    delete m_efficiencyPlots[m_fullTriggerCombination[i]];
  }
}

void TriggerEfficiencyEvaluator::DisplayWelcomeMessage(){
  std::cout << "========================================================" << std::endl;
  std::cout << "================Trigger efficiency tool=================" << std::endl;
  std::cout << "========================================================" << std::endl;
  std::cout << "Input file: \t" << m_inputFileName << std::endl;
  std::cout << "Output folder: \t" << m_outputFilePath << std::endl;
  std::cout << "Settings file: \t" << m_configFileName << std::endl;
  std::cout << "\t Jet Collections: " <<  std::endl;
  for(auto jetAlog : m_jetCollections)
    std::cout << "\t\t"<< jetAlog << std::endl;


  std::cout << "\t Combinations: " <<  std::endl;
  for(unsigned int i = 0; i < m_fullTriggerCombination.size(); i++){
    std::cout << "\t\t " << m_fullTriggerCombination[i] << " | " << m_fullTriggerCombination[m_referenceTriggerIndex[i]] << std::endl;
  }
  std::cout << "\t Graphs to be drawn: " <<  std::endl;
  for( auto graphList : m_graphsToDraw){

    for( int i = 0; i < graphList.size();i++)
        std::cout<<(i != 0 ? " and " : "\t\t") << m_fullTriggerCombination[graphList[i]];
    std::cout << std::endl;
  }
  std::cout << "========================================================" << std::endl;
}

void TriggerEfficiencyEvaluator::InitBranches(){

  m_dijetInsituTree->SetBranchAddress("passTrigger",&m_passTrigger);
  m_dijetInsituTree->SetBranchAddress("Asym_MM",&m_asymMM);
  m_dijetInsituTree->SetBranchAddress("Asym_SM",&m_asymSM);
  m_dijetInsituTree->SetBranchAddress("weight",&m_weight);
  m_dijetInsituTree->SetBranchAddress("isMC",&m_isMC);
  m_dijetInsituTree->SetBranchAddress("JX",&m_JX);
  m_dijetInsituTree->SetBranchAddress("pTavg",&m_pTavg);
  m_dijetInsituTree->SetBranchAddress("left_eta",&m_left_deta);
  m_dijetInsituTree->SetBranchAddress("right_eta",&m_right_deta);
  m_dijetInsituTree->SetBranchAddress("probe_eta",&m_probe_eta);
  m_dijetInsituTree->SetBranchAddress("ref_eta",&m_ref_eta);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j15",&m_pass_HLT_j15);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j15_320eta490",&m_pass_HLT_j15_320eta490);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j25",&m_pass_HLT_j25);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j25_320eta490",&m_pass_HLT_j25_320eta490);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j35",&m_pass_HLT_j35);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j35_320eta490",&m_pass_HLT_j35_320eta490);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j45",&m_pass_HLT_j45);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j45_320eta490",&m_pass_HLT_j45_320eta490);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j60",&m_pass_HLT_j60);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j60_320eta490",&m_pass_HLT_j60_320eta490);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j110",&m_pass_HLT_j110);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j110_320eta490",&m_pass_HLT_j110_320eta490);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j175",&m_pass_HLT_j175);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j175_320eta490",&m_pass_HLT_j175_320eta490);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j260",&m_pass_HLT_j260);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j260_320eta490",&m_pass_HLT_j260_320eta490);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j360",&m_pass_HLT_j360);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j360_320eta490",&m_pass_HLT_j360_320eta490);
  m_dijetInsituTree->SetBranchAddress("trigBitsCtrl",&m_trigBitsCtrl);
  m_dijetInsituTree->SetBranchAddress("trigBitsFwd",&m_trigBitsFwd);

  m_dijetInsituTree->SetBranchAddress("j3_eta",&m_j3_eta);
  m_dijetInsituTree->SetBranchAddress("j3_phi",&m_j3_phi);
  m_dijetInsituTree->SetBranchAddress("j3_pT",&m_j3_pT);

  m_dijetInsituTree->SetBranchAddress("j1_phi",&m_j1_phi);
  m_dijetInsituTree->SetBranchAddress("j2_phi",&m_j2_phi);

  m_dijetInsituTree->SetBranchAddress("j1_pT",&m_j1_pT);
  m_dijetInsituTree->SetBranchAddress("j2_pT",&m_j2_pT);
  m_dijetInsituTree->SetBranchAddress("j1_E",&m_j1_E);
  m_dijetInsituTree->SetBranchAddress("j2_E",&m_j2_E);
  m_dijetInsituTree->SetBranchAddress("j1_eta",&m_j1_eta);
  m_dijetInsituTree->SetBranchAddress("j2_eta",&m_j2_eta);
}
void TriggerEfficiencyEvaluator::InitialiseHistograms(){
  int counter =0;
  for(auto combination : m_fullTriggerCombination){

    float binWidth = 1;
    int nBins = int((m_pTMax[counter] - m_pTMin[counter])/binWidth);
    float etaBinWidth=0.3;
    int nEtaBins = int((4.9f*2.0f)/(etaBinWidth));


    m_hists1D[combination] = new TH1F(combination,";p_{T}^{Avg};N_{Events}",nBins,m_pTMin[counter],m_pTMax[counter]);
    //generate a new histogram for each binning
    for(int i = 0; i < m_pTMin.size(); i++){
      TString histName = combination+"_triggered_binningScheme" + Form("%d",i);
      nBins = int((m_pTMax[i] - m_pTMin[i])/binWidth);

      m_hists1D[histName] = new TH1F(histName,";p_{T}^{Avg};N_{Events}",nBins,m_pTMin[i],m_pTMax[i]);
    }

    m_hists1D[combination+"_eta"] = new TH1F(combination+"_eta",";#eta;N_{Events}",nEtaBins,-4.9,4.9);
    m_hists1D[combination+"_Triggered_eta"] = new TH1F(combination+"_Triggered_eta",";#eta;N_{Events}",nEtaBins,-4.9,4.9);
    m_efficiencyPlots[combination] = new TGraphAsymmErrors(nBins);
    m_efficiencyPlots[combination+"_eta"] = new TGraphAsymmErrors(nEtaBins);
    counter++;
  }
}
bool TriggerEfficiencyEvaluator::PassJVTCut(){

  //check eta and pT of
  bool j1Passed = true;
  bool j2Passed = true;
  if(m_j1_pT < 50 && fabs(m_j1_eta) < 2.4 ){
    if(m_j1_JVT < m_jvtCut)
      j1Passed = false;
  }
  if(m_j2_pT < 50 && fabs(m_j2_eta) < 2.4 ){
    if(m_j2_JVT < m_jvtCut)
      j2Passed = false;
  }

  return (j1Passed && j2Passed);
}
bool TriggerEfficiencyEvaluator::PassDeltaPhiCut(){
  return  (fabs(m_j1_phi - m_j2_phi) >= m_dphijjCut );
}
bool TriggerEfficiencyEvaluator::PassPt3Cut(){
  if(m_j3_pT == -99)return true;
  float ptAvg = (m_j1_pT+m_j2_pT)/2.0;
  return (m_j3_pT < 0.4*ptAvg);
}
bool TriggerEfficiencyEvaluator::PassCuts(){
  //pass nominal cuts
  if( PassJVTCut() )
  if( PassDeltaPhiCut() )
  if( PassPt3Cut() )
    return true;
}

void TriggerEfficiencyEvaluator::ReadSettings(){
  m_settings =OpenSettingsFile(m_configFileName);
  if(!m_settings){
    std::cerr << "Failed to open settings file: "<< m_configFileName << std::endl;
    abort();
  }

	m_jetCollections = vectorise(m_settings->GetValue("JetCollections",""));
  m_fullTriggerCombination =  vectorise(m_settings->GetValue("triggerCombination",""));
  m_referenceTriggerIndex = vectoriseI(m_settings->GetValue("referenceTriggers",""));
  m_pTMin = vectoriseD(m_settings->GetValue("pTMin",""));
  m_pTMax = vectoriseD(m_settings->GetValue("pTMax",""));
  m_jvtCut = m_settings->GetValue("JVTCut", 0.62);
  m_dphijjCut = m_settings->GetValue("DPhiCut", 2.5);
  m_etaPlotsMinPTAvg = vectoriseD(m_settings->GetValue("etaDistributionMinPt",""));
  for(TString combination : m_fullTriggerCombination){
    combination.ReplaceAll("_OR_"," ");
    vector<TString> combiantionList = vectorise(combination);
    m_triggerCombinations.push_back(combiantionList);
  }
  vector<TString> graphsToDraw = vectorise(m_settings->GetValue("GraphsToDraw","")," ");
  for(auto graphList : graphsToDraw){
    m_graphsToDraw.push_back(vectoriseI(graphList.ReplaceAll("_", " ")," "));
  }

}

void TriggerEfficiencyEvaluator::InitialiseInputFile(){
  //verify the input file exists
  m_inputFile = TFile::Open(TString(m_inputFileName));
  if (m_inputFile==NULL) error("Problem opening "+ m_inputFileName);
}
float GetTriggerEt(TString trigger){
  TString triggerETString = trigger;
  triggerETString = triggerETString.ReplaceAll("HLT_j", "");
  triggerETString = triggerETString.ReplaceAll("_320eta490","");
  float triggerET = triggerETString.Atof();
  return triggerET;
}

float TriggerEfficiencyEvaluator::GetTurnOnPT(TGraphAsymmErrors* efficiencyCurve){
  float turnOnPt=-1;
  float turnOnPtFromMin=-1;
  //loop over all points
  for(int i = efficiencyCurve->GetN()-1; i >= 0; i--){
    if( efficiencyCurve->GetY()[i] < 0.99 && turnOnPt == -1)
      turnOnPt =efficiencyCurve->GetX()[i+1];
  }
  for(int i = 0; i <efficiencyCurve->GetN(); i++){
      cout << "("<<efficiencyCurve->GetX()[i]<<","<<efficiencyCurve->GetY()[i]<<")";
      if( turnOnPtFromMin == -1 && efficiencyCurve->GetY()[i] > 0.99)
        turnOnPtFromMin= efficiencyCurve->GetX()[i];
  }
  cout << "turnOnPt = " << turnOnPt << " and from Min up = " << turnOnPtFromMin << endl;
  return turnOnPtFromMin;
}
///NOTE: Since we're making a LOT of histograms, all with a lot of bins the decision was made to
///      create them one at a time - this saves on RAM but at the cost of iterating over the file multiple times.
void TriggerEfficiencyEvaluator::Evaluate(){

  InitialiseInputFile();

	for (const auto& jetAlgo : m_jetCollections){
    m_inputFile->cd();

    m_dijetInsituTree = (TTree*) m_inputFile->Get(jetAlgo+"_dijet_insitu");
		if (  m_dijetInsituTree==0){
      cerr << "WARNING: Could not find dijet insitu tree: " << jetAlgo+"_dijet_insitu" << std::endl;
      continue;
    }
    InitBranches();

    ProcessTree();

  }//m_jetCollections
  m_inputFile->Close();

  //now let's create the efficiency plots
  for(unsigned int i = 0; i < m_fullTriggerCombination.size();i++){
    cout << "Producing efficiency curve for: " << m_fullTriggerCombination[i]<< endl;
    m_efficiencyPlots[m_fullTriggerCombination[i]]->GetXaxis()->SetTitle("p_{T}^{Avg} / GeV");
    m_efficiencyPlots[m_fullTriggerCombination[i]]->GetYaxis()->SetTitle("Efficiency");

    m_efficiencyPlots[m_fullTriggerCombination[i]+"_eta"]->GetXaxis()->SetTitle("#eta");
    m_efficiencyPlots[m_fullTriggerCombination[i]+"_eta"]->GetYaxis()->SetTitle("Efficiency");


    TString histName = m_fullTriggerCombination[m_referenceTriggerIndex[i]]+"_triggered_binningScheme" + Form("%d",i);

    m_efficiencyPlots[m_fullTriggerCombination[i]]->SetTitle(m_fullTriggerCombination[i]);
    m_efficiencyPlots[m_fullTriggerCombination[i]]->Divide(m_hists1D[m_fullTriggerCombination[i]],
                                                    m_hists1D[histName],
                                                    "e0");

    m_efficiencyPlots[m_fullTriggerCombination[i]+"_eta"]->SetTitle(m_fullTriggerCombination[i]);
    m_efficiencyPlots[m_fullTriggerCombination[i]+"_eta"]->Divide(m_hists1D[m_fullTriggerCombination[i]+"_eta"],
                                                          m_hists1D[m_fullTriggerCombination[m_referenceTriggerIndex[i]]+"_Triggered_eta"],
                                                          "e0");

    m_turnOnPT[m_fullTriggerCombination[i]] = GetTurnOnPT(m_efficiencyPlots[m_fullTriggerCombination[i]]);
    cout << m_fullTriggerCombination[i] << " has turn on PT=" << m_turnOnPT[m_fullTriggerCombination[i]] << endl;
    }//finished looping over trigger combinations and producing the efficiency curves.
}


void TriggerEfficiencyEvaluator::SetTrigMap(){
  	trigMap["HLT_j15"] = m_pass_HLT_j15;
  	trigMap["HLT_j25"] = m_pass_HLT_j25;
  	trigMap["HLT_j35"] = m_pass_HLT_j35;
  	trigMap["HLT_j45"] = m_pass_HLT_j45;
  	trigMap["HLT_j60"] = m_pass_HLT_j60;
  	trigMap["HLT_j110"] = m_pass_HLT_j110;
  	trigMap["HLT_j175"] = m_pass_HLT_j175;
  	trigMap["HLT_j260"] = m_pass_HLT_j260;
  	trigMap["HLT_j360"] = m_pass_HLT_j360;

  	trigMap["HLT_j15_320eta490"] = m_pass_HLT_j15_320eta490;
  	trigMap["HLT_j25_320eta490"] = m_pass_HLT_j25_320eta490;
  	trigMap["HLT_j35_320eta490"] = m_pass_HLT_j35_320eta490;
  	trigMap["HLT_j45_320eta490"] = m_pass_HLT_j45_320eta490;
  	trigMap["HLT_j60_320eta490"] = m_pass_HLT_j60_320eta490;
  	trigMap["HLT_j110_320eta490"] = m_pass_HLT_j110_320eta490;
  	trigMap["HLT_j175_320eta490"] = m_pass_HLT_j175_320eta490;
  	trigMap["HLT_j260_320eta490"] = m_pass_HLT_j260_320eta490;
  	trigMap["HLT_j360_320eta490"] = m_pass_HLT_j360_320eta490;

}

bool TriggerEfficiencyEvaluator::PassEmulation(TString trigger){
  return false;
}
bool TriggerEfficiencyEvaluator::PassTriggerCombination(vector<TString> triggerComb, bool emulate){
  //trigger combination is a vector of triggers - this funtion returns true if any of these fire
  // if emulating it returns ture if the jets pass any of the EMULATION
  for( auto &trigger : triggerComb){
    //trigger evluation already completed by the microXAOD
    if(!emulate){
        if(trigMap[trigger]) return true;
    }else{
      //emulae the Triggers

      //get the ET and eta of the jet and trigger
      float j1_ET = m_j1_E/cosh(m_j1_eta);
      float j2_ET = m_j2_E/cosh(m_j2_eta);
      bool  forwardTrigger = trigger.Contains("_320eta490");

      //recal trigger combination is a vector of trigger names - such as HLT_jX_320eta490
      //therefore we need to get that X as its the ET required of a jet to fire the trigger
      TString triggerETString = trigger;
      triggerETString = triggerETString.ReplaceAll("HLT_j", "");
      triggerETString = triggerETString.ReplaceAll("_320eta490","");
      float triggerET = triggerETString.Atof();

      if(forwardTrigger){
          //emuate the forward  trigger
        if( (j1_ET > triggerET) && (fabs(m_j1_eta) >= 3.2 && fabs(m_j1_eta) <= 4.9 ) ||
            (j2_ET > triggerET) && (fabs(m_j2_eta) >= 3.2 && fabs(m_j2_eta) <= 4.9 )){
              return true;
        }
      }else{
          //emuate the central trigger
          if( (j1_ET > triggerET) && (fabs(m_j1_eta) < 3.2 ) ||
              (j2_ET > triggerET) && (fabs(m_j2_eta) < 3.2 )){
                return true;
          }
      }//end else forward trigger
    }//emnd else emulate
 }//end for loop
  return false;
}
void TriggerEfficiencyEvaluator::FillHistograms(){
  for(unsigned int i = 0; i < m_fullTriggerCombination.size(); i++){
    //Fill triggered Histograms
    if( PassTriggerCombination(m_triggerCombinations[i], false)){
      if(m_pTavg/1000 >= m_etaPlotsMinPTAvg[i])
        m_hists1D[m_fullTriggerCombination[i]+"_Triggered_eta"]->Fill(m_j1_eta);
      for(unsigned int j = 0; j < m_pTMin.size(); j++){
        TString histName = m_fullTriggerCombination[i]+"_triggered_binningScheme" + Form("%d",j);
        m_hists1D[histName]->Fill(m_pTavg);
       }
     }
    //fill emulation histogram
    if( PassTriggerCombination(m_triggerCombinations[i], true ) )
    if( PassTriggerCombination(m_triggerCombinations[m_referenceTriggerIndex[i]], false ) ){
         m_hists1D[m_fullTriggerCombination[i]]->Fill(m_pTavg);
         if(m_pTavg/1000 >= m_etaPlotsMinPTAvg[i])  //require the event to be in the platuae region.
          m_hists1D[m_fullTriggerCombination[i]+"_eta"]->Fill(m_j1_eta);
     }


  }
  //
}
void TriggerEfficiencyEvaluator::ProcessTree()
{
	Long64_t nentries = m_dijetInsituTree->GetEntries();
	printf("\nWill loop over %d events\n\n",int(nentries));
	for (Long64_t jentry=0; jentry<nentries; jentry++) {
		if (jentry&&(jentry%100000)==0) {
			cout<<Form("Processed %5dk / %6.2fM events ( %7.1f%% )",int(jentry)/1000,float(nentries)/1e6,100.0*jentry/nentries);
      PrintTime();
		}

		//Long64_t ientry = m_dijetInsituTree->LoadTree(jentry);
		m_dijetInsituTree->GetEntry(jentry);
    SetTrigMap();
    if(PassCuts());
      FillHistograms();
	}
  trigMap.clear();
}//event loop

void TriggerEfficiencyEvaluator::WriteOutput(){
  TFile* outputFile = InitOutputFile(m_outputFilePath+"triggerEfficiencies.root");
  if(!outputFile){ error("Failed to open outputFile" ); }

  for(unsigned int i = 0; i < m_fullTriggerCombination.size(); i++){
      m_hists1D[m_fullTriggerCombination[i]]->Write();
      m_efficiencyPlots[m_fullTriggerCombination[i]]->Write();
  }
  outputFile->Close();
}

void FormatGraph(TGraph *h, int mstyle, int col, double msize, bool FillStyle=false, int fillStyle=0 ) {

  h->SetMarkerStyle(mstyle);
  h->SetMarkerSize(msize);
  h->SetMarkerColor(col);
  h->SetLineColor(col);
  h->SetLineWidth(3);

  gStyle->SetHatchesSpacing(0.7);
  gStyle->SetHatchesLineWidth(2);

  if(FillStyle){
    h->SetFillStyle(fillStyle);
    h->SetFillColor(col);
  }else{
    h->SetFillStyle(0);
  }


}


void FormatGraph(TGraph *h, int style, bool fillStyle=false)
{

  if      (style==0)  FormatGraph( h, 20, kBlack,  0.8, fillStyle, 3001 );
  else if (style==1)  FormatGraph( h, 21, kRed+1,  0.8, fillStyle, 3001 );
  else if (style==2)  FormatGraph( h, 22, kBlue,    0.6, fillStyle, 3351 );
  else if (style==3)  FormatGraph( h, 23, kGreen+2,   0.8, fillStyle, 3315 );
  else if (style==4)  FormatGraph( h, 29, kOrange+1,      1.2, fillStyle, 3302 );
  else if (style==5)  FormatGraph( h, 33, kViolet+1,     1.6, fillStyle, 3003 );
  else if (style==6)  FormatGraph( h, 34, kGray+2,     0.8, fillStyle, 3004 );
  else if (style==7)  FormatGraph( h, 5,  kYellow+2,     0.8, fillStyle, 3004 );
  else if (style==8)  FormatGraph( h, 28, kGreen-5,     0.8, fillStyle, 3004 );
  else if (style==9 ) FormatGraph( h, 27, kYellow-5,     0.8, fillStyle, 3004 );
  else if (style==10) FormatGraph( h, 25, kCyan,      1.2, fillStyle, 3005 );


}

int GetColorValue(int style){

    if      (style==0) return  kBlack;
    else if (style==1)  return kRed+1;
    else if (style==2)  return kBlue;
    else if (style==3)  return kGreen+2;
    else if (style==4)  return kOrange+1;
    else if (style==5)  return kViolet+1;
    else if (style==6)  return kGray+2;
    else if (style==7)  return kYellow+2;
    else if (style==8)  return kGreen-5;
    else if (style==9)  return kYellow-5;
    else if (style==10) return kCyan;
}

void TriggerEfficiencyEvaluator::DrawEfficiencyCurve(int index,int counter, bool drawEta){
  int i = index;
  float dy = 0.045;
  float y  = 0.65-index*dy;
  TString histName = m_fullTriggerCombination[i];
  if(drawEta) histName += "_eta";
  FormatGraph(  m_efficiencyPlots[histName],counter, false);
  m_efficiencyPlots[histName]->SetMaximum(1.1f);
  m_efficiencyPlots[histName]->GetXaxis()->SetTitle("p_{T}^{Avg}/GeV");
  m_efficiencyPlots[histName]->GetYaxis()->SetTitle("Trigger Efficiency");
  m_efficiencyPlots[histName]->Draw("same p");
}
float TriggerEfficiencyEvaluator::GetMinPt(vector<int> graphIndices){
  vector<float> pTs;

  for(auto i : graphIndices)
    pTs.push_back(m_pTMin[i]);

  float minPt = pTs[0];
  for( auto pT : pTs)
    if(pT < minPt) minPt = pT;

  return minPt;
}

float TriggerEfficiencyEvaluator::GetMaxPt(vector<int> graphIndices){
  vector<float> pTs;

  for(auto i : graphIndices)
    pTs.push_back(m_pTMax[i]);

  float maxPt = pTs[0];
  for( auto pT : pTs)
    if(pT > maxPt) maxPt = pT;

  return maxPt;
}

void TriggerEfficiencyEvaluator::DrawEfficiencyCurves(vector<int> graphIndices, bool drawEta){
  SetAtlasStyle();
  TCanvas* can = new TCanvas("Trigger Efficiency" ,"Trigger Efficiency", 842,595 );
  cout << "Creating latex object.." << endl;
  TLatex tex;
  tex.SetTextFont(43);
  tex.SetTextSize(15);
  tex.SetNDC();

  gStyle->SetTitleOffset(1.2,"X"); gStyle->SetTitleOffset(1.2,"Y");
  gStyle->SetTitleOffset(1.2,"Z");

  TString efficiencyPlotsName =m_outputFilePath + (drawEta ? "Eta" : "Pt" ) +  "EfficiencyPlots";
  for(int i = 0; i< graphIndices.size();i++)
    efficiencyPlotsName += "_" + m_fullTriggerCombination[graphIndices[i]];
  efficiencyPlotsName += ".pdf";

  float minX = -4.9, maxX = 4.9;
  if(!drawEta){
    minX = GetMinPt(graphIndices), maxX =GetMaxPt(graphIndices);
  }

  TH1F *mall = new TH1F("mall","",1,minX,maxX);
  mall->SetMinimum(0); mall->SetMaximum(1.1);
  mall->GetXaxis()->SetTitle((drawEta ? "#eta_{det}" :  "p_{T}^{Avg} /GeV"));
  mall->GetYaxis()->SetTitle("Trigger Efficiency");
  mall->GetYaxis()->SetTitleOffset(0.9);
  mall->GetXaxis()->SetTitleOffset(0.92);
  mall->GetYaxis()->SetTitleSize(0.04);
  mall->GetXaxis()->SetTitleSize(0.04);
  mall->GetYaxis()->SetLabelSize(0.03);
  mall->GetXaxis()->SetLabelSize(0.03);
  mall->Draw();

  std::cout << "Creating plot: " << efficiencyPlotsName << endl;
  can->Print(efficiencyPlotsName+"[");

  float y = 0.15;
  for(int i = 0; i <  graphIndices.size();i++){
    y += 0.05;
    DrawEfficiencyCurve(graphIndices[i],i,drawEta);
    tex.DrawLatex(0.65,y,Form("#color[%d]{%s}",GetColorValue(i), m_fullTriggerCombination[graphIndices[i]].Data()));
  }
  can->Print(efficiencyPlotsName);
  can->Print(efficiencyPlotsName+"]");
  delete mall;
  delete can;
}
void TriggerEfficiencyEvaluator::DrawOutput(){
  for(auto graphIndices : m_graphsToDraw)
    DrawEfficiencyCurves(graphIndices,false);
  for(auto graphIndices : m_graphsToDraw)
    DrawEfficiencyCurves(graphIndices,true);

  TCanvas* can = new TCanvas("Trigger Efficiency" ,"Trigger Efficiency", 842,595 );
  TCanvas* can2 = new TCanvas("Number of events passed trigger" ,"Number of events passed trigger", 842,595 );
  TCanvas* can3 = new TCanvas("Trigger Efficiency Plot" ,"Trigger Efficiency Plot", 842,595 );
  TCanvas* can4 = new TCanvas("Trigger Efficiency Eta" ,"Trigger Efficiency Eta", 842,595 );

  TLatex tex;
  tex.SetTextFont(43);
  tex.SetTextSize(15);
  tex.SetNDC();

  SetAtlasStyle();

  gStyle->SetTitleOffset(1.2,"X"); gStyle->SetTitleOffset(1.2,"Y");
  gStyle->SetTitleOffset(1.2,"Z");

  TString efficiencyPlotsName = m_outputFilePath + "triggerEfficiencies.pdf";
  TString nEventsPlotsName = m_outputFilePath + "triggerNevents.pdf";

  can->Print(efficiencyPlotsName+"[");
  can2->Print(nEventsPlotsName+"[");

  float dy =0.05;
  float y  =0.65;
  //SetUp blank histograms to draw that cover the correct range.
  TH1F *mall = new TH1F("mall","",1,0,600);
  TH1F *mallEta = new TH1F("mallEta","",1,-4.6,4.6);
  mallEta->GetYaxis()->SetRangeUser(0,1.0);
  can3->cd();
  mall->Draw();
  can4->cd();
  mallEta->Draw();
  for(unsigned int i = 0; i < m_fullTriggerCombination.size(); i++){
    TString combinationName = m_fullTriggerCombination[i];
    TString denominatorName = m_fullTriggerCombination[m_referenceTriggerIndex[i]];

    can->cd();
    can->Clear();
    m_efficiencyPlots[m_fullTriggerCombination[i]]->Draw();
    tex.DrawLatex(0.15,0.75,Form("#color[2]{#frac{%s}{%s}}",combinationName.Data(),denominatorName.Data()));
    can->Print(efficiencyPlotsName);
    can->Print(m_outputFilePath+"CFiles/"+combinationName+"_efficiency.C");
    can->Print(m_outputFilePath+"plots/"+combinationName+"_efficiency.pdf");


    can->Clear();
    m_efficiencyPlots[m_fullTriggerCombination[i]+"_eta"]->Draw();
    tex.DrawLatex(0.15,0.75,Form("#color[2]{#frac{%s}{%s}}",combinationName.Data(),denominatorName.Data()));
    can->Print(efficiencyPlotsName);
    can->Print(m_outputFilePath+"CFiles/"+combinationName+"_eta_efficiency.C");
    can->Print(m_outputFilePath+"plots/"+combinationName+"_eta_efficiency.pdf");

    can2->cd();
    can2->Clear();
    m_hists1D[m_fullTriggerCombination[i]]->Draw();
    can2->Print(nEventsPlotsName);
    can2->Print(m_outputFilePath+"CFiles/"+combinationName+"_nevents.C");
    can2->Print(m_outputFilePath+"plots/"+combinationName+"_nevents.pdf");

    can2->Clear();
    m_hists1D[m_fullTriggerCombination[i]+"_eta"]->Draw();
    can2->Print(nEventsPlotsName);
    can2->Print(m_outputFilePath+"CFiles/"+combinationName+"_eta_nevents.C");
    can2->Print(m_outputFilePath+"plots/"+combinationName+"_eta_nevents.pdf");

    can3->cd();
    FormatGraph(  m_efficiencyPlots[m_fullTriggerCombination[i]],i, false);
    tex.DrawLatex(0.58,y-=dy,Form("#color[%d]{%s}",GetColorValue(i), combinationName.Data()));
    m_efficiencyPlots[m_fullTriggerCombination[i]]->Draw("same p");

    can4->cd();
    FormatGraph(  m_efficiencyPlots[m_fullTriggerCombination[i]+"_eta"],i, false);
    tex.DrawLatex(0.58,y,Form("#color[%d]{%s}",GetColorValue(i), combinationName.Data()));
    m_efficiencyPlots[m_fullTriggerCombination[i]+"_eta"]->Draw("same p");


  }

  can3->Print(Form("%sallEfficiencies.pdf",m_outputFilePath.c_str()));
  can3->Print(Form("%sCFiles/allEfficiencies.C",m_outputFilePath.c_str()));

  can4->Print(Form("%sallEfficienciesEta.pdf",m_outputFilePath.c_str()));
  can4->Print(Form("%sCFiles/allEfficienciesEta.C",m_outputFilePath.c_str()));

  can->Print(efficiencyPlotsName+"]");
  can2->Print(nEventsPlotsName+"]");
  delete can;
  delete can2;
  delete can3;
  delete can4;
}
