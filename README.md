# Dijet in situ eta intercalibration

Caution: this is very much a work in progress!!

Currently the analysis is implemented over 4 packages, with each stage taking the previous stage's output as input :
 1. EtaIntercalxAODAnalysis - Generates an NTuple, storing relevant kinematics, asymmetry variables and variables for systematics
 2. GenerateHistograms      - Generates a 3D histogram of (Asym,pT,eta) with either a systematic applied or for the nominal cuts
 3. MinimizeMatrixMethod    - Evaluates the response plots
 4. CalibrationAndUncertainties - Evaluates the final calibration.


## Quick Start

Firstly, if you have not already done so, check out the package from git:

$ git clone ssh://git@gitlab.cern.ch:7999/atlas-jetetmiss-jesjer/Dijet-Insitu-Calibration-and-Resolution.git

On lxplus, setup the ATLAS software environment in the usual way:

$ setupATLAS

Or if elsewhere:

$ export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase  
$ alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'  
$ setupATLAS


## EtaIntercalxAODAnalysis script

### RootCore setup for R20.X data samples

In the Dijet-Insitu-Calibration-and-Resolution directory, setup an ATLAS Analysis Release:

$ rcSetup Base,2.5.1

If you have already setup an Analysis Release in the past (and have a RootCoreBin directory), simply call:

$ rcSetup

To compile the code, run:

$ rc find_packages  
$ rc compile

Note that in principle the find_packages script only needs to be called when changing the package dependencies.

This will create an executable EtaInterCalxAODAnalysis which you can run over some xAOD files!

### CMake setup for R21.X data samples

In the Dijet-Insitu-Calibration-and-Resolution directory, for each session you just need to call: <br />
$ source setup.sh

This will make/build the package.

### Configuration file

EtaInterCalxAODAnalysis/share/settings.config

Important things to specify in the config file are:

* if you are running on R21 samples (isR21:1) or R20 samples (isR21:0)
* GRL xml file: can be downloaded from [here](http://atlasdqm.web.cern.ch/atlasdqm/grlgen/All_Good/), and should be put in EtaInterCalxAODAnalysis/GRLs directory
* JetCalibTool.*.config: the latest recommended config file for jet calibration (note that it's different for R21 or R20 samples)
* JetCollections: AntiKt4EMTopo AntiKt4EMPFlow AntiKt4LCTopo
* Cuts/calibration/selection settings

### Running locally

For now the executable expects as an argument a comma-separated list of input files, for example:

EtaInterCalAnalysis Pythia8_JZ1W_xAOD_1.pool.root,Pythia8_JZ1W_xAOD_2.pool.root,Pythia8_JZ1W_xAOD_3.pool.root

The output is a single ROOT file called Pythia8_EtaInterCal_3DHistos.root (this will be made configurable!).

### Running on the grid

1. Setup voms:  
$  voms-proxy-init -voms atlas

2. Then panda:  
$  lsetup panda

3. Modify the submission script's user setting USERNAME to be your CERN USERNAME, file:  
$  EtaInterCalxAODAnalysis/scripts/gridSubmit_data15_13TeV_25ns.sh

4. From the folder above EtaInterCalxAODAnalysis run the grid submission scripts  
$  source EtaInterCalxAODAnalysis/scripts/gridSubmit_data15_13TeV_25ns.sh  
   This submits jobs for all 25ns, 13TeV ran in 2015.

5. Files can then be retrived through rucio in the following way:  
$  voms-proxy-init -voms atlas  
$  lsetup rucio  
$  cd <FolderToDownloadDataTo>  
$  rucio download user.<CERNUSERNAME>.data15_13TeV.*.DijetInterCal.*_EtaInterCal_3DHistos_data.root  

prun command configuration differences for RootCore and CMake:

* **RootCore**: --useRootCore --exec="EtaInterCalxAODAnalysis/scripts/gridExec.sh %IN"
* **CMake**: --useAthenaPackages --exec="EtaInterCalxAODAnalysis/scripts/gridExec_cmake.sh %IN"

## Install Bootstrap package

The package (specifically the GenerateHistograms script) now uses the official Bootstrap package to derive toys (needed to propagate statistical uncertainties).

Just for the first time when setting the package, do: <br />
$  source Bootstrap_setup.sh

This will clone the package from git and install it. <br />
The commands to link Bootstrap package when making the GenerateHistograms script are already included.

## GenerateHistograms script

### Configuration file

GenerateHistograms/utils/settings.config

Important things to specify:
* doNominal: for nominal cuts.
* doSystematics: for shifted cuts. Also should select which systematic to run over:  
doDphiShiftUp - doDphiShiftDown - doJet3PtFracShiftUp - doJet3PtFracShiftDown - doJVTCutTight - doJVTCutLoose
* pTavg.Bins: these bins are defined based on the dedicated triggers efficiencies.  
Usually final required pT binning is chosen at this step, although bin combinations can be done in the next script.
* EtaBins.3Dhists: at this step, very fin binning is chosen. Different Eta binning to be tested at the next script.
* Cuts/calibration/selection settings
* JetCollections
* inclusionTrigComb: nominal is 1, to use the inclusion triggers combination method. If put to 0, an older combination method is used but contains a bias due to trigger inefficiency and prescale (used only for testings). In this script, events are only divided into different histograms, but no weights are applied. (see these presentations [p1](https://indico.cern.ch/event/658773/contributions/2733203/attachments/1530351/2394896/presentation_eta-intercalibration_Triggers_tension_p2-20170926_v3.pdf) and [p2](https://indico.cern.ch/event/670718/contributions/2757267/attachments/1542201/2419031/presentation_eta-intercalibration_Triggers_combinations_p3-20171017_v1.pdf) for methods description and comparison)
* CentralLumis.${productionTag} and ForwardLumis.${productionTag}: luminosity registered by each trigger.  
You can derive them using this [LumiCalculator](https://atlas-lumicalc.cern.ch/). You need to upload the GRL file used to derive the data NTuples, and specify the name of the trigger.
* Deliveredlumi.${productionTag}: total unprescaled luminosity, derived same as the previous item using LumiCalculator.
* ${MC}.CrossSections.${productionTag} and ${MC}.FilterEff.${productionTag}: for each slice. These can be found using [AMI](https://ami.in2p3.fr/) website.
* ${MC}.NEvents.${productionTag}:  
if Powheg or Sherpa: Total sum of weights of all events: these sums are saved in histograms in the NTuples.  
if Pythia or Herwig: Total sum of events: found using [AMI](https://ami.in2p3.fr/) website.

### Luminosity weights
Depending on which triggers combination method is used, the events classification and their corresponding lumi weights are found here: <br />
GenerateHistograms/Histogram3DTool.C -> TriggerClassificationOldMethod() and TriggerClassificationInclusionMethod()

### Running locally

From GenerateHistograms directory:

$ ./histogramGenerator.exe utils/settings.config ${inFile} ${outPath} ${dataSet} ${productionTag} ${onTheFlySettings}  
**inFile**: path to the NTuples produces using the first script.  
**outPath**: path of the output file to be produced which will contain the 3D histograms.  
**dataSet**: specify if data, PowhegPythia8, Sherpa.
**productionTag**: to get the correct lumi inputs
**onTheFlySettings**: can be used to over-ride the settings specified in the config file (leave empty if nothing to be changed).

You need to run this script on data, and 2 MC (one which is the nominal chosen generator, and another generator to derive the systematic on the modeling).

### Toys running

To run on toys, add to the variable ${onTheFlySettings}: <br />
doBootStrap=1,nbToys=XX <br />
XX is the total number of toys you want to generate.

## MinimizeMatrixMethod script

### Configuration file
MinimizeMatrixMethod/utils/Dijet_xAOD.config

Important things to specify:
* PtBinsAntiKt4 and PtRangesAntiKt4: final required pT binning.
* AntiKt4.${pTrange}.TrigOR: trigger combination for each pT range.
* EtaBinsSM_${etaBinning} and EtaBinsMM_${etaBinning} : final required Eta binning.
* ~~isInclusionTrigComb: should be the same as inclusionTrigComb in GenerateHistograms config~~ this is now automatically done by the code.
* ${MC}.SampleNames: name of the slices to be included (J1, J2 ...).

### Running locally

From MinimizeMatrixMethod directory:

$ ./matrix_minimization.exe utils/Dijet_xAOD.config ${JetCollection} ${DirName} ${etaBinning} ${inFile} ${outDir}/${outName}.root ${cuts}  
**inFile**: output of GenerateHistograms script
**DirName**: name of the directory inside the root file. This is equal to $JetCollection, except when running on toys it is equal to ${JetCollection}_toy${i} (i=number of toy)
**cuts**: nominal, syst_dphiDown, syst_dphiUp, syst_j3down, syst_j3up, syst_jvtTight or syst_jvtLoose

## CalibrationAndUncertainties

### Running locally

From CalibrationAndUncertainties directory:

$ source deriveCalibrationAndUncertainties.sh

you should specify inside **deriveCalibrationAndUncertainties.sh** file:
* code=EtaIncerCalibrationAndUncertainties.C
* jetAlgos: JetCollections you want to run the calibration on
* methods: "MM" for Matrix Method or "SM" for Standard Method
* input files for data, nominal MC and variated MC: these files are the produced outputs of the MinimizeMatrixMethod script.
* outputPath.

Inside **EtaIncerCalibrationAndUncertainties.C** file, you can choose also to include and draw the selection(cuts)systematics from the various shifted cuts.  
For that change **_drawSyst** and **_includeSyst** variables to true, and specify the systematic input files for data **f_data_dphi_dn, f_data_dphi_up, f_data_jvt_tight, f_data_j3pt_dn, f_data_j3pt_up** variables, and the systematic input files for the nominal MC in **f_mc_dphi_dn, f_mc_dphi_up, f_mc_jvt_tight, f_mc_j3pt_dn, f_mc_j3pt_up** variables.

At this step, you will obtain the calibration factors, with their statistical uncertainties, the systematic from the MC modeling, and the selection(cuts) systematics.  
Statistical uncertainties of the selection systematics can not be obtained at this step because of the correlation between the derived correction factors.

### Running on toys to derive uncertainties of selection(cuts) systematics

It's important to derive these statistical uncertainties in order to see if the observed systematic fluctuations are statistically significant or if larger bins should be used.

Toys of nominal and shifted cuts need to be provided for data and nominal MC.  
There's 2 ways for producing these toys:
1. Histogram bin content fluctuation (easier and faster method):  
Previously, using GenerateHistograms script, we have generated 3D histograms for nominal and shifted selection(cuts).  
For each of these 3D histograms, configure the script GenerateToys/generate_toys.py to read in this histogram and run it.  
$ python generate_toys.py ${input_file_name} ${output_file_name} ${jet_collection}

2. Event by event fluctuation (longer method, but will be needed to propagate the systematic correlations)  
Using GenerateHistograms script:  
Make a loop on the number of toys needed, and execute the script specifying onTheFlySettings="doBootStrap=1,toy=${toyNum},lastToy=$(($toyNum+1))".  
There's also a python script to manage all that, **SubmitBootStrapJobs.py**.  
(P.S.: for now, the script uses a random seed for each event, not based on the event number and run number. To be able to propagate the correlations, the script should be modified to use the Bootstrap package.)

Then:
* MinimizeMatrixMethod script: minimize all produced toys.
* SystematicShiftStatSignificance/EvalShiftStatSignificance.py:
Specify inside it the path to nominal and toys outputs of MinimizeMatrixMethod script
run: $ python EvalShiftStatSignificance.py ${JetCollection} ${method} ${TotalToysNumber}
* CalibrationAndUncertainties/EtaIncerCalibrationAndUncertaintiesBS.C  
Output files of EvalShiftStatSignificance.py are the inputs for EtaIncerCalibrationAndUncertaintiesBS.C  
run: $ source deriveCalibrationAndUncertainties.sh  
and specify inside it: code=EtaIncerCalibrationAndUncertaintiesBS.C  
