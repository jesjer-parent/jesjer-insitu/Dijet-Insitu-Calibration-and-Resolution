#!/bin/sh

code=Prelim2015EtaIncercalUncertainties.C
exec=finalize_calibration.exe

etaBinning="0.3"
jetAlgos="AntiKt4EMTopo"
sampleNames="nominal"
methods="MM"

flagsNlibs="`$ROOTSYS/bin/root-config --cflags --glibs` -lTreePlayer -lHistPainter" 

rm -f $exec
clang++ $flagsNlibs -o $exec $code && {

    echo "*********************" 
    echo "Compilation successful" 
    echo "*********************" 
    
    for meth in $methods ; do
	method=$meth
	for jA in $jetAlgos ; do
	    for sample in $sampleNames ; do
		
		echo $jA
		
		dataFile=/Users/jlacey/ATLAS_Analysis/JetEtMiss/July22_etaIntercal_fromMichaela/data_13TeV_periodC_highPtTriggers_Eta0.3_AntiKt4EMTopo.root
		nom_mc_File=/Users/jlacey/ATLAS_Analysis/JetEtMiss/July22_etaIntercal_fromMichaela/PowhegPythia_50ns_highPtBins_AntiKt4EMTopo.root
		syst_mc_File=/Users/jlacey/ATLAS_Analysis/JetEtMiss/July22_etaIntercal_fromMichaela/Pythia_50ns_highPtBins_AntiKt4EMTopo.root
		pythia2015_truth_File=/Users/jlacey/ATLAS_Analysis/JetEtMiss/July20_etaIntercal_fromMichaela/Pythia8_50ns_AntiKt4Truth.root

		
		pythia2012_truth_File=/Users/jlacey/ATLAS_Analysis/JetEtMiss/Truth_Rscan/pythia_AntiKt4Truth_nominal_p1562_Eta0.3.root
		uncertainties2012=/Users/jlacey/ATLAS_Analysis/JetEtMiss/DijetEtaInterCalibration/trunk/DeriveCalibrationUncertainties/AntiKt4TopoEM_EtaIntercalibrationUncertainty_MM_Eta0.3.root

		
		echo "***************************************************************"
		echo "   method: $meth -- jetalgo: $jA -- sample: $sample "
		echo "***************************************************************"
		echo "*********************   inputs   ******************************"
		echo "***************************************************************"
		
		echo $dataFile
		echo $nom_mc_File
		./$exec $sample $jA $meth $dataFile $nom_mc_File $syst_mc_File $pythia2012_truth_File $pythia2015_truth_File $uncertainties2012
		
		
		echo "**********************************************************"
		echo "**********************************************************"
		echo "*********************** done *****************************"
	    done
	done
    done
    
    
}


